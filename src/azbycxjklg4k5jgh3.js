
/*
*****************************************************************************
* License Information :Accord Global Technology Solutions Pvt. Ltd.                  *

*                      #72 & 73, Krishna Reddy Colony, Domlur layout,            *

*                      Domlur,Bangalore - 560071, INDIA                     *

*                      Licensed software and All rights reserved.           *

*****************************************************************************

* File             : security
*
* Description      : Security modal page
*
* Author(s)        : Kavita Gupta
*
* Version History:
* <Version Number>                 <Author>              <date>      <defect Number>      <Modification
*                                                                                          made and the
*                                                                                          reason for
*                                                                                          modification >
*  1.0                             Kavita Gupta          25.11.2022        --         initial version
*
* References        :
*                     
* Assumption(s)     : None.
*                     
* Constraint(s)     : None.
*                     
 ****************************************************************************
*/

import React, { Component  } from 'react';
import {port} from 'config/config.js';
import alertSound from "sounds/notification.mp3";
import msgSound from "sounds/soft_notification.mp3";

const alertAudio = new Audio(alertSound);
const msgAudio = new Audio(msgSound);

export default class AuthHelper extends Component {

  constructor(props) {
    super(props);
    this.state = { 
      LogoutModal:false,
      logoutErrMsg:"",
      isOK:true,
    }    
}


 
  
playSound(audioFile){
  //let playedPromise;
  switch(audioFile){
    case "alert":
      const alertAudioPromise = alertAudio.play();
      if (alertAudioPromise) {
        alertAudioPromise.catch((e) => {
            if (e.name === 'NotAllowedError' ||
                e.name === 'NotSupportedError') {
                console.log(e.name);
            }
        });
      }
      break;
    case "message":
      const msgAudioPromise = msgAudio.play();
      if (msgAudioPromise) {
        msgAudioPromise.catch((e) => {
            if (e.name === 'NotAllowedError' ||
                e.name === 'NotSupportedError') {
                console.log(e.name);
            }
        });
      }
      break;

    default:
      const alertAudioPromis = alertAudio.play();
      if (alertAudioPromis) {
        alertAudioPromis.catch((e) => {
            if (e.name === 'NotAllowedError' ||
                e.name === 'NotSupportedError') {
                console.log(e.name);
            }
        });
      }

      break;
  }

}

loader = (booln) =>{
  var loader;
  if(document.getElementById("loader") != null){
    loader = document.getElementById("loader");
    if(booln){
      document.body.style.cursor = "wait";
      loader.style.display = "block";
    }else{
      document.body.style.cursor = "default";
      loader.style.display = "none";
    }
  }

}

handleResize = (e) => {
  if(window.innerWidth > 767){
      document.body.classList.add("sidebar-collapse");
  }else{
      document.body.classList.remove("sidebar-open");
  }
};

setToken = idToken => {
    // Saves user token to localStorage
    sessionStorage.setItem("groot", idToken);
};


// Retrieves the user token from localStorage
getToken = () => {
  if(sessionStorage.getItem('groot') !== null){    
    return sessionStorage.getItem('groot');
  }  
  else{    
    this.clearStorage();
  return "";  
  } 
}; 

fetch = (url, options) => {
    var headers = {};
    if (this.loggedIn()) {
     options.headers["Authorization"] = this.getToken();
     options.headers['Accept'] = "application/json";
     options.headers['Groot'] = this.getLoggedinId();
     options.headers['Accept_'] = this.get_l();
    }   

    url = port + url;
    if(this.state.isOK){
    }else{
      console.log("expired",this.state.isOK)
    } 

    return fetch(url, {
         headers,
          ...options
     })    
    .then(response => {
         //console.log("Received Response",response)
        if(response.status >= 200 && response.status <= 300){
          //console.log(response);
          return response.json();    

        }else if(response.status === 500){ 
            console.log("waiting..");

        }else{
            this.setState({isOk:false});
            console.log("Something went wrong", response.status)
            //alert("Session Expired! relogin to continue..");
            this.clearStorage();
            window.location.replace("/login");
            //return response;
        }
    })
    .then(response => { 
      //alert("next");
      
      return response;
    })    
  

  };

}