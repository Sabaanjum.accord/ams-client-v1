import React from 'react';
import ReactDOM from 'react-dom/client';
import App from 'App';
import 'css/index.css'  //Primary CSS
import 'css/dark.scss'  //Dark theme CSS
import 'css/home.scss'  //Home page based CSS
import 'css/login.scss' //Login page based CSSs
import 'fontawesome-5.14.0/all.min.css';
import reportWebVitals from './reportWebVitals';

const root = ReactDOM.createRoot(document.getElementById('accord-app'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();


