
/*
*****************************************************************************
* License Information :Accord Global Technology Solutions Pvt. Ltd.                  *

*                      #72 & 73, Krishna Reddy Colony, Domlur layout,            *

*                      Domlur,Bangalore - 560071, INDIA                     *

*                      Licensed software and All rights reserved.           *

*****************************************************************************

* File             : config.js
*
* Description      : App congifuration page
*
* Author(s)        : Kavita Gupta
*
* Version History:
* <Version Number>                 <Author>              <date>      <defect Number>      <Modification
*                                                                                          made and the
*                                                                                          reason for
*                                                                                          modification >
*  1.0                             Kavita Gupta          20.11.2022        --         initial version
*
* References        :
*                     
* Assumption(s)     : None.
*                     
* Constraint(s)     : None.
*                     
 ****************************************************************************
*/

import crypto from 'crypto';

const port = 'baseURL';
const passGroot = [84, 193, 225, 127, 243, 85, 233, 61, 191, 167, 106, 20, 79, 238, 245, 95, 69, 93, 126, 19, 174, 72, 6, 126, 61, 52, 164, 198, 14, 94, 166, 56];

// function which extracts substring from string by pattern
const extract = (str, pattern) => (str.match(pattern) || []).pop() || '';

// function to limit length of string
const limitLength = (str, length) => str.substring(0, length);

//wrap the  extract function into functions which solve pattern problems
const num = (str) => extract(str, "[0-9]+");
const namecheck = (str) => extract(str, "[A-Za-z' ]*");



let key = passGroot;
const iv = [111, 225, 248, 211, 135, 91, 206, 27, 96, 13, 113, 104, 4, 166, 197, 254];
//console.log(iv);

function encrypt(text) {
    //return text;
    text = (text == null ? "" : text).toString();
    let cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(key), iv);
    let encrypted = cipher.update(text, 'utf8', 'hex') + cipher.final('hex');
    return encrypted.toString('hex');
}

function decrypt(text) {
    //return text;
    text = (text == null ? "" : text).toString();
    let encryptedText = Buffer.from(text, 'hex');
    let decipher = crypto.createDecipheriv('aes-256-cbc', Buffer.from(key), iv);
    let decrypted = decipher.update(encryptedText, 'hex', 'utf8') + decipher.final('utf8');
    //console.log(decrypted);
    return (decrypted).toString();
}

const baseURL = 'http://192.168.14.102:5000/';
// const baseURL = 'http://192.168.13.108:5000/';

export {
    port,
    encrypt,
    decrypt,
    passGroot,
    extract,
    num,
    namecheck,
    limitLength,
    baseURL,
};