// eslint-disable-next-line
/******************************************************************************
* License Information :Accord Global Technology Solutions Pvt. Ltd.                  *

*                      #72 & 73, Krishna Reddy Colony, Domlur layout,            *

*                      Domlur,Bangalore - 560071, INDIA                     *

*                      Licensed software and All rights reserved.           *

*****************************************************************************

* File             : home.js
*
* Description      : App sidebar dynamic routing page
*
* Author(s)        : Kavita Gupta
*
* Version History:
* <Version Number>                 <Author>              <date>      <defect Number>      <Modification
*                                                                                          made and the
*                                                                                          reason for
*                                                                                          modification >
*  1.0                             Kavita Gupta          12.11.2022        --         initial version
*
* References        :
*                     
* Assumption(s)     : None.
*                     
* Constraint(s)     : None.
*                     
 ****************************************************************************
*/

import React from 'react';
import Users from 'views/users/users';

const Dashboard = React.lazy(() => import('views/dashboard/dashboard.js'));
const Demo = React.lazy(() => import('pages/demo.js'));

//profile
const Userprofile = React.lazy(() => import('views/userprofile/userprofile.js')); 


const Department = React.lazy(() => import('views/department/department.js'));

const Asset = React.lazy(() => import('views/asset/asset.js'));

const user = React.lazy(() => import('views/users/users'));

const addAsset = React.lazy(() => import('views/asset/addAsset.js'));

const addBuilding = React.lazy(() => import('views/addBuilding/addBuilding'));

const updateAsset = React.lazy(() => import('views/asset/updateAsset.js'));

const ViewAsset = React.lazy(() => import('views/asset/viewAsset.js'));


const routes = [
{ path: '/demo', name: 'Demo', element:Demo },
{ path: '/dashboard', name: 'Dashboard', element: Dashboard },

//profile
{ path: '/profile', name: 'Userprofile', element:Userprofile },


{ path: '/department', name: 'department', element: Department },

{ path: '/asset', name: 'asset', element: Asset },

{ path: '/users', name: 'user', element: user },

{ path: '/addAsset', name: 'addAsset', element: addAsset },

{ path: '/addBuilding', name: 'addBuilding', element: addBuilding },

{ path: '/updateAsset', name: 'updateAsset', element: updateAsset },

{ path: '/viewAsset', name: 'viewAsset', element: ViewAsset },

];

export default routes;
