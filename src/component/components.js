
/*
*****************************************************************************
* License Information :Accord Global Technology Solutions Pvt. Ltd.                  *

*                      #72 & 73, Krishna Reddy Colony, Domlur layout,            *

*                      Domlur,Bangalore - 560071, INDIA                     *

*                      Licensed software and All rights reserved.           *

*****************************************************************************

* File             : 
*
* Description      : App Component page
*
* Author(s)        : Kavita Gupta
*
* Version History:
* <Version Number>                 <Author>              <date>      <defect Number>      <Modification
*                                                                                          made and the
*                                                                                          reason for
*                                                                                          modification >
*  1.0                             1         12.10.2022        --         initial version
*
* References        :
*                     
* Assumption(s)     : None.
*                     
* Constraint(s)     : None.
*                     
 ****************************************************************************
*/
import React from 'react';
import SkinColor from "defaultLayout/skin-color";
//import LeftSidebar from "defaultlayout/leftsidebar";
import { Link } from 'react-router-dom';
import CustomFile from "component/customfile";

// ContainerFluid Component
const ContainerFluid=({ ...props })=> {
  const {
      children,
      className,
      ...rest
  } = props;

  const containerFluidprop=(className===undefined?"container-fluid":"container-fluid "+className);
  return (
    <div data-testid="containerfluid" {...rest} className={containerFluidprop}>
      {children}
    </div>
  );
}
//--------------------//

// Container Component
const Container=({ ...props })=> {
  const {
      children,
      className,
    ...rest

  } = props;

  const containerprop=(className===undefined?"container":"container "+className);

  return (
    <div data-testid="container" {...rest} className={containerprop}>
      {children}
    </div>
  );
}
//--------------------//


// ContentWrapper Component
const Wrapper=({ ...props })=> {
  const {
      children,
      className,
    ...rest

  } = props;

  const containerprop=(className===undefined?"":" "+className);

  return (
    <div data-testid="wrapper" {...rest} className={`wrapper${containerprop}`}>
      {children}
    </div>
  );
}
//--------------------//


// ContentWrapper Component
const ContentWrapper=({ ...props })=> {
  const {
      children,
      className,
    ...rest

  } = props;

  const containerprop=(className===undefined?"":" "+className);

  return (
    <div data-testid="content-wrapper" {...rest} className={`content-wrapper${containerprop}`} >
      {children}
    </div>
  );
}
//--------------------//


// Row Component
const Row=({ ...props })=> {
  const {
      className,
      children,
      
    ...rest

  } = props;

  const rowprop=(className===undefined?"row":"row "+className);

  return (
    <div data-testid="row" {...rest} className={rowprop}>
      {children}
    </div>
  );
}
//--------------------//

// Col Component
const Col=({ ...props })=> {
  const {
    className,
      children,
      
    ...rest

  } = props;

  const colprop=(className===undefined?"col":className);
  
  return (
    <div data-testid="col" {...rest} className={colprop}>
      {children}
    </div>
  );
}
//--------------------//

// Clearfix Component
const Clearfix=()=> {
  return (
    <div data-testid="clearfix" className="clearfix">
     
    </div>
  );
}
//--------------------//
  
//Image
const Image = ({ ...props}) => {
  const{
    className,
    alt,
    src,
    ...rest
  }=props;
  return(
    <img data-testid="image" {...rest} src={src} alt={alt} className={className} />
  )
}

// Button Component
 const Button=({ ...props })=> {
    const {
        className,
        children,
        
      ...rest
  
    } = props;

    return (
      <button data-testid="button" {...rest} className={className}>
        {children}
      </button>
    );
  
  }

//--------------------//

// Card Component
 const Card=({ ...props })=> {
  const {
      className,
      children,
      
    ...rest

  } = props;

  const cprop=(className===undefined?"":" "+className);

  return (
    <div data-testid="card" {...rest}   className={`card${cprop}`}>
      {children}
    </div>
  );

}

//--------------------//


// CardHeader Component
const CardHeader=({ ...props })=> {
  const {
      className,
      children,
      
    ...rest

  } = props;
  
  const chprop=(className===undefined?"":" "+className);

  return (
    <div data-testid="cardheader" {...rest}   className={`card-header${chprop}`}>
      {children}
    </div>
  );
}
//--------------------//

// CardBody Component
const CardBody=({ ...props })=> {
  const {
      className,
      children,
      
    ...rest

  } = props;

  const cbprop=(className===undefined?"":" "+className);

  return (
    <div data-testid="cardbody" {...rest}   className={`card-body${cbprop}`}>
      {children}
    </div>
  );
}
//--------------------//

// CardFooter Component
const CardFooter=({ ...props })=> {
  const {
      className,
      children,
      
    ...rest

  } = props;

  const cfprop=(className===undefined?"":" "+className);

  return (
    <div data-testid="cardfooter" {...rest}   className={`card-footer${cfprop}`}>
      {children}
    </div>
  );
}
//--------------------//


// Alert Component
const Alert=({ ...props })=> {
  const {
      className,
      children,
      isOpen,
      type,
      onClick,
      autoTimeout,
      closetitle,
    ...rest

  } = props;

  var alerttype;

  const alertclass=(className===undefined?"":className);
  const alerttitle=(closetitle===undefined?"close":closetitle);

 switch (type) {
   case "primary":
    alerttype="alert-primary";
     break;
    case "warning":
    alerttype="alert-warning";
      break;

    case "danger":
    alerttype="alert-danger";
      break;

    case "success":
    alerttype="alert-success";
      break;
   default:
    alerttype="alert-primary";
     break;
 }


//console.log(isClose);
 if(isOpen)
 {
  return (
    <div data-testid="alert" {...rest} className={`alert alert-dismissible ${alerttype} ${alertclass}`} role="alert">
      {children}
      <i title={alerttitle} data-dismiss="alert" className="close fas fa-times"  aria-hidden="true"  onClick={onClick}></i>
    </div>
  );
 }

 else{
  return null;
 }
  
}
//--------------------//


// Modal Component
const Modal=({ ...props })=> {
  const {
      className,
      children,
      show,
      onHide,
      size,
      type ,

    ...rest

  } = props;

  const mprop=(className===undefined?"":className);
  const modalSize=(size===undefined?"":size);
  const modalType=(type===undefined?"":type);
 
  if(show)
  {
    return (
      <div>
     <div className="modal-backdrop show fade"></div>
      <div data-testid="test-modal" {...rest}  className={`modal fade show d-block${mprop}`} 
      aria-label="Modal" >
        <div className={`modal-dialog ${modalSize}`}>
          <div className={`modal-content ${modalType}`}>
            {children}
          </div>
        </div>
        </div>
      </div>
    );
  }
  else{
return null;
  }
 
}
//--------------------//  


// Modal Component
const AlertModal=({ ...props })=> {
  const {
      className,
      children,
      show,
      onHide,
      size,
      type,
      onClose,
      onOk,

    ...rest

  } = props;

  const mprop=(className===undefined?"":className);
  const modalSize=(size===undefined?"":size);
  var modalType,modalTitle,modalbtn,modalIcon;
  
    
 switch (type) {
  case "info":
    modalType="modal-info";
    modalTitle="Info";
    modalbtn="btn-primary";
    modalIcon="fa-info-circle";
    break;
   case "warning":
    modalType="modal-warning";
    modalTitle="Warning";
    modalbtn="btn-warning";
    modalIcon="fa-exclamation-triangle";
     break;

   case "danger":
    modalType="modal-danger";
    modalTitle="Error";
    modalbtn="btn-danger";
    modalIcon="fa-exclamation-circle";
     break;

   case "success":
    modalType="modal-success";
    modalTitle="Success";
    modalbtn="btn-success";
    modalIcon="fa-check-circle";
     break;
  default:
    modalType="modal-info";
    modalTitle="Info";
    modalbtn="btn-primary";
    modalIcon="fa-info-circle";
    break;
}
  

if(show)
 {
  return (
    <div>
    <div className="modal-backdrop show fade"></div>
    <div data-testid="test-modal" {...rest} 
    className={`modal fade custom show d-block ${mprop}`} 
    aria-labelledby="ModalLabel" 
    aria-hidden="true">
    
      <div className={`modal-dialog ${modalSize}`}>
        <div className={`modal-content ${modalType}`}>
        <div className={`modal-header`}>
          <Button type="button" className="close" onClick={onClose} aria-label="Close">
          <span aria-hidden="true">&times;</span>
          </Button> 
          <i className={`fas ${modalIcon}`} aria-hidden="true"></i>
          <h4 className="modal-title">{modalTitle}</h4>
       </div>
       <hr></hr>
        <ModalBody className="text-center">
        {children}
        </ModalBody>
        <ModalFooter>
          <Button type="button" className={`btn btn-block ${modalbtn}` } onClick={onOk}>Ok</Button>
        </ModalFooter>  
         
        </div>
      </div>
      </div>
      </div>
  );
 }

 else{
  return null;
 }
}
//--------------------//  


// Modal Component
const ConfirmAlertModal=({ ...props })=> {
  const {
      className,
      children,
      show,
      onHide,
      size,
      type,
      onClose,
      onCancel,
      onOk,

    ...rest

  } = props;

  const mprop=(className===undefined?"":className);
  const modalSize=(size===undefined?"":size);
  

if(show)
 {
  return (
    <div>
    <div className="modal-backdrop show fade"></div>
    <div data-testid="test-modal" {...rest} className={`modal fade custom show d-block ${mprop}`} aria-labelledby="ModalLabel" aria-hidden="true">
      <div className={`modal-dialog ${modalSize}`}>
        <div className="modal-content modal-info">
        <div className={`modal-header`}>
          <Button type="button" className="close" onClick={onClose} aria-label="Close">
          <span aria-hidden="true">&times;</span>
          </Button> 
          <i className="fas fa-info-circle" aria-hidden="true"></i>
          <h4 className="modal-title">Info</h4>
       </div>
        <hr/>
        <ModalBody className="text-center">
        {children}
        </ModalBody>
        <ModalFooter>
          <Button type="button" className="btn btn-outline-primary" onClick={onCancel}>Cancel</Button>
          <Button type="button" className="btn btn-primary" onClick={onOk}>Ok</Button>
        </ModalFooter>  
         
        </div>
      </div>
      </div>
      </div>
  );
 }

 else{
  return null;
 }
}
//--------------------// 


// ModalHeader Component
const ModalHeader=({ ...props })=> {
  const {
      className,
      children,
      onClose,
    ...rest

  } = props;

  const mhprop=(className===undefined?"":className);

  return (
    <div data-testid="modelheader" {...rest}   className={`modal-header ${mhprop}`}>
      {children}
      <Button type="button" className="close" onClick={onClose} aria-label="Close">
       <span aria-hidden="true">&times;</span>
      </Button> 
    </div>
  );
}
//--------------------//

// ModalBody Component
const ModalBody=({ ...props })=> {
  const {
      className,
      children,
      
    ...rest

  } = props;

  const mbprop=(className===undefined?"":className);
  return (
    <div data-testid="modalbody" {...rest}   className={`modal-body ${mbprop}`}>
      {children}
    </div>
  );
}
//--------------------//

// ModalFooter Component
const ModalFooter=({ ...props })=> {
  const {
      className,
      children,
      
    ...rest

  } = props;

  const mfprop=(className===undefined?"":className);

  return (
    <div data-testid="modalfooter" {...rest}   className={`modal-footer ${mfprop}`}>
      {children}
    </div>
  );
}
//--------------------//

// Form Component
const Form=({ ...props })=> {
  const {
      children,
      ...rest
  } = props;

  return (
    <form data-testid="form" {...rest}>
      {children}
    </form>
  );
}
//--------------------//

// FormRow Component
const FormRow=({ ...props })=> {
  const {
    children,
    className,
      ...rest
  } = props;
  const frprop=(className===undefined?"":" "+className);
  return (
    <div data-testid="form-row" className={`form-row${frprop}`} {...rest}>
      {children}
    </div>
  
  );
}
//--------------------//

//FormGroup
const FormGroup=({ ...props })=> {
  const {
      className,
      children,
    ...rest

  } = props;

  //const customprop=(className===undefined?"":"custom-control "+className);
  const formgroup=(className===undefined?"":" "+className);

  return (
    <div {...rest} className={`form-group${formgroup}`}>
      {children}
    </div>
  );
}
//--------------------//

// Label Component
const Label=({ ...props })=> {
  const {
      htmlFor,
      children,
      ...rest

  } = props;

  return (
    <label data-testid="label" htmlFor={htmlFor} {...rest}>
      {children}
    </label>
  );
}
//--------------------//

//CustomCheckbox
const CustomCheckbox=({ ...props })=> {
  const {
      id, 
      labelName, 
      className,

      ...rest

  } = props;

  const customCheckbox=(className===undefined?"":" "+className);

  return (
    <div className={`custom-control custom-checkbox${customCheckbox}`} >
      <input type="checkbox" className="custom-control-input" id={id} {...rest}/>
      <label className="custom-control-label" htmlFor={id}>{labelName}</label>
    </div>
  );
}
//--------------------//


//CustomCheckbox
const CustomRadio=({ ...props })=> {
  const {
      id, 
      name,
      labelName,
      className,
      ...rest
  } = props;

  const customRadio=(className===undefined?"":className);

  return (
    <div className={`custom-control custom-radio ${customRadio}`}>
      <input type="radio" name={name} className="custom-control-input" id={id} {...rest}  />
      <label className="custom-control-label" htmlFor={id}>{labelName}</label>
    </div>
  );
}
//--------------------//


//CustomControl
const CustomControl=({ ...props })=> {
  const {
      className,
      children,
      
    ...rest

  } = props;

  const customCheckbox=(className===undefined?"":className);

  return (
    <div  {...rest} className={`custom-control ${customCheckbox}`}>
      {children}
    </div>
  );
}
//--------------------//


//CustomControl
const InvalidFeedback=({ ...props })=> {
  const {
      className,
      children,
      
    ...rest

  } = props;

  const invalidfeedback=(className===undefined?"":" "+className);

  return (
    <div {...rest} className={`invalid-feedback text-danger${invalidfeedback}`}>
      {children}
    </div>
  );
}
//--------------------//


// Input Component
const Input=({ ...props })=> {
  const {
      className,
      children,
      type,
      id,
      ...rest

  } = props;

  // switch (type) {
  //   case "text": 
  //             textprop= "form-control "+className;
  //             break;
  //   case "purple": 
  //   themeColor.removeAttribute("class");
  //   themeColor.classList.add("purple");
  //   break;
  //   case "red": 
  //   themeColor.removeAttribute("class");
  //   themeColor.classList.add("red");
  //   break;
  //   case "dark": 
  //   themeColor.removeAttribute("class");
  //   themeColor.classList.add('black');
  //   break;
  //   default:
  //   themeColor.removeAttribute("class");
  //   themeColor.classList.add('blue');
  //   break;
  // }

  // if (type === "text" && className !== undefined)
  // {
  //   textprop= "form-control " +className;
  // }
  // else if( (type === "checkbox" || type === "radio") && className !== undefined )
  // {
  //   textprop= "custom-control-input "+className;
  // }
  // else if ((type === "checkbox" || type === "radio") && className === undefined)
  // {
  //   textprop= "custom-control-input";
  // }  
  // else if( (type === "file" ) && className !== undefined )
  // {
  //   textprop= "custom-file-input "+className;
  // }
  // else if ((type === "file" ) && className === undefined)
  // {
  //   textprop= "custom-file-input";
  // }   
  // else 
  // { 
  //   textprop= "form-control";
  // }


  //  if (className === undefined && type !== "radio" && type !== "checkbox")
  // {
  //   textprop="form-control";
  // }  
  // else 
  // { 
  //   textprop= className;
  // } 
  

  return (
    <input data-testid="input" {...rest} type={type} id={id} className={className}>
      {children}
    </input>
  );
  
}
//--------------------//

// Textarea Component
const Textarea=({ ...props })=> {
  const {
      className,
      children,
      ...rest

  } = props;

  return (
    // <div className={typeprop}>
    <textarea data-testid="textarea" {...rest}  className={className}>
       {children}
    </textarea>
   
    // </div>
  );
}

// Header Component
const Navbar=({ ...props })=> {
  const {
      className,
      children,
      ...rest

  } = props;

const hdprop=(className===undefined?"navbar fixed-top navbar-dark bg-primary navbar-expand-md":"navbar "+className)

  return (
    <nav data-testid="navbar" {...rest}  className={hdprop}>
       {children}
    </nav>
  );
}

//--------------------//

// NavBarBrand  Component
const NavbarBrand=({ ...props })=> {
  const {
      className,
      children,
      full,
      ...rest

  } = props;

  const brandprop=(className===undefined?"":" "+className)

  return (
    <a title="Accord FMS" href="/" data-testid="" {...rest}  className={`navbar-brand ${brandprop}`}>
       <img src={full.src} alt={full.alt}/> 
    </a>
  
  );
}//-

//Navbar-nav
const NavbarNav=({...props})=>{
  const{
    className,
    children,
    ...rest
  } = props;
  const navbarnavprop=(className===undefined?"":" "+className)

  return (
    <ul data-testid="navbar-nav" {...rest}  className={`navbar-nav${navbarnavprop}`}>
       {children}
    </ul>
  );
}

// Nav  Component
const Nav=({ ...props })=> {
  const {
      className,
      children,
      ...rest

  } = props;

  const navprop=(className===undefined?"":" "+className)

  return (
    <ul data-testid="nav" {...rest}  className={`nav${navprop}`}>
       {children}
    </ul>
  );
}//--------------------//

// NavItem  Component
const NavItem=({ ...props })=> {
  const {
      className,
      children,
      ...rest

  } = props;

  const navitemprop=(className===undefined?"":" "+className)
  return (
    <li data-testid="navitem" {...rest} className={`nav-item${navitemprop}`}>
       {children}
    </li>
  );
}//--------------------//

// HeaderDropdown  Component
const HeaderDropdown=({ ...props })=> {
  const {
      className,
      children,
      ...rest

  } = props;

  const hdprop=(className===undefined?"":" "+className)
  return (
    <div data-testid="headerdropdown" {...rest} className={`dropdown${hdprop}`}>
       {children}
    </div>
  );
}//--------------------//

// DropdownToggle  Component
const DropdownToggle=({ ...props })=> {
  const {
      className,
      children,
      ...rest

  } = props;

  const dtprop=(className===undefined?"":" "+className)
  return (
    <div data-testid="dropdowntoggle" {...rest} className={`dropdown-toggle${dtprop}`}>
       {children}
    </div>
  );
}//--------------------//

// DropdownMenu Component
const DropdownMenu=({ ...props })=> {
  const {
      className,
      children,
      ...rest

  } = props;

  const dmprop=(className===undefined?"":" "+className)
  return (
    <div data-testid="dropdownmenu" {...rest} className={`dropdown-menu dropdown-menu-right${dmprop}`}>
       {children}
    </div>
  );
}//--------------------//

// DropdownItem Component
const DropdownItem=({ ...props })=> {
  const {
      className,
      children,
      ...rest

  } = props;

  const diprop=(className===undefined?"":" "+className)
  return (
    <div data-testid="dropdownitem" {...rest} className={`dropdown-item${diprop}`}>
       {children}
    </div>
  );
}//--------------------//

// Flex Component
const Flex=({ ...props })=> {
  const {
      className,
      children,
      ...rest

  } = props;

const flexprop=(className===undefined?"":" "+className)

  return (
    <div data-testid="header" {...rest}  className={`d-flex ${flexprop}`}>
       {children}
    </div>
  );
}

// Header Component
const Header=({ ...props })=> {
  const {
      className,
      children,
      ...rest

  } = props;

const footprop=(className===undefined?"":" "+className)

  return (
    <header data-testid="header" {...rest}  className={`header ${footprop}`}>
       {children}
    </header>
  );
}

// Footer Component
const Footer=({ ...props })=> {
  const {
      className,
      children,
      ...rest

  } = props;

const footprop=(className===undefined?"":" "+className)

  return (
    <footer data-testid="footer" {...rest}  className={footprop}>
       {children}
    </footer>
  );
}

  // Sidebar Component
  const Sidebar=({ ...props })=> {
    const {
        className,
        children,
        ...rest
  
    } = props;
  
  const sdprop=(className===undefined?"":" "+className)
  
    return (
      <aside data-testid="sidebar" {...rest}  className={`sidebar${sdprop}`}>
         {children}
      </aside>
    );
  }
  
  // NavIcon  Component
const Icon=({ ...props })=> {
  const {
      className,
      children,
      ...rest

  } = props;

  const iconprop=(className===undefined?"":" "+className)
  return (
    <i data-testid="Icon" {...rest} className={iconprop} aria-hidden="true">
       {children}
    </i>
  );
}//--------------------//

// Table  Component
const Table=({ ...props })=> {
  const {
      className,
      children,
      ...rest
 
  } = props;
 
  const tableprop=(className===undefined?"":" "+className);
 
  return (
    <table data-testid="table" {...rest} className={`table${tableprop}`} >
       {children}
    </table>
  );
}//--------------------//

//CustomSwitch
const CustomSwitch=({ ...props })=> {
  const {
      id, 
      name,
      labelName,
      className,
      ...rest
  } = props;

  const customSwitch=(className===undefined?"":className);
  return (
    <div className={`custom-control custom-switch ${customSwitch}`}>
      <input type="checkbox" name={name} className="custom-control-input" id={id} {...rest}  />
      <label className="custom-control-label" htmlFor={id}>{labelName}</label>
    </div>
  );
}
//--------------------//


export {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Container,
  Clearfix,
  ContainerFluid,
  Row,
  Col,
  Alert,
  Modal,
  ConfirmAlertModal, 
  AlertModal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  FormGroup,
  Form,
  Label,
  Input,
  Textarea,
  Navbar,
  NavbarNav,
  Nav,
  NavbarBrand,
  NavItem,
  HeaderDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Header,
  Footer,
  Flex,
  Sidebar,
  Image,
  CustomCheckbox,
  CustomControl,
  CustomRadio,
  InvalidFeedback,
  FormRow,
  Wrapper,
  ContentWrapper,
  Icon,
  Table,
  CustomSwitch,
  CustomFile,
  Link,
  SkinColor,
  //LeftSidebar,
}