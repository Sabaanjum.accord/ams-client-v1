
let converttimestamp=(timestamp)=> {     
    var d = new Date(timestamp * 1000), // Convert the passed timestamp to milliseconds
    yyyy = d.getFullYear(),
    mm = ('0' + (d.getMonth() + 1)).slice(-2),  // Months are zero based. Add leading 0.
    dd = ('0' + d.getDate()).slice(-2),     // Add leading 0.
    hh = d.getHours(),
    h = hh,
    min = ('0' + d.getMinutes()).slice(-2),   // Add leading 0.
    sec =('0' + d.getSeconds()).slice(-2),  
    ampm = 'AM',
    time;

    if (hh > 12) {
    h = hh - 12;
    ampm = 'PM';
    } 
    else if (hh === 12) {
    h = 12;
    ampm = 'PM';
    } 
    else if (hh === 0) {
    h = 12;
    }

    time = yyyy + '-' + mm + '-' + dd + ',' + h + ':' + min + ':' + sec +ampm;

    return time;
}
 
const StateList = [
    {label:"Arunachal Pradesh", value:"Arunachal Pradesh"},
    {label:"Andhara Pradesh", value:"Andhara Pradesh"},
    {label:"Assam", value:"Assam"},
    {label:"Bihar", value:"Bihar"},
    {label:"Chhattisgarh", value:"Chhattisgarh"},
    {label:"Goa", value:"Goa"},
    {label:"Gujarat", value:"Gujarat"},
    {label:"Haryana", value:"Haryana"},
    {label:"Himachal Pradesh", value:"Himachal Pradesh"},
    {label:"Jammu and Kashmir", value:"Jammu and Kashmir"},
    {label:"Jharkhand", value:"Jharkhand"},
    {label:"Karnataka", value:"Karnataka"},
    {label:"Kerala", value:"Kerala"},
    {label:"Madhya Pradesh", value:"Madhya Pradesh"},
    {label:"Maharashtra", value:"Maharashtra"},
    {label:"Manipur", value:"Manipur"},
    {label:"Meghalaya", value:"Meghalaya"},
    {label:"Mizoram", value:"Mizoram"},
    {label:"Nagaland", value:"Nagaland"},
    {label:"Odisha", value:"Odisha"},
    {label:"Punjab", value:"Punjab"},
    {label:"Rajasthan", value:"Rajasthan"},
    {label:"Tamil Nadu", value:"Tamil Nadu"},
    {label:"Telangana", value:"Telangana"},
    {label:"Tripura", value:"Tripura"},
    {label:"Uttarakhand", value:"Uttarakhand"},
    {label:"West Bengal", value:"West Bengal"},
    {label:"Andaman and Nicobar Islands", value:"Andaman and Nicobar Islands"},
    {label:"Chandigarh", value:"Chandigarh"},
    {label:"Dadra and Nagar Haveli", value:"Dadra and Nagar Haveli"},
    {label:"Daman and Diu", value:"Daman and Diu"},
    {label:"Delhi", value:"Delhi"},
    {label:"Lakshadweep", value:"Lakshadweep"},
    {label:"Pondicherry", value:"Pondicherry"},
];

const dateConverter=(sdate,edate)=>{
    var startdate = sdate;
   // console.log(new Date());
    var stime = new Date(startdate).toISOString().replace(/T[0-9]{1,2}:[0-9]{1,2}:[0-9]{1,2}\.[0-9]{1,3}Z/g, 'T00:00:00');
    stime = Math.floor(new Date(stime)) / 1000;
   // console.log("stime=",stime);
 
   var enddate = edate;
   var etime = new Date(enddate).toISOString().replace(/T[0-9]{1,2}:[0-9]{1,2}:[0-9]{1,2}\.[0-9]{1,3}Z/g, 'T23:59:59');
    etime = Math.floor(new Date(etime)) / 1000;
   // console.log("etime=",etime);
 
    return ([stime,etime]);
  }

const timeConverter=(UNIX_timestamp)=>{
var a = new Date(UNIX_timestamp * 1000);
var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
var year = a.getFullYear();
var month = months[a.getMonth()];
var date = a.getDate();
var hour = a.getHours();
var min = a.getMinutes();
var sec = a.getSeconds();
var time = date + ' ' + month + ' ' + year + ' ' + (hour < 10 ? "0"+hour : hour) + ':' + (min < 10 ? "0"+min : min) + ':' + (sec < 10 ? "0"+sec : sec) ;
return time;
}



export{
    converttimestamp,StateList, dateConverter, timeConverter
};


    
 
