import React, { Component } from 'react';

class CustomFile extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
 
    fileUpload =() =>{
        var v = document.getElementById(this.props.id).value;
        document.getElementById(this.props.id).nextSibling.innerHTML=v.replace(/^.*[\\\\/]/, '');
    } 
 
  render() { 
    const {
        id,
        name,
        className,
        children,
      ...rest
  
    } = this.props;
    const customFile=(className===undefined?"":" "+className);
      return ( 
        <div className={`custom-file${customFile}`}>
              <input {...rest} type="file" name={name} className="custom-file-input" id={id} onChange={this.fileUpload.bind(this)}/>
              <label className="custom-file-label" htmlFor={id}>Choose file</label>
          </div>
        );
    }
}
 
export default CustomFile;