
/*
*****************************************************************************
* License Information :Accord Global Technology Solutions Pvt. Ltd.                  *

*                      #72 & 73, Krishna Reddy Colony, Domlur layout,            *

*                      Domlur,Bangalore - 560071, INDIA                     *

*                      Licensed software and All rights reserved.           *

*****************************************************************************

* File             : 
*
* Description      : Left sidebare page component
*
* Author(s)        : Kavita Gupta
*
* Version History:
* <Version Number>                 <Author>              <date>      <defect Number>      <Modification
*                                                                                          made and the
*                                                                                          reason for
*                                                                                          modification >
*  1.0                             Kavita Gupta         20.11.2020        --         initial version
*
* References        :
*                     
* Assumption(s)     : None.
*                     
* Constraint(s)     : None.
*                     
 ****************************************************************************
*/

import React, { Component } from 'react';
import { Link, Icon, Button } from 'component/components';
 // sidebar navigation config
// import {navigation} from 'nav';
//import AuthHelper from 'azbycxjklg4k5jgh3';
import { adminNavigation } from 'roleBasedLogin/adminNavigation';
import { controllerNavigation } from 'roleBasedLogin/controllerNavigation';
import { viewerNavigation } from 'roleBasedLogin/viewerNavigation';

var navigation;

const roletype = window.localStorage.getItem('roletypeid');

console.log('role types',roletype);

switch (roletype) {
 
case '1':
    navigation = adminNavigation;
    break;
case '2':
    navigation = controllerNavigation;
    break;
case '3':
    navigation = viewerNavigation;
    break;

default:
    navigation=[]
    break;
}

class LeftSidebar extends Component {
    constructor(props) {
        super(props);
        this.state = {
          openMenuL2:[],
          openMenuL3:[],
          openMenuL1:{
            1:true
          },
        }

        this.wrapperRef = React.createRef();
        this.sidebarClickL1 = this.sidebarClickL1.bind(this);
        this.sidebarClickL2 = this.sidebarClickL2.bind(this);
        this.sidebarClickL3 = this.sidebarClickL3.bind(this);
        this.sidebarOutsideClick = this.sidebarOutsideClick.bind(this);
    }

    componentDidMount(){
      document.addEventListener('click', this.sidebarOutsideClick, false);   
    }
    componentWillUnmount(){
      document.removeEventListener('click', this.sidebarOutsideClick, false);   
    }

    //Outside click handle for left sidebar and left menu
    sidebarOutsideClick(event){
      if (this.wrapperRef && !this.wrapperRef.current.contains(event.target)) {
        this.setState({
          openMenuL2: [],
          openMenuL3: [],
        });
      }

       //Hide menu on dropdown item click 
       if(event.target.className !== null && 
        (event.target.className === "tree-item" || 
        event.target.className === "tree-item-span" || 
        event.target.classList.contains("tree-item-fa") )){
          this.setState({
            openMenuL2: [],
            openMenuL3: [],
          });
          //Global body left menu class update
          this.handleResize();
       }
        
      
    }

    handleResize = (e) => {
      if(window.innerWidth > 767){
          document.body.classList.add("sidebar-collapse");
      }else{
          document.body.classList.remove("sidebar-open");
      }
    };

    sidebarClickL1(index,e){   //1,2,3,4..
      let openMenuL1=[];
      console.log(index," ",openMenuL1);
      if(!this.state.openMenuL1[index]){
        openMenuL1[index]= !openMenuL1[index];
        this.setState({openMenuL1: openMenuL1});
        this.setState({openMenuL2: []});
        this.setState({openMenuL3: []});
      }else{
        this.setState({openMenuL1: []});
        this.setState({openMenuL2: []});
        this.setState({openMenuL3: []});
      }
      //console.log(openMenu);
    }

    sidebarClickL2(index,e){   // 1.1, 1.2, 1.3...
      let openMenuL2=[];
      console.log(index," ",openMenuL2);
      if(!this.state.openMenuL2[index]){
        openMenuL2[index]= !openMenuL2[index];
        this.setState({openMenuL2: openMenuL2});
        this.setState({openMenuL3: []});
      }else{
        this.setState({openMenuL2: []});
        this.setState({openMenuL3: []});
      }
      //console.log(openMenu);
    }

    sidebarClickL3(index,e){  //1.1.1, 1.1.2, 1.1.3 ....
      let openMenuL3=[];
      console.log(index," ",openMenuL3);
      if(!this.state.openMenuL3[index]){
        openMenuL3[index]= !openMenuL3[index];
        this.setState({openMenuL3: openMenuL3});
      }else{
        this.setState({openMenuL3: []});
      }
      //console.log(openMenu);
    }
  
  render() { 
    return ( 
           
  <aside className="main-sidebar" aria-label="Left sidebar">
   
  <section className="sidebar" ref={this.wrapperRef}>
  
    <ul className="sidebar-menu tree">
      <li className="header d-none d-md-block">
        <div className="row">
          <div className="col-12">
              <Button title="Sidebar toggler" className="sidebar-toggler" onClick={this.props.onClick}>
                <i className="fas fa-angle-double-left " style={{color:'black'}}></i>
              </Button>
          </div>
        </div>
      </li> 

      {navigation.map((item, indexi) => {
            
            if(item.children !== undefined )
            { 
              
                return (

                <li key={indexi} className={"treeview " +(this.state.openMenuL1[indexi+1]?"menu-open active":"")} >
                  <Link title={item.name} role="button" to="#" onClick={this.sidebarClickL1.bind(this,indexi+1)}>
                    <Icon className={item.icon}></Icon><span>{item.name}</span>
                    <span className="pull-right-container">
                      <Icon className="fa fa-angle-left pull-right " ></Icon>
                    </span>
                  </Link>
                  <ul className="treeview-menu">
                    {item.children.map((item, indexJ) => {
                      if(item.children !== undefined )
                      {
                        return (
                        <li key={parseInt(indexi+1)+"."+parseInt(indexJ+1)} className={"treeview " +(this.state.openMenuL2[parseInt(indexi+1)+"."+parseInt(indexJ+1)]?"menu-open":"")} >
                        <Link title={item.name} role="button" to="#" onClick={this.sidebarClickL2.bind(this,parseInt(indexi+1)+"."+parseInt(indexJ+1))}>
                          <Icon className={item.icon}></Icon><span>{item.name}</span>
                          <span className="pull-right-container">
                            <Icon className="fa fa-angle-left pull-right"></Icon>
                          </span>
                        </Link>

                        <ul className="treeview-menu">
                          {item.children.map((item, indexK) => {
                            if(item.children !== undefined )
                            {
                              return(
                                <li key={parseInt(indexi+1)+"."+parseInt(indexJ+1)+"."+parseInt(indexK+1)} className={"treeview " +(this.state.openMenuL3[parseInt(indexi+1)+"."+parseInt(indexJ+1)+"."+parseInt(indexK+1)]?"menu-open":"")} >
                                  <Link to="#" onClick={this.sidebarClickL3.bind(this,parseInt(indexi+1)+"."+parseInt(indexJ+1)+"."+parseInt(indexK+1))}>
                                    <Icon className={item.icon}></Icon><span>{item.name}</span>
                                    <span className="pull-right-container">
                                      <Icon className="fa fa-angle-left pull-right"></Icon>
                                    </span>
                                  </Link>
                                  <ul className="treeview-menu">
                                      {item.children.map((item, indexL) => {
                                        return(
                                          <li key={parseInt(indexi+1)+"."+parseInt(indexJ+1)+"."+parseInt(indexK+1)+"."+parseInt(indexL+1)}>
                                                
                                                {/* Remove this tree-item class, tree-item-fa and tree-item-span */}
                                                <Link className="tree-item" to={item.path} >
                                                  <Icon className={"tree-item-fa bg-dark "+item.icon} aria-hidden="true" style={{color:'black'}} />
                                                  <span className="tree-item-span">{item.name}</span>
                                                  {/* <span className="pull-right-container">
                                                  <Icon className="fa fa-angle-left pull-right"></Icon>
                                                  </span> */}
                                                </Link>
                                        </li>
                                        );
                                        
                                      })}
                                  </ul>
                              </li>
                              );  
                            }else{
                              return (
                                  <li className={(this.state.openMenuL3[indexi+1]?"active":"")} key={parseInt(indexi+1)+"."+parseInt(indexJ+1)+"."+parseInt(indexK+1)}>
                                    <Link className="tree-item" to={item.path} >
                                      <Icon className={"tree-item-fa "+item.icon} aria-hidden="true" />
                                      <span className="tree-item-span" >{item.name}</span>
                                    </Link>
                                  </li>
                              );
                            }
                          })}
                        </ul>
                      </li>
                        );   
                      }else{
                        return (
                            <li key={parseInt(indexi+1)+"."+parseInt(indexJ+1)} className={(this.state.openMenuL2[indexi+1]?"active":"")}>
                              <Link className="tree-item" to={item.path}>
                                <Icon className={"tree-item-fa "+item.icon} aria-hidden="true" />
                                <span className="tree-item-span" >{item.name}</span>
                              </Link>
                            </li>
                        );
                      }
                    })}
                        
                  </ul>
                </li>
                );
            }
            else{
                return (
                <li key={indexi} className={(this.state.openMenuL1[indexi+1]?"active":"")}>
                  <Link className="tree-item" title={item.name} to={item.path} >
                    <Icon className={"tree-item-fa "+item.icon} aria-hidden="true" />
                    <span className="tree-item-span">{item.name}</span>
                  </Link>
                </li>
                );
            }
            
            })}

      
    
      </ul>
  </section>
  {/* /.sidebar */}
</aside>
         );
    }
}
 
export default LeftSidebar;