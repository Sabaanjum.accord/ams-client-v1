
/*
*****************************************************************************
* License Information :Accord Global Technology Solutions Pvt. Ltd.                  *

*                      #72 & 73, Krishna Reddy Colony, Domlur layout,            *

*                      Domlur,Bangalore - 560071, INDIA                     *

*                      Licensed software and All rights reserved.           *

*****************************************************************************

* File             : config.js
*
* Description      : App Content page
*
* Author(s)        : Kavita Gupta
*
* Version History:
* <Version Number>                 <Author>              <date>      <defect Number>      <Modification
*                                                                                          made and the
*                                                                                          reason for
*                                                                                          modification >
*  1.0                             Kavita Gupta          20.11.2022        --         initial version
*
* References        :
*                     
* Assumption(s)     : None.
*                     
* Constraint(s)     : None.
*                     
 ****************************************************************************
*/
import React from 'react'
import { Navigate, Route, Routes } from 'react-router-dom'

// routes config
import routes from 'routes';

const AppContent = () => {
  return (
        <Routes>
          {routes.map((route, idx) => {
            return (
              route.element && (
                <Route
                  key={idx}
                  path={route.path}
                  exact={route.exact}
                  name={route.name}
                  element={<route.element />}
                />
              )
            )
          })}
          <Route path="/" element={<Navigate to="dashboard" replace />} />
        </Routes>
  )
}

export default AppContent;
