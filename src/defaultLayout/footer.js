
/*
*****************************************************************************
* License Information :Accord Global Technology Solutions Pvt. Ltd.                  *

*                      #72 & 73, Krishna Reddy Colony, Domlur layout,            *

*                      Domlur,Bangalore - 560071, INDIA                     *

*                      Licensed software and All rights reserved.           *

*****************************************************************************

* File             : config.js
*
* Description      : App Footer page
*
* Author(s)        : Kavita Gupta
*
* Version History:
* <Version Number>                 <Author>              <date>      <defect Number>      <Modification
*                                                                                          made and the
*                                                                                          reason for
*                                                                                          modification >
*  1.0                             Kavita Gupta          20.11.2022        --         initial version
*
* References        :
*                     
* Assumption(s)     : None.
*                     
* Constraint(s)     : None.
*                     
 ****************************************************************************
*/

import React, { Component } from 'react';
import {Footer, Row, Col, ContainerFluid} from "component/components";
//import logo from "../images/logo.svg";

class DefaultFooter extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        return ( 
            <div>
                  <Footer className="main-footer d-none d-sm-none d-md-block fixed-bottom">
                    <ContainerFluid>
                    <Row>
                        <Col className='col-sm-3 col-md-3 col-xl-3'>
                        </Col>
                        <Col className="col-sm-6 col-md-6 col-xl-6">
                            <center>
                        <b>Copyright © 2023 Accord Global Technology Solutions.</b>
                       
                        </center>
                        </Col>
                        {/* <Col className="col-md-3 text-muted text-right" >
                            Version R0.0.1
                        </Col> */}
                    </Row>
                    
                    </ContainerFluid>
                  </Footer>

                  <Footer className="main-footer d-block d-sm-block d-md-none">
                    <ContainerFluid>
                    <Row className="text-center">
                        <Col>
                        Copyright © 2020  <a href="https://accord-global.com">Accord-global.com</a>. 
                        </Col>
                    </Row>
                    
                    </ContainerFluid>
                  </Footer>
            </div>
         );
    }
}
 
export default DefaultFooter;