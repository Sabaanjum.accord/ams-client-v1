
/*
*****************************************************************************
* License Information :Accord Global Technology Solutions Pvt. Ltd.                  *

*                      #72 & 73, Krishna Reddy Colony, Domlur layout,            *

*                      Domlur,Bangalore - 560071, INDIA                     *

*                      Licensed software and All rights reserved.           *

*****************************************************************************

* File             : 
*
* Description      : App master page layout
* Author(s)        : Kavita Gupta
* Version History:
* <Version Number>                 <Author>              <date>      <defect Number>      <Modification
*                                                                                          made and the
*                                                                                          reason for
*                                                                                          modification >
*  1.0                             Kavita Gupta          25.10.2022        --         initial version
*
* References        :
*                     
* Assumption(s)     : None.
*                     
* Constraint(s)     : None.
*                     
 ****************************************************************************
*/
import React, { Suspense } from 'react';
import { Wrapper, SkinColor,
         ContentWrapper, } from 'component/components';
        

import DefaultFooter from './footer';
import AppContent from './AppContent';
import AuthHelper from "azbycxjklg4k5jgh3";

//Importing 
const  DefaultHeader = React.lazy(() => import('./header'));
const LeftSidebar = React.lazy(() => import('./leftsidebar'));
let body = document.body;
class DefaultLayout extends AuthHelper {
    constructor(props) {
        super(props);
        this.state = {  }

        this.contentWrapperRef = React.createRef();
        
        body.classList.add("sidebar-mini");
        if(window.innerWidth > 767){
            body.classList.add("sidebar-collapse");
        }
        this.sidebarHandle = this.sidebarHandle.bind(this);
    };

    componentDidMount(){

        //login check
      /*   if(!this.loggedIn()){
                window.location.replace("/login");
        } */

        //click event
        document.addEventListener('click', this.contentWrapperClick, false);              
    }

    componentWillUnmount(){
        document.removeEventListener('click', this.contentWrapperClick, false);
    }

    contentWrapperClick=(event)=>{
               //console.log(event.target)
                if (this.contentWrapperRef && this.contentWrapperRef.current.contains(event.target)) {
                    this.handleResize();
                }
    }

    

    sidebarHandle=()=>{
        var sidebarCollapse = "sidebar-collapse";
        var sidebarExpanded = "sidebar-expanded-on-hover";
        if(window.innerWidth > 767){
            body.classList.remove("sidebar-open");
            if(body.classList.contains(sidebarCollapse)){
                body.classList.remove(sidebarCollapse);
                //body.classList.add(sidebarExpanded);
            }   
            else{
                body.classList.add(sidebarCollapse);
                body.classList.remove(sidebarExpanded);
            }
                
        }else{
            body.classList.remove(sidebarCollapse);
            if(body.classList.contains('sidebar-open')){
                body.classList.remove("sidebar-open");
            }
            else{
                body.classList.add("sidebar-open");
            }
        }

    }


loadingHeader = () => <div className="animated fadeIn pt-1 text-center"></div>
loadingBody = () => 
    <div className="animated fadeIn pt-1 text-center">
        <div className="circle-loader"></div>
    </div>
    render() { 
        return ( 
            <Wrapper>
             <Suspense fallback={this.loadingHeader()}>
                    {/* Application Header component */}
                    <DefaultHeader onClick={()=>this.sidebarHandle()} />
                    {/* Application Left Sidebar component */}
                    <LeftSidebar onClick={()=>this.sidebarHandle()}/>
                    {/* Application theme skin color change component */}
                    <SkinColor/>
                </Suspense>

                {/* App main body section */}
                <ContentWrapper onClick={()=>this.contentWrapperClick(this)} >
                    <main role="main" >
                    <div ref={this.contentWrapperRef}>
                    <Suspense fallback={this.loadingBody()}>
                       <AppContent/>
                    </Suspense>
                    </div>
                    </main>
                </ContentWrapper>
                <DefaultFooter />
            </Wrapper>

         );
    }
}
 
export default DefaultLayout;
