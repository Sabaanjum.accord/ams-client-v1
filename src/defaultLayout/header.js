


/*
*****************************************************************************
* License Information :Accord Global Technology Solutions Pvt. Ltd.                  *

*                      #72 & 73, Krishna Reddy Colony, Domlur layout,            *

*                      Domlur,Bangalore - 560071, INDIA                     *

*                      Licensed software and All rights reserved.           *

*****************************************************************************

* File             : config.js
*
* Description      : App header page
*
* Author(s)        : Kavita Gupta
*
* Version History:
* <Version Number>                 <Author>              <date>      <defect Number>      <Modification
*                                                                                          made and the
*                                                                                          reason for
*                                                                                          modification >
*  1.0                             Kavita Gupta          20.11.2022        --         initial version
*
* References        :
*                     
* Assumption(s)     : None.
*                     
* Constraint(s)     : None.
*                     
 ****************************************************************************
*/

import React, { Component } from 'react';
import {  useHistory } from 'react-router-dom';
//import Utility from 'component/component-util';
// import Offcanvas from "react-bootstrap/Offcanvas";

import {Navbar, 
    // Button,Flex,
    NavbarBrand,
    NavItem,
    NavbarNav,
    Header,Icon,
    Image, Link,
    Row,Col,
    Form,
    Input, 
    Label,
    InvalidFeedback,
    FormGroup,
    Button,
    Alert,
    AlertModal,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
} from 'component/components';
import 'App.css';
import { baseURL } from 'config/config';
// import { FaCookie } from 'react-icons/fa';
//import {decrypt} from "config/config";
//import AuthHelper from 'azbycxjklg4k5jgh3';
// import Language from 'MultipleLanguage/Language';
// import cookies from 'react-cookies';


class AppHeader extends Component{
    constructor(props) {
        super(props);
        this.state = { 
            showMenu:[],
            showMenuForHouse:[],
            feature:[],
            LogoutModal:false,
            logoutErrMsg:'',
            profileName:'',
            homeName:'',
            userProfileImage: '',
            HomeImage: '',
            userType:'Guest', 
            showmodal:false,
            fields: {
                oldPass: '',
                newPass: '',
                confirmPass: '',
            },
            newPassError:false,
            oldPassError:false,
            confirmPassError:false,
            updatepass_alert:false,
            updatepass_error:'',
            updatepass_alert_type:'primary',
            update_loader:'none',
            NewPassErrMsg:'',
            ConfirmPassErrMsg:'',
            setNameList:[],
            //statve variables releated to alert
            show: false,
            navDisplay:'none',
            emptyNavDisplay:'none',
            todayNavDisplay:'none',
            read_count:0,
            notification_count:0,
            notifications:[],
            danger_count:0,
            warning_count:0,
            info_count:0,
            success_count:0,
            total_cnt:0,
            stime:'',
            etime:''
        };

        this.navOutsideRef = React.createRef();
        this.showDropdown = this.showDropdown.bind(this);
        this.getAllHomesByUserId = this.getAllHomesByUserId.bind(this);
        this.showDropdownForHouse = this.showDropdownForHouse.bind(this);
        this.handleItemClick = this.handleItemClick.bind(this);
        
        this.showDropdownOutsideClick = this.showDropdownOutsideClick.bind(this);
        this.showDropdownOutsideClickForHouse = this.showDropdownOutsideClickForHouse.bind(this);

        this.handleClose = this.handleClose.bind(this);
        this.handleShow = this.handleShow.bind(this);

        // showDropdownOutsideClickForHouse
    }

    componentDidMount(){
        // "D:\Gitlab_project\ihass-smart-home-client\src\images\home-icon.jpg"
        var pImage =  require('images/avatar_image.jpg');
        this.setState({userProfileImage:pImage});
        // this.getAllHomesByUserId();
       
        var hImage =  require('images/Accord_logo_white_notag_192.png');
        this.setState({HomeImage:hImage});

        this.setuserType('Guest');
     
        document.addEventListener('click', this.showDropdownOutsideClick, false);  
        document.addEventListener('click', this.showDropdownOutsideClickForHouse, false); 
        //this.setState({profileName: "Ambika Nayak" });
        this.setState({profileName: <h7>{localStorage.getItem('username')}</h7> }); 
        this.setState({homeName: <h7>{localStorage.getItem('homename')}</h7> });
        
    }

    handleClose() {
        this.setState({ show: false });
      }
    
      handleShow() {
        this.setState({ show: true });
      }

    getuserType(userType){
        switch(parseInt(userType)){
        case 1:
            return 'CRM Admin';
        case 2:
            return 'Fleet Owner';
        case 3:
            return 'Delaer';
        case 4:
            return 'Manager';
        case 99:
            return 'Master';
        default:
            return 'Guest';
        }
    }
    
    setuserType(userType){
        userType = parseInt(userType);
        switch(userType){
        case 1:
            this.setState({userType: 'CRM Admin'});
            break;
        case 2:
            this.setState({userType: 'Fleet Owner'});
            break;
        case 3:
            this.setState({userType: 'Delaer'});
            break;
        case 4:
            //this.setLoggedinType(userType);
            this.setState({userType: 'Manager'});
            break;
        case 99:
            // this.setLoggedinType(userType);
            this.setState({userType: 'Master'});
            break;
                 
        default:
            //this.setLoggedinType(userType);
            this.setState({userType: 'Guest'});
            break;
        }
    }

    componentWillUnmount(){
        this.setState({feature:[]});
        document.removeEventListener('click', this.showDropdownOutsideClick, false);   
        document.removeEventListener('click', this.showDropdownOutsideClickForHouse, false);   
    }

    showDropdown(i,event){
        event.preventDefault();
        let showMenu=[];
        if(!this.state.showMenu[i]){
            showMenu[i]= !showMenu[i];
            this.setState({showMenu: showMenu});
        }else{
            this.setState({showMenu: []});
        }
      
    }

    // showMenuForHouse

    showDropdownForHouse(i,event){
        event.preventDefault();
        this.getAllHomesByUserId();
        let showMenuForHouse=[];
        if(!this.state.showMenuForHouse[i]){
            showMenuForHouse[i]= !showMenuForHouse[i];
            this.setState({showMenuForHouse: showMenuForHouse});
        }else{
            this.setState({showMenuForHouse: []});
        }
      
    }

    showDropdownOutsideClick(event){
        if (this.navOutsideRef  && !this.navOutsideRef.current.contains(event.target)) {
            this.setState({
                showMenu: []
            });
        }

        //Hide menu on dropdown item click 
        if(event.target.className === 'dropdown-item' || event.target.getAttribute('data-click') === 'link'){
            this.setState({
                showMenu: []
            });
        }
      
    }

    showDropdownOutsideClickForHouse(event){
        if (this.navOutsideRef  && !this.navOutsideRef.current.contains(event.target)) {
            this.setState({
                showMenuForHouse: []
            });
        }

        //Hide menu on dropdown item click 
        if(event.target.className === 'dropdown-item' || event.target.getAttribute('data-click') === 'link'){
            this.setState({
                showMenuForHouse: []
            });
        }
      
    }

    //show modal to update password
    handleShowModal=()=>{
        this.setState(prevState => ({
            showmodal: !prevState.showmodal
        }));
    };

    // create group form input onchange function    
    changeHandler = (field,event )=> {
        this.onDismiss('updatePassword');
        let fields = this.state.fields;
        switch (field){
        case 'newPass':
            fields['newPass'] = event.target.value;
            this.setState({newPassError: false});
            break;
        case 'oldPass':
            fields['oldPass'] = event.target.value;
            this.setState({oldPassError: false});
            break;
        case 'confirmPass':
            fields['confirmPass'] =event.target.value;
            this.setState({confirmPassError: false});
            break;
        default:
            break;
        }
        this.setState({fields});
    };


    //Filed Validation for Password Update
    handleValidation(){

        this.onDismiss('updatepass_alert');
        let formIsValid = true;
        // var strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^\\&*])(?=.{8,})");
        var strongRegex = new RegExp('^[A-Za-z]{5,10}$');
        let newPass = this.state.fields.newPass,
            oldPass = this.state.fields.oldPass,
            confirmPass = this.state.fields.confirmPass;
  
        //oldPass
        if(oldPass === undefined || (oldPass).trim()===''){
            formIsValid = false;
            this.setState({oldPassError: true});
        }

        //newpassword
        if(newPass === undefined || (newPass).trim() ===''){
            formIsValid = false;
            this.setState({newPassError: true,NewPassErrMsg:'New Password field is empty or invalid'});
        }

        //newpassword length validation
        if((newPass).trim() !=='')
        {
            if(newPass.length<5)
            {
                formIsValid = false; //Please enter at least 8 character
                this.setState({newPassError: true, NewPassErrMsg:'Must contain atleast 5 character'});
            }
            //Password Pattern validation
            // if(strongRegex.test(newPass)){}
            // else{
            //   formIsValid = false; //should contain at least one uppercase, lowercase, number and symbol
            //   this.setState({newPassError: true,NewPassErrMsg:"Must contain at least one uppercase, lowercase, number and symbol"});
            // }
        }
     
        //confirmPass
        if(confirmPass === undefined || (confirmPass).trim() ===''){
            formIsValid = false;
            this.setState({confirmPassError: true,ConfirmPassErrMsg:'Confirm Password field is empty or invalid'});
        }

        //confirmPass length validation
        if((confirmPass).trim() !=='' && newPass.length<4)
        {
            formIsValid = false; //Please enter at least 8 character
            this.setState({newPassError: true,ConfirmPassErrMsg:'Must contain atleast 5 character'});
        }
      
        return formIsValid;
    }


    //api call to update password
    handleUpdatePassword=(event)=>{
        event.preventDefault();
        if(this.handleValidation()){
            if(this.state.fields.newPass.trim()!==this.state.fields.confirmPass.trim())
            {
                this.setState({updatepass_error:'Confirm password does not match!'});
                this.setState({updatepass_alert:true,updatepass_alert_type:'danger'});
            }
            else 
            {
                console.log('etyuhtgh');
                fetch(baseURL+'updatepassword',{
                    method: 'PATCH',
                    body : JSON.stringify({
                        'username':sessionStorage.getItem('Pname'),
                        'password':this.state.fields.newPass,
                    }),
                    headers: {
                        'Content-Type' : 'application/json',
                        'Authorization':`Bearer ${localStorage.getItem('token')}`
                    }
                })
                    .then((response)=>response.json())
                    .then((responseJson)=>{
                        console.log('sendata response=',responseJson);
                        if(responseJson.status===1)
                        {
                            alert('Updates Successfully');
               
                        }
                        else if(responseJson.status===0){
                            alert(responseJson.data);
                  
                        }
                    })
                    .catch((error)=>{
                        console.log('error=',error);
                    }); 
            }
        }
    };    

    // Update password alert/error dismiss
    onDismiss=(value)=>{
        switch (value) {
        case 'updatepass_alert':this.setState({updatepass_alert: false,updatepass_alert_type:'primary'});
            break;

        case 'modalclose':
            this.setState({showmodal: false});
            this.handleModalClear();
            break;
    
        default:
            break;
        }
    };
  
    //to clear all the field value and error message on modal close click
    handleModalClear=()=>{
        let fields = this.state.fields;
        fields['newPass'] = '';
        fields['oldPass'] = '';
        fields['confirmPass'] = '';

        this.setState({ 
            newPassError:false,
            oldPassError:false,
            confirmPassError:false,
            updatepass_alert:false,
            NewPassErrMsg:'',
            ConfirmPassErrMsg:''
        });

    }; 
    getAllHomesByUserId() {
        fetch(baseURL + 'getallhomesbyuserid', {
            method: 'POST',
            body: JSON.stringify({
               "userid":localStorage.getItem('userid')
            }),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then(
                (response) => response.json()
            ).then((responseJson) => {
                console.log("response date",responseJson)
                if (responseJson.status == 1) {
                    // localStorage.setItem('homename',responseJson.home)
                    var temp = [];
                    
                    // console.log("home id displayed",localStorage.getItem('homeid'));
                    for (let i = 0; i < responseJson.data.length; i++) {
                        temp.push({
                            homeid: responseJson.data[i].home.homeid,
                            homename: responseJson.data[i].home.homename,
                            // userid: responseJson.data[i].user.userid,
                            // username: responseJson.data[i].user.username,
                            // firstname: responseJson.data[i].user.firstname
                
                        });
                    }
                    
                }
              
                this.setState({ showMenuForHouse: temp }, () => {
                    console.log('check drop down', this.state.showMenuForHouse);
                });
          
        
            });
    }


     handleItemClick = (homeid,homename) => {
        console.log("welcome",homename)
        localStorage.setItem('homeid', homeid);
        // this.setState({homeName:homename});
        localStorage.setItem('homename', homename);
        this.setState({ homeName: homename }, () => {
            console.log('check drop down', this.state.homeName);
        });
        window.location.replace("/dashboard");
    }


    //to remove badge and set read count to 0
    handleNotification()
    {
        this.setState({showBadge:'none'});
        this.setState({read_count:0});
    }

    clearStorage = () =>{
        localStorage.removeItem('token');
        // localStorage.removeItem("l_name");
        // localStorage.removeItem("e_");
        // localStorage.removeItem("c_");
        sessionStorage.clear();
    };
  
    logout()   
    {   
        //alert("Logged out - No data found!");
        //this.clearStorage();
        localStorage.removeItem('token');
        localStorage.removeItem('homename');
        // cookies.remove('username');
        window.location.replace('/login');
    }
  


    render() { 
        return ( 
            <Header>
                <Navbar className="fixed-top navbar-expand-md" >
                    {/* <button title="Sidebar toggler" className="d-block d-md-none nav-sidebar-toggler" 
                        onClick={this.props.onClick} 
                        ref={this.props.refToggler}>
                        <Icon className="fas fa-bars" aria-hidden="true"></Icon>
                    </button> */}

                    <NavbarBrand full={{src: require('images/Accord_logo_notag_192.png'), alt:'Accord'}}>Saba</NavbarBrand> 
                   
                    <div className="d-flex flex-row ml-auto" ref={this.navOutsideRef}>
                        <NavbarNav className="flex-row">
                             {/* <NavItem className="nav-item">
                            <Link className="nav-link px-2" to="#">Menu1</Link></NavItem>  */}
                          
                           

                            {/* <NavItem className={"dropdown"+(this.state.showMenu[0]?" show":"")} onClick={this.handleNotification.bind(this)}>
                              <Link title="Notification alert" to="#" className="nav-link" role="button" 
                              onClick={this.showDropdown.bind(this,0)}
                              data-toggle="dropdown" aria-haspopup="true" aria-expanded={(this.state.showMenu[0]?"true":"false")}>
                                <i className="fas fa-bell" aria-hidden="true"></i>
                                <span className="badge badge-dark" style={{display:"block"}}>{79}</span>
                              </Link> */}
                            {/*  Alert List*/}
                            {/* <div style={{display: "block"}} >
                              <div className={"dropdown-menu dropdown-menu-right "+(this.state.showMenu[0]?"show":"")} >
                                <Link data-click="link" className="dropdown-item text-center dropdown-header" to="#">
                                 <strong> {79} New Notification</strong>
                                  </Link>
                                <Link data-click="link" className="dropdown-item" to="#">
                                <i  className="fa fa-exclamation-circle text-danger mr-4" aria-hidden="true"></i>Critical Alerts <span className="float-right"> {4}</span>
                                </Link>
                                <Link data-click="link" className="dropdown-item" to="#">
                                <i  className="fa fa-exclamation-triangle text-warning mr-4" aria-hidden="true"></i>Warning Alerts <span className="float-right">{20}</span>
                                </Link>
                                <Link  data-click="link" className="dropdown-item" to="#">
                                <i  className="fa fa-check-circle text-success mr-4 " aria-hidden="true"></i>Success Alerts <span className="float-right">{50}</span>
                                </Link>
                                <Link data-click="link" className="dropdown-item" to="#">
                                <i  className="fa fa-info-circle text-primary mr-4" aria-hidden="true"></i>Info Alerts <span className="float-right">{5}</span>
                                </Link>
                                <div className="dropdown-divider"></div>
                                <Link data-click="link" className="dropdown-item text-center" to="#"><strong data-click="link">View All Messages</strong></Link>
                              </div>
                              </div> */}
                            {/* Today Alerts */}  
                            {/* <div style={{ display: "none"}} >
                                <div className={"dropdown-menu dropdown-menu-right "+(this.state.showMenu[0]?"show":"")} >
                                  <Link data-click="link" className="dropdown-item text-center" to="/#"><strong> -- Today Alerts --</strong></Link>
                                </div>
                             </div> */}
                            {/* No Alerts */} 
                            {/* <div style={{ display: this.state.emptyNavDisplay}} >
                            <div className={"dropdown-menu dropdown-menu-right "+(this.state.showMenu[0]?"show":"")} >
                                  <Link data-click="link" className="dropdown-item text-center" to="/#"><strong> -- No Alerts --</strong></Link>
                            </div>
                          </div>
                          </NavItem> */}

                            {/* <NavItem>
                              <Link title="Notification Mail" className="nav-link" to="#" onClick={this.showDropdown.bind(this,3)}>
                              <i className="fas fa-envelope" aria-hidden="true"></i>
                              <span className="badge badge-dark">12</span>
                              </Link>
                            </NavItem> */}
{/* <Language/> */}
                            {/* {this.state.feature.length > 1 ?  
                                <NavItem className={'dropdown'+(this.state.showMenu[2]?' show':'')}>
                                    <Link title="Your Roles" to="#" className="nav-link" role="button" 
                                        onClick={this.showDropdown.bind(this,2)}
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded={(this.state.showMenu[2]?'true':'false')}>
                                        <i className="fas fa-users-cog" aria-hidden="true"></i>
                                        <span className="badge badge-dark">{this.state.feature.length}</span>
                                    </Link>

                                    <div className={'dropdown-menu dropdown-menu-right '+(this.state.showMenu[2]?'show':'')} >
                               
                                        {this.state.feature.map((a, index) => {
                                            return (
                                                <Link key={index} data-click="link" className="dropdown-item" to="#" onClick={this.setuserType.bind(this,a)}>{this.getuserType(a)}</Link>
                                            );
                                        })}
                               
                                        <div className="dropdown-divider"></div>
                                        <Link data-click="link" className="dropdown-item text-muted" to="#">You're Now: {this.state.userType}</Link>
                                    </div>
                                </NavItem>
                                :''} */}

                                        


                           
                                  
                                
                                


 
                            <Link title="Notification alert" to="#" className="nav-link" role="button" 
                              onClick={this.showDropdown.bind(this,0)}
                              data-toggle="dropdown" aria-haspopup="true" aria-expanded={(this.state.showMenu[0]?"true":"false")}>
                                <i className="fas fa-bell" aria-hidden="true"></i>
                                <span className="badge badge-dark"></span>
                              </Link>
                            <NavItem className={'dropdown'+(this.state.showMenu[1]?' show':'')}>
                                
                                <Link to="#" className="nav-link" role="button" 
                                    onClick={this.showDropdown.bind(this,1)}
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded={(this.state.showMenu[1]?'true':'false')}>
                                    <Image src={this.state.userProfileImage} className="user-image" alt="User Profile" />
                                    <span className="d-none d-sm-inline">{this.state.profileName}</span>
                                </Link>
                                <div className={'dropdown-menu dropdown-menu-right '+(this.state.showMenu[1]?'show':'')}>
                                    {/* <Link data-click="link" className="dropdown-item text-center profile" to="/profile">
                                <Image data-click="link" src={this.state.userProfileImage} className="img-thumbnail rounded-circle mx-auto d-block" height="120px" alt="User Profile" />
                                  <div data-click="link" className="profile_name"><strong data-click="link">{this.state.profileName}</strong></div>
                                  <div data-click="link" className="profile_role">{this.state.userType}</div>
                                </Link> */}
                                    <Link data-click="link" className="dropdown-item" to="/profile">
                                        <i data-click="link" className="fas fa-user" aria-hidden="true"></i> Profile
                                    </Link>
                                    {/* <Link data-click="link" className="dropdown-item" to="/updatepassword"  >
                                        <i data-click="link" className="fas fa-lock" aria-hidden="true"></i> Update Password
                                    </Link> */}
                                    <div className="dropdown-divider"></div>
                                    <div className="text-center">
                                        <Link data-click="link" className="btn btn-outline-dark btn-block" onClick={this.logout.bind(this)} to="#">
                                            <i data-click="link" className="fas fa-sign-out-alt" aria-hidden="true"></i> Logout
                                        </Link>
                                    </div>
                                
                                </div>
                            </NavItem>
                           {/* {console.log("name",localStorage.getItem('homename'))} */}

                      

                        </NavbarNav>
                        
                    </div>
                
                </Navbar>
                
               
                
                {/*modal to update password */}
                <Modal show={this.state.showmodal}>
                    <ModalHeader onClose={this.onDismiss.bind(this,'modalclose')}>
                        <h3 className="modal-title"><strong>Update Password</strong></h3>
                    </ModalHeader>
                    <div style={{display:this.state.update_loader}} className="loader"></div>
                    <ModalBody >
                        {(window.innerWidth < 768 ? 
                            <div>
                                <Col className="col-md-12">
                                    <AlertModal show={this.state.updatepass_alert} type={this.state.updatepass_alert_type} 
                                        onOk={this.onDismiss.bind(this,'updatepass_alert')} 
                                        onClose={this.onDismiss.bind(this,'updatepass_alert')}>
                                        <p className="text-center">{this.state.updatepass_error}</p>
                                    </AlertModal>   
                                </Col>
                            </div>:
                            <div>
                                <Alert type={this.state.updatepass_alert_type} className="text-center"
                                    isOpen={this.state.updatepass_alert} onClick={this.onDismiss.bind(this,'updatepass_alert')}>
                                    {this.state.updatepass_error}
                                </Alert>
                            </div>
                        )}  
                        <Form>
                            <Row>
                                <Col className="col-md-12 col-lg-12 col-xl-12" >
                               
                                    <FormGroup>
                                        <Label  htmlFor="Old Password">Old Password </Label>
                                        <Input type="password" onChange={this.changeHandler.bind(this,'oldPass')} value={this.state.fields.oldPass} 
                                            className={'mb-3 form-control'+(this.state.oldPassError?' error':'')} required=""/>
                                        <InvalidFeedback style={{display: (this.state.oldPassError?'block':'none')}}>
                                                Old Password field is empty or invalid
                                        </InvalidFeedback>
                                    </FormGroup>
                                   
                                    <FormGroup>
                                        <Label  htmlFor="New Password">New Password </Label>
                                        <Input type="password" onChange={this.changeHandler.bind(this,'newPass')} value={this.state.fields.newPass} 
                                            className={'mb-3 form-control'+(this.state.newPassError?' error':'')} required=""/>
                                        <InvalidFeedback style={{display: (this.state.newPassError?'block':'none')}}>
                                            {this.state.NewPassErrMsg}
                                        </InvalidFeedback>
                                    </FormGroup>
                               
                                    <FormGroup>
                                        <Label  htmlFor="Confirm Password">Confirm Password</Label>
                                        <Input type="password" onChange={this.changeHandler.bind(this,'confirmPass')} value={this.state.fields.confirmPass} 
                                            className={'mb-3 form-control'+(this.state.emailError?' error':'')} required=""/>
                                        <InvalidFeedback style={{display: (this.state.confirmPassError?'block':'none')}}>
                                            {this.state.ConfirmPassErrMsg}
                                        </InvalidFeedback>
                                    </FormGroup>
                                     
                                </Col>
                            </Row>
                        </Form>  
                    </ModalBody>
                    <ModalFooter>
                        <Row >
                            <Col className="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                                <Button type="button" className="btn btn-outline-primary btn-block" 
                                    onClick={this.onDismiss.bind(this,'modalclose')}>Close</Button>
                            </Col>
                            <Col className="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                                <Button type="button" className="btn btn-primary btn-block" 
                                    onClick={this.handleUpdatePassword.bind(this)}>Update</Button>
                            </Col>
                        </Row>
                    </ModalFooter>
                </Modal>



               


              
                {/*modal to show logount not success*/} 
                <AlertModal type="danger" show={this.state.LogoutModal}
                    onOk={() => { this.setState({LogoutModal:false});}}
                    onClose={() => { this.setState({LogoutModal:false});}}>
                    {this.state.logoutErrMsg}
                </AlertModal>

                <div style={{display:'none'}} className="loader" id="loader"></div>
            </Header>
           
        );
    }
}
 
export default AppHeader;





// /*
// *****************************************************************************
// * License Information :Accord Global Technology Solutions Pvt. Ltd.                  *

// *                      #72 & 73, Krishna Reddy Colony, Domlur layout,            *

// *                      Domlur,Bangalore - 560071, INDIA                     *

// *                      Licensed software and All rights reserved.           *

// *****************************************************************************

// * File             : config.js
// *
// * Description      : App header page
// *
// * Author(s)        : Kavita Gupta
// *
// * Version History:
// * <Version Number>                 <Author>              <date>      <defect Number>      <Modification
// *                                                                                          made and the
// *                                                                                          reason for
// *                                                                                          modification >
// *  1.0                             Kavita Gupta          20.11.2022        --         initial version
// *
// * References        :
// *                     
// * Assumption(s)     : None.
// *                     
// * Constraint(s)     : None.
// *                     
//  ****************************************************************************
// */

// import React, { Component } from 'react';
// import {  useHistory } from 'react-router-dom';
// import Utility from 'component/component-util';
// import {Navbar, 
//     // Button,Flex,
//     NavbarBrand,
//     NavItem,
//     NavbarNav,
//     Header,Icon,
//     Image, Link,
//     Row,Col,
//     Form,
//     Input, 
//     Label,
//     InvalidFeedback,
//     FormGroup,
//     Button,
//     Alert,
//     AlertModal,
//     Modal,
//     ModalHeader,
//     ModalBody,
//     ModalFooter,
// } from 'component/components';
// import { baseURL } from 'config/config';
// import { FaCookie } from 'react-icons/fa';
// //import {decrypt} from "config/config";
// //import AuthHelper from 'azbycxjklg4k5jgh3';
// // import Language from 'MultipleLanguage/Language';
// import cookies from 'react-cookies';


// class AppHeader extends Component{
//     constructor(props) {
//         super(props);
//         this.state = { 
//             showMenu:[],
//             showMenuForHouse:[],
//             feature:[],
//             LogoutModal:false,
//             logoutErrMsg:'',
//             profileName:'',
//             homeName:'',
//             userProfileImage: '',
//             HomeImage: '',
//             userType:'Guest', 
//             showmodal:false,
//             fields: {
//                 oldPass: '',
//                 newPass: '',
//                 confirmPass: '',
//             },
//             newPassError:false,
//             oldPassError:false,
//             confirmPassError:false,
//             updatepass_alert:false,
//             updatepass_error:'',
//             updatepass_alert_type:'primary',
//             update_loader:'none',
//             NewPassErrMsg:'',
//             ConfirmPassErrMsg:'',
//             setNameList:[],
//             //statve variables releated to alert
          
//             navDisplay:'none',
//             emptyNavDisplay:'none',
//             todayNavDisplay:'none',
//             read_count:0,
//             notification_count:0,
//             notifications:[],
//             danger_count:0,
//             warning_count:0,
//             info_count:0,
//             success_count:0,
//             total_cnt:0,
//             stime:'',
//             etime:''
//         };

//         this.navOutsideRef = React.createRef();
//         this.showDropdown = this.showDropdown.bind(this);
//         this.getAllHomesByUserId = this.getAllHomesByUserId.bind(this);
//         this.showDropdownForHouse = this.showDropdownForHouse.bind(this);
//         this.handleItemClick = this.handleItemClick.bind(this);
        
//         this.showDropdownOutsideClick = this.showDropdownOutsideClick.bind(this);
//         this.showDropdownOutsideClickForHouse = this.showDropdownOutsideClickForHouse.bind(this);

//         // showDropdownOutsideClickForHouse
//     }

//     componentDidMount(){
//         // "D:\Gitlab_project\ihass-smart-home-client\src\images\home-icon.jpg"
//         var pImage =  require('images/avatar_image.jpg');
//         this.setState({userProfileImage:pImage});
//         // this.getAllHomesByUserId();
       
//         var hImage =  require('images/Accord_logo_white_notag_192.png');
//         this.setState({HomeImage:hImage});

//         this.setuserType('Guest');
     
//         document.addEventListener('click', this.showDropdownOutsideClick, false);  
//         document.addEventListener('click', this.showDropdownOutsideClickForHouse, false); 
//         //this.setState({profileName: "Ambika Nayak" });
//         this.setState({profileName: <h7>{localStorage.getItem('username')}</h7> }); 
//         this.setState({homeName: <h7>{localStorage.getItem('homename')}</h7> });
        
//     }

//     getuserType(userType){
//         switch(parseInt(userType)){
//         case 1:
//             return 'CRM Admin';
//         case 2:
//             return 'Fleet Owner';
//         case 3:
//             return 'Delaer';
//         case 4:
//             return 'Manager';
//         case 99:
//             return 'Master';
//         default:
//             return 'Guest';
//         }
//     }
    
//     setuserType(userType){
//         userType = parseInt(userType);
//         switch(userType){
//         case 1:
//             this.setState({userType: 'CRM Admin'});
//             break;
//         case 2:
//             this.setState({userType: 'Fleet Owner'});
//             break;
//         case 3:
//             this.setState({userType: 'Delaer'});
//             break;
//         case 4:
//             //this.setLoggedinType(userType);
//             this.setState({userType: 'Manager'});
//             break;
//         case 99:
//             // this.setLoggedinType(userType);
//             this.setState({userType: 'Master'});
//             break;
                 
//         default:
//             //this.setLoggedinType(userType);
//             this.setState({userType: 'Guest'});
//             break;
//         }
//     }

//     componentWillUnmount(){
//         this.setState({feature:[]});
//         document.removeEventListener('click', this.showDropdownOutsideClick, false);   
//         document.removeEventListener('click', this.showDropdownOutsideClickForHouse, false);   
//     }

//     showDropdown(i,event){
//         event.preventDefault();
//         let showMenu=[];
//         if(!this.state.showMenu[i]){
//             showMenu[i]= !showMenu[i];
//             this.setState({showMenu: showMenu});
//         }else{
//             this.setState({showMenu: []});
//         }
      
//     }

//     // showMenuForHouse

//     showDropdownForHouse(i,event){
//         event.preventDefault();
//         this.getAllHomesByUserId();
//         let showMenuForHouse=[];
//         if(!this.state.showMenuForHouse[i]){
//             showMenuForHouse[i]= !showMenuForHouse[i];
//             this.setState({showMenuForHouse: showMenuForHouse});
//         }else{
//             this.setState({showMenuForHouse: []});
//         }
      
//     }

//     showDropdownOutsideClick(event){
//         if (this.navOutsideRef  && !this.navOutsideRef.current.contains(event.target)) {
//             this.setState({
//                 showMenu: []
//             });
//         }

//         //Hide menu on dropdown item click 
//         if(event.target.className === 'dropdown-item' || event.target.getAttribute('data-click') === 'link'){
//             this.setState({
//                 showMenu: []
//             });
//         }
      
//     }

//     showDropdownOutsideClickForHouse(event){
//         if (this.navOutsideRef  && !this.navOutsideRef.current.contains(event.target)) {
//             this.setState({
//                 showMenuForHouse: []
//             });
//         }

//         //Hide menu on dropdown item click 
//         if(event.target.className === 'dropdown-item' || event.target.getAttribute('data-click') === 'link'){
//             this.setState({
//                 showMenuForHouse: []
//             });
//         }
      
//     }

//     //show modal to update password
//     handleShowModal=()=>{
//         this.setState(prevState => ({
//             showmodal: !prevState.showmodal
//         }));
//     };

//     // create group form input onchange function    
//     changeHandler = (field,event )=> {
//         this.onDismiss('updatePassword');
//         let fields = this.state.fields;
//         switch (field){
//         case 'newPass':
//             fields['newPass'] = event.target.value;
//             this.setState({newPassError: false});
//             break;
//         case 'oldPass':
//             fields['oldPass'] = event.target.value;
//             this.setState({oldPassError: false});
//             break;
//         case 'confirmPass':
//             fields['confirmPass'] =event.target.value;
//             this.setState({confirmPassError: false});
//             break;
//         default:
//             break;
//         }
//         this.setState({fields});
//     };


//     //Filed Validation for Password Update
//     handleValidation(){

//         this.onDismiss('updatepass_alert');
//         let formIsValid = true;
//         // var strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^\\&*])(?=.{8,})");
//         var strongRegex = new RegExp('^[A-Za-z]{5,10}$');
//         let newPass = this.state.fields.newPass,
//             oldPass = this.state.fields.oldPass,
//             confirmPass = this.state.fields.confirmPass;
  
//         //oldPass
//         if(oldPass === undefined || (oldPass).trim()===''){
//             formIsValid = false;
//             this.setState({oldPassError: true});
//         }

//         //newpassword
//         if(newPass === undefined || (newPass).trim() ===''){
//             formIsValid = false;
//             this.setState({newPassError: true,NewPassErrMsg:'New Password field is empty or invalid'});
//         }

//         //newpassword length validation
//         if((newPass).trim() !=='')
//         {
//             if(newPass.length<5)
//             {
//                 formIsValid = false; //Please enter at least 8 character
//                 this.setState({newPassError: true, NewPassErrMsg:'Must contain atleast 5 character'});
//             }
//             //Password Pattern validation
//             // if(strongRegex.test(newPass)){}
//             // else{
//             //   formIsValid = false; //should contain at least one uppercase, lowercase, number and symbol
//             //   this.setState({newPassError: true,NewPassErrMsg:"Must contain at least one uppercase, lowercase, number and symbol"});
//             // }
//         }
     
//         //confirmPass
//         if(confirmPass === undefined || (confirmPass).trim() ===''){
//             formIsValid = false;
//             this.setState({confirmPassError: true,ConfirmPassErrMsg:'Confirm Password field is empty or invalid'});
//         }

//         //confirmPass length validation
//         if((confirmPass).trim() !=='' && newPass.length<4)
//         {
//             formIsValid = false; //Please enter at least 8 character
//             this.setState({newPassError: true,ConfirmPassErrMsg:'Must contain atleast 5 character'});
//         }
      
//         return formIsValid;
//     }


//     //api call to update password
//     handleUpdatePassword=(event)=>{
//         event.preventDefault();
//         if(this.handleValidation()){
//             if(this.state.fields.newPass.trim()!==this.state.fields.confirmPass.trim())
//             {
//                 this.setState({updatepass_error:'Confirm password does not match!'});
//                 this.setState({updatepass_alert:true,updatepass_alert_type:'danger'});
//             }
//             else 
//             {
//                 console.log('etyuhtgh');
//                 fetch(baseURL+'updatepassword',{
//                     method: 'PATCH',
//                     body : JSON.stringify({
//                         'username':sessionStorage.getItem('Pname'),
//                         'password':this.state.fields.newPass,
//                     }),
//                     headers: {
//                         'Content-Type' : 'application/json',
//                         'Authorization':`Bearer ${localStorage.getItem('token')}`
//                     }
//                 })
//                     .then((response)=>response.json())
//                     .then((responseJson)=>{
//                         console.log('sendata response=',responseJson);
//                         if(responseJson.status===1)
//                         {
//                             alert('Updates Successfully');
               
//                         }
//                         else if(responseJson.status===0){
//                             alert(responseJson.data);
                  
//                         }
//                     })
//                     .catch((error)=>{
//                         console.log('error=',error);
//                     }); 
//             }
//         }
//     };    

//     // Update password alert/error dismiss
//     onDismiss=(value)=>{
//         switch (value) {
//         case 'updatepass_alert':this.setState({updatepass_alert: false,updatepass_alert_type:'primary'});
//             break;

//         case 'modalclose':
//             this.setState({showmodal: false});
//             this.handleModalClear();
//             break;
    
//         default:
//             break;
//         }
//     };
  
//     //to clear all the field value and error message on modal close click
//     handleModalClear=()=>{
//         let fields = this.state.fields;
//         fields['newPass'] = '';
//         fields['oldPass'] = '';
//         fields['confirmPass'] = '';

//         this.setState({ 
//             newPassError:false,
//             oldPassError:false,
//             confirmPassError:false,
//             updatepass_alert:false,
//             NewPassErrMsg:'',
//             ConfirmPassErrMsg:''
//         });

//     }; 
//     getAllHomesByUserId() {
//         fetch(baseURL + 'getallhomesbyuserid', {
//             method: 'POST',
//             body: JSON.stringify({
//                "userid":localStorage.getItem('userid')
//             }),
//             headers: {
//                 'Content-Type': 'application/json',
//                 'Authorization': `Bearer ${localStorage.getItem('token')}`
//             }
//         })
//             .then(
//                 (response) => response.json()
//             ).then((responseJson) => {
//                 console.log("response date",responseJson)
//                 if (responseJson.status == 1) {
//                     // localStorage.setItem('homename',responseJson.home)
//                     var temp = [];
                    
//                     // console.log("home id displayed",localStorage.getItem('homeid'));
//                     for (let i = 0; i < responseJson.data.length; i++) {
//                         temp.push({
//                             homeid: responseJson.data[i].home.homeid,
//                             homename: responseJson.data[i].home.homename,
//                             // userid: responseJson.data[i].user.userid,
//                             // username: responseJson.data[i].user.username,
//                             // firstname: responseJson.data[i].user.firstname
                
//                         });
//                     }
                    
//                 }
              
//                 this.setState({ showMenuForHouse: temp }, () => {
//                     console.log('check drop down', this.state.showMenuForHouse);
//                 });
          
        
//             });
//     }


//      handleItemClick = (homeid,homename) => {
//         console.log("welcome",homename)
//         localStorage.setItem('homeid', homeid);
//         // this.setState({homeName:homename});
//         localStorage.setItem('homename', homename);
//         this.setState({ homeName: homename }, () => {
//             console.log('check drop down', this.state.homeName);
//         });
//         window.location.replace("/dashboard");
//     }


//     //to remove badge and set read count to 0
//     handleNotification()
//     {
//         this.setState({showBadge:'none'});
//         this.setState({read_count:0});
//     }

//     clearStorage = () =>{
//         localStorage.removeItem('token');
//         // localStorage.removeItem("l_name");
//         // localStorage.removeItem("e_");
//         // localStorage.removeItem("c_");
//         sessionStorage.clear();
//     };
  
//     logout()   
//     {   
//         //alert("Logged out - No data found!");
//         //this.clearStorage();
//         localStorage.removeItem('token');
//         localStorage.removeItem('homename');
//         // cookies.remove('username');
//         window.location.replace('/login');
//     }
  


//     render() { 
//         return ( 
//             <Header>
//                 <Navbar className="fixed-top navbar-dark bg-light navbar-expand-md" >
//                     {/* <button title="Sidebar toggler" className="d-block d-md-none nav-sidebar-toggler" 
//                         onClick={this.props.onClick} 
//                         ref={this.props.refToggler}>
//                         <Icon className="fas fa-bars" aria-hidden="true"></Icon>
//                     </button> */}

//                     <NavbarBrand full={{src: require('images/Accord_logo_192.png'), alt:'Accord'}}></NavbarBrand> 
                   
//                     <div className="d-flex flex-row ml-auto" ref={this.navOutsideRef}>
//                         <NavbarNav className="flex-row">
//                             {/*  <NavItem className="nav-item">
//                             <Link className="nav-link px-2" to="#">Menu1</Link></NavItem> 
//                            <NavItem className="nav-item">
//                             <Link className="nav-link" onClick={()=>this.sidebarHandle()} to="#">
//                             <i className="fas fa-bars" aria-hidden="true"></i>
//                             </Link>
//                             </NavItem>  */}

//                             {/*   <NavItem className={"dropdown"+(this.state.showMenu[0]?" show":"")}>
//                               <Link title="Notification alert" to="#" className="nav-link" role="button" 
//                               onClick={this.showDropdown.bind(this,0)}
//                               data-toggle="dropdown" aria-haspopup="true" aria-expanded={(this.state.showMenu[0]?"true":"false")}>
//                                 <i className="fas fa-bell" aria-hidden="true"></i>
//                                 <span className="badge badge-dark">59</span>
//                               </Link>
//                               <div className={"dropdown-menu dropdown-menu-right "+(this.state.showMenu[0]?"show":"")} >
//                                 <Link className="dropdown-item" to="#">Alert 1</Link>
//                                 <Link className="dropdown-item" to="#">Alert 2</Link>
//                                 <div className="dropdown-divider"></div>
//                                 <Link className="dropdown-item" to="#">Special Alert</Link>
//                               </div>
//                             </NavItem>  */}

//                             {/* <NavItem className={"dropdown"+(this.state.showMenu[0]?" show":"")} onClick={this.handleNotification.bind(this)}>
//                               <Link title="Notification alert" to="#" className="nav-link" role="button" 
//                               onClick={this.showDropdown.bind(this,0)}
//                               data-toggle="dropdown" aria-haspopup="true" aria-expanded={(this.state.showMenu[0]?"true":"false")}>
//                                 <i className="fas fa-bell" aria-hidden="true"></i>
//                                 <span className="badge badge-dark" style={{display:"block"}}>{79}</span>
//                               </Link> */}
//                             {/*  Alert List*/}
//                             {/* <div style={{display: "block"}} >
//                               <div className={"dropdown-menu dropdown-menu-right "+(this.state.showMenu[0]?"show":"")} >
//                                 <Link data-click="link" className="dropdown-item text-center dropdown-header" to="#">
//                                  <strong> {79} New Notification</strong>
//                                   </Link>
//                                 <Link data-click="link" className="dropdown-item" to="#">
//                                 <i  className="fa fa-exclamation-circle text-danger mr-4" aria-hidden="true"></i>Critical Alerts <span className="float-right"> {4}</span>
//                                 </Link>
//                                 <Link data-click="link" className="dropdown-item" to="#">
//                                 <i  className="fa fa-exclamation-triangle text-warning mr-4" aria-hidden="true"></i>Warning Alerts <span className="float-right">{20}</span>
//                                 </Link>
//                                 <Link  data-click="link" className="dropdown-item" to="#">
//                                 <i  className="fa fa-check-circle text-success mr-4 " aria-hidden="true"></i>Success Alerts <span className="float-right">{50}</span>
//                                 </Link>
//                                 <Link data-click="link" className="dropdown-item" to="#">
//                                 <i  className="fa fa-info-circle text-primary mr-4" aria-hidden="true"></i>Info Alerts <span className="float-right">{5}</span>
//                                 </Link>
//                                 <div className="dropdown-divider"></div>
//                                 <Link data-click="link" className="dropdown-item text-center" to="#"><strong data-click="link">View All Messages</strong></Link>
//                               </div>
//                               </div> */}
//                             {/* Today Alerts */}  
//                             {/* <div style={{ display: "none"}} >
//                                 <div className={"dropdown-menu dropdown-menu-right "+(this.state.showMenu[0]?"show":"")} >
//                                   <Link data-click="link" className="dropdown-item text-center" to="/#"><strong> -- Today Alerts --</strong></Link>
//                                 </div>
//                              </div> */}
//                             {/* No Alerts */} 
//                             {/* <div style={{ display: this.state.emptyNavDisplay}} >
//                             <div className={"dropdown-menu dropdown-menu-right "+(this.state.showMenu[0]?"show":"")} >
//                                   <Link data-click="link" className="dropdown-item text-center" to="/#"><strong> -- No Alerts --</strong></Link>
//                             </div>
//                           </div>
//                           </NavItem> */}

//                             {/* <NavItem>
//                               <Link title="Notification Mail" className="nav-link" to="#" onClick={this.showDropdown.bind(this,3)}>
//                               <i className="fas fa-envelope" aria-hidden="true"></i>
//                               <span className="badge badge-dark">12</span>
//                               </Link>
//                             </NavItem> */}
// {/* <Language/> */}
//                             {this.state.feature.length > 1 ?  
//                                 <NavItem className={'dropdown'+(this.state.showMenu[2]?' show':'')}>
//                                     <Link title="Your Roles" to="#" className="nav-link" role="button" 
//                                         onClick={this.showDropdown.bind(this,2)}
//                                         data-toggle="dropdown" aria-haspopup="true" aria-expanded={(this.state.showMenu[2]?'true':'false')}>
//                                         <i className="fas fa-users-cog" aria-hidden="true"></i>
//                                         <span className="badge badge-dark">{this.state.feature.length}</span>
//                                     </Link>

//                                     <div className={'dropdown-menu dropdown-menu-right '+(this.state.showMenu[2]?'show':'')} >
                               
//                                         {this.state.feature.map((a, index) => {
//                                             return (
//                                                 <Link key={index} data-click="link" className="dropdown-item" to="#" onClick={this.setuserType.bind(this,a)}>{this.getuserType(a)}</Link>
//                                             );
//                                         })}
                               
//                                         <div className="dropdown-divider"></div>
//                                         <Link data-click="link" className="dropdown-item text-muted" to="#">You're Now: {this.state.userType}</Link>
//                                     </div>
//                                 </NavItem>
//                                 :''}

                                        


//                             {/* <NavItem className={"dropdown"+(this.state.showMenu[1]?" show":"")}>
//                               <Link title="Settings" aria-haspopup="false" aria-expanded="false" className="nav-link" to="#" onClick={this.showDropdown.bind(this,3)}>
//                               <i className="fas fa-users-cog" aria-hidden="true"></i>
//                               </Link>
//                             </NavItem> */}
                            

//                             <NavItem className={'dropdown'+(this.state.showMenuForHouse[1]?' show':'')}>
//                                 {/* <Link to="#" className="nav-link" role="button" 
//                                     onClick={this.showDropdownForHouse.bind(this,1)}
//                                     data-toggle="dropdown" aria-haspopup="true" aria-expanded={(this.state.showMenuForHouse[1]?'true':'false')}>
//                                     <Image src={this.state.HomeImage} className="user-image" alt="User Profile" />
//                                     <span className="d-none d-sm-inline">{this.state.homeName}</span>
//                                 </Link> */}
                                
//                                 <div className={'dropdown-menu dropdown-menu-right '+(this.state.showMenuForHouse[1]?'show':'')} >
//                                 {(this.state.showMenuForHouse.map((listPointer) =>(
//                                     <Link data-click="link" className="dropdown-item" to="/dashboard" key={listPointer.homeid} onClick={()=>this.handleItemClick(listPointer.homeid,listPointer.homename)}>
//                                         <i data-click="link" className="fa fa-house-user" aria-hidden="true"></i> {listPointer.homename}
//                                     </Link>
                                    
                                    
                                
//                                     )))}
                                   
//                                 </div>
                                
                               
//                             </NavItem>

//                             <NavItem className="nav-item">
//                                                      <Link className="nav-link" onClick={this.handleShow} to="#">
//                                                      <i className="fas fa-bars" aria-hidden="true"></i>
//                                                      </Link>
//                                                  </NavItem> 




//                             <NavItem className={'dropdown'+(this.state.showMenu[1]?' show':'')}>
//                                 <Link to="#" className="nav-link" role="button" 
//                                     onClick={this.showDropdown.bind(this,1)}
//                                     data-toggle="dropdown" aria-haspopup="true" aria-expanded={(this.state.showMenu[1]?'true':'false')}>
//                                     <Image src={this.state.userProfileImage} className="user-image" alt="User Profile" />
//                                     <span className="d-none d-sm-inline">{this.state.profileName}</span>
//                                 </Link>
//                                 <div className={'dropdown-menu dropdown-menu-right '+(this.state.showMenu[1]?'show':'')}>
//                                     {/* <Link data-click="link" className="dropdown-item text-center profile" to="/profile">
//                                 <Image data-click="link" src={this.state.userProfileImage} className="img-thumbnail rounded-circle mx-auto d-block" height="120px" alt="User Profile" />
//                                   <div data-click="link" className="profile_name"><strong data-click="link">{this.state.profileName}</strong></div>
//                                   <div data-click="link" className="profile_role">{this.state.userType}</div>
//                                 </Link> */}
//                                     <Link data-click="link" className="dropdown-item" to="/profile">
//                                         <i data-click="link" className="fas fa-user" aria-hidden="true"></i> Profile
//                                     </Link>
//                                     <Link data-click="link" className="dropdown-item" to="/updatepassword"  >
//                                         <i data-click="link" className="fas fa-lock" aria-hidden="true"></i> Update Password
//                                     </Link>
//                                     <div className="dropdown-divider"></div>
//                                     <div className="text-center">
//                                         <Link data-click="link" className="btn btn-outline-primary btn-block" onClick={this.logout.bind(this)} to="#">
//                                             <i data-click="link" className="fas fa-sign-out-alt" aria-hidden="true"></i> Logout
//                                         </Link>
//                                     </div>
                                
//                                 </div>
//                             </NavItem>
//                            {/* {console.log("name",localStorage.getItem('homename'))} */}

                      

//                         </NavbarNav>
                        
//                     </div>
                
//                 </Navbar>
                
//                 {/*modal to update password */}
//                 <Modal show={this.state.showmodal}>
//                     <ModalHeader onClose={this.onDismiss.bind(this,'modalclose')}>
//                         <h3 className="modal-title"><strong>Update Password</strong></h3>
//                     </ModalHeader>
//                     <div style={{display:this.state.update_loader}} className="loader"></div>
//                     <ModalBody >
//                         {(window.innerWidth < 768 ? 
//                             <div>
//                                 <Col className="col-md-12">
//                                     <AlertModal show={this.state.updatepass_alert} type={this.state.updatepass_alert_type} 
//                                         onOk={this.onDismiss.bind(this,'updatepass_alert')} 
//                                         onClose={this.onDismiss.bind(this,'updatepass_alert')}>
//                                         <p className="text-center">{this.state.updatepass_error}</p>
//                                     </AlertModal>   
//                                 </Col>
//                             </div>:
//                             <div>
//                                 <Alert type={this.state.updatepass_alert_type} className="text-center"
//                                     isOpen={this.state.updatepass_alert} onClick={this.onDismiss.bind(this,'updatepass_alert')}>
//                                     {this.state.updatepass_error}
//                                 </Alert>
//                             </div>
//                         )}  
//                         <Form>
//                             <Row>
//                                 <Col className="col-md-12 col-lg-12 col-xl-12" >
                               
//                                     <FormGroup>
//                                         <Label  htmlFor="Old Password">Old Password </Label>
//                                         <Input type="password" onChange={this.changeHandler.bind(this,'oldPass')} value={this.state.fields.oldPass} 
//                                             className={'mb-3 form-control'+(this.state.oldPassError?' error':'')} required=""/>
//                                         <InvalidFeedback style={{display: (this.state.oldPassError?'block':'none')}}>
//                                                 Old Password field is empty or invalid
//                                         </InvalidFeedback>
//                                     </FormGroup>
                                   
//                                     <FormGroup>
//                                         <Label  htmlFor="New Password">New Password </Label>
//                                         <Input type="password" onChange={this.changeHandler.bind(this,'newPass')} value={this.state.fields.newPass} 
//                                             className={'mb-3 form-control'+(this.state.newPassError?' error':'')} required=""/>
//                                         <InvalidFeedback style={{display: (this.state.newPassError?'block':'none')}}>
//                                             {this.state.NewPassErrMsg}
//                                         </InvalidFeedback>
//                                     </FormGroup>
                               
//                                     <FormGroup>
//                                         <Label  htmlFor="Confirm Password">Confirm Password</Label>
//                                         <Input type="password" onChange={this.changeHandler.bind(this,'confirmPass')} value={this.state.fields.confirmPass} 
//                                             className={'mb-3 form-control'+(this.state.emailError?' error':'')} required=""/>
//                                         <InvalidFeedback style={{display: (this.state.confirmPassError?'block':'none')}}>
//                                             {this.state.ConfirmPassErrMsg}
//                                         </InvalidFeedback>
//                                     </FormGroup>
                                     
//                                 </Col>
//                             </Row>
//                         </Form>  
//                     </ModalBody>
//                     <ModalFooter>
//                         <Row >
//                             <Col className="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
//                                 <Button type="button" className="btn btn-outline-primary btn-block" 
//                                     onClick={this.onDismiss.bind(this,'modalclose')}>Close</Button>
//                             </Col>
//                             <Col className="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
//                                 <Button type="button" className="btn btn-primary btn-block" 
//                                     onClick={this.handleUpdatePassword.bind(this)}>Update</Button>
//                             </Col>
//                         </Row>
//                     </ModalFooter>
//                 </Modal>
              
//                 {/*modal to show logount not success*/} 
//                 <AlertModal type="danger" show={this.state.LogoutModal}
//                     onOk={() => { this.setState({LogoutModal:false});}}
//                     onClose={() => { this.setState({LogoutModal:false});}}>
//                     {this.state.logoutErrMsg}
//                 </AlertModal>

//                 <div style={{display:'none'}} className="loader" id="loader"></div>
//             </Header>
           
//         );
//     }
// }
 
// export default AppHeader;