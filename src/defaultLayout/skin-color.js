
// /*
// *****************************************************************************
// * License Information :Accord Global Technology Solutions Pvt. Ltd.                  *

// *                      #72 & 73, Krishna Reddy Colony, Domlur layout,            *

// *                      Domlur,Bangalore - 560071, INDIA                     *

// *                      Licensed software and All rights reserved.           *

// *****************************************************************************

// * File             : 
// *
// * Description      : App skin color theme change
// *
// * Author(s)        : Kavita Gupta
// *
// * Version History:
// * <Version Number>                 <Author>              <date>      <defect Number>      <Modification
// *                                                                                          made and the
// *                                                                                          reason for
// *                                                                                          modification >
// *  1.0                             Kavita Gupta         12.12.2020        --         initial version
// *
// * References        :
// *                     
// * Assumption(s)     : None.
// *                     
// * Constraint(s)     : None.
// *                     
//  ****************************************************************************
// */

// import React, { Component } from 'react';
// import {
//     Button
// } from "component/components";

// var skinColor = ["blue", "purple", "red", "black"];
// var body = document.body;
// class SkinColor extends Component {
//     constructor(props) {
//         super(props);
//         this.state = {  }
//     }

//     componentDidMount(){
//         if(localStorage.getItem("skin") != null){
//             this.handleClick(localStorage.getItem("skin"));
//         }          
//     }

//     handleClick = (newColor)=>{
//         var colorFound = false;
//         for(var color=0;color<skinColor.length;color++){
//                 if(newColor === skinColor[color]){
//                     body.classList.add(newColor);
//                     localStorage.setItem("skin",newColor);
//                     colorFound = true;
//                 }   
//                 else{
//                     body.classList.remove(skinColor[color]);
//                 }
//                 if(color === skinColor.length-1 && !colorFound )
//                 document.body.classList.add(skinColor[0]);
//         }
//     }

//   render() { 
//       return ( 
//         <div role="complementary" className="color-plate" id="colorPlate">
//         <ol className="color-selector" >
//             <li>
//                 <Button title="blue" name="blue" id="bluecolorid" onClick={()=>this.handleClick("blue")} className="color-button blue"></Button>
//             </li>
//             <li>
//                 <Button title="red" name="blue"  id="redcolorid" onClick={()=>this.handleClick("red")} className="color-button red"></Button>
//             </li>
//             <li>
//                 <Button title="purple" name="blue"  id="purplecolorid" onClick={()=>this.handleClick("purple")} className="color-button purple"></Button>
//             </li>
//             <li>
//                 <Button title="black" name="blue"  id="darkcolorid" onClick={()=>this.handleClick("black")} className="color-button black"></Button>
//             </li>
//         </ol>
//         </div>

//         );
//     }
// }
 
// export default SkinColor;

import React from "react";

const skinColor=()=>{

}

export default skinColor;