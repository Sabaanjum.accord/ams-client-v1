// eslint-disable-next-line
/******************************************************************************
* License Information :Accord Global Technology Solutions Pvt. Ltd.                  *

*                      #72 & 73, Krishna Reddy Colony, Domlur layout,            *

*                      Domlur,Bangalore - 560071, INDIA                     *

*                      Licensed software and All rights reserved.           *

*****************************************************************************

* File             : home.js
*
* Description      : App sidebar navigation list page
*
* Author(s)        : Kavita Gupta
*
* Version History:
* <Version Number>                 <Author>              <date>      <defect Number>      <Modification
*                                                                                          made and the
*                                                                                          reason for
*                                                                                          modification >
*  1.0                             Kavita Gupta          12.11.2022        --         initial version
*
* References        :
*                     
* Assumption(s)     : None.
*                     
* Constraint(s)     : None.
*                     
 ****************************************************************************
*/

export  const  adminNavigation = [
    {
      name: 'Dashboard',
      path: '/dashboard',
      icon: 'fa fa-house-user',
      // children:[
      //   {
      //     name: 'Dashboard',
      //     path: '/dashboard',
      //     icon: 'far fa-circle',
      //   },
      //   {
      //     name: 'Vehicle Behaviour',
      //     path: '/vehicle',
      //     icon: 'fas fa-car-crash',
      //   },

      //  ],
    },
    
    {
      name: 'Asset',
      path: '/asset',
      icon: 'fas fa-map-marked-alt',
      // children: [
      //   {
      //     name: '',
      //     path:  '/groupview',
      //     icon: 'fas fa-map-marker',
      //   },
      //   {
      //     name: 'Live View',
      //     path:  '/livetrack',
      //     icon: 'fas fa-bus',
      //   },
      //   {
      //     name: 'History Track',
      //     path: '/historytrack',
      //     icon: 'fas fa-history',
      //   },
      // ],
    },
    {
      name: 'Users',
      path: '/users',
      icon: 'fas fa-users',
   
    },
    // {
    //   name: 'Demo',
    //   path: '/demo',
    //   icon: 'fas fa-grin-wink'
    // },
    
     // Multi Level Dropdown
     {
      name: 'Settings',
      path: '/toplevel',
      icon: 'fa fa-share',
      children: [
        {
          name: 'Department',
          path:  '/department',
          icon: 'fa fa-map-marker',
        },
        {
          name: 'Building',
          path:  '/addBuilding',
          icon: 'fa fa-building',
        }]
          // children: [
          //   {
          //     name: 'Level Two',
          //     path:  '/level2',
          //     icon: 'fa fa-map-marker',
          //     children: [
          //       {
          //         name: 'Level Three',
          //         path:  '/level3',
          //         icon: 'fa fa-map-marker',
          //       },
          //       {
          //         name: 'Level Tree',
          //         path:  '/level3',
          //         icon: 'fa fa-bus',
          //       },
          //       {
          //         name: 'Level Tree',
          //         path: '/level3',
          //         icon: 'fa fa-history',
          //       },
             
          //     ],
          //   },
          //   {
          //     name: 'Level Two',
          //     path:  '/level2',
          //     icon: 'fa fa-bus',
          //   },
          //   {
          //     name: 'Level Two',
          //     path: '/level2',
          //     icon: 'fa fa-history',
          //   },
         
          // ],
        },

        // {
        //   name: 'Level One',
        //   path:  '/level',
        //   icon: 'fa fa-bus',
        // },
        // {
        //   name: 'Level One',
        //   path: '/level',
        //   icon: 'fa fa-history',
        // },
      
   
  ];



