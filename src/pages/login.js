﻿//eslint-disable-next-line
/******************************************************************************
* License Information :Accord Global Technology Solutions Pvt. Ltd.                  *

*                      #72 & 73, Krishna Reddy Colony, Domlur layout,            *

*                      Domlur,Bangalore - 560071, INDIA                     *

*                      Licensed software and All rights reserved.           *

*****************************************************************************

* File             : login.js
*
* Description      : App login page
*
* Author(s)        : Kavita Gupta
*
* Version History:
* <Version Number>                 <Author>              <date>      <defect Number>      <Modification
*                                                                                          made and the
*                                                                                          reason for
*                                                                                          modification >
*  1.0                             Saba Anjum G        20.11.2022        --         initial version
*
* References        :
*                     
* Assumption(s)     : None.
*                     
* Constraint(s)     : None.
*                     
 ****************************************************************************
*/
import { React } from 'react';
import { Link } from 'react-router-dom';
import 'App.css';
import { baseURL } from 'config/config';
import cookies from 'react-cookies';
import { useState, useEffect } from 'react';

import {
    Navbar,
    Input, Form,
    Col,
    Row,
    Container,
    NavbarBrand,
    Button,
    Wrapper,
    NavbarNav,
    NavItem,
    FormGroup,
    Footer,
    InvalidFeedback,
    Alert,
    Image

} from "component/components";



const Login = () => {

    /******************Hooks for Input Fields********************* */
    const [fields, setFields] = useState({ loginid: '', password: '', forgotloginid: '', });

    /******************Hooks for Input Fields Error****************** */
    const [loginIdError, setLoginidError] = useState(false);
    const [passwordError, setPasswordError] = useState(false);
    const [rememberMe, setRememberMe] = useState(false);

    /******************Hooks for displaying Error Msg**************** */
    const [craeteGrpAlert, setCraeteGrpAlert] = useState(false);
    const [craeteGrpAlertType, setCraeteGrpAlertType] = useState("");
    const [craeteGrpError, setCraeteGrpError] = useState("");

    /****************** useEffect for storing the name and pswd for remember me *********** */
    useEffect(() => {
        const usernameLocal = cookies.load('username');
        console.log("username", usernameLocal);
        const passwordLocal = cookies.load('password');
        if (usernameLocal && passwordLocal) {
            setFields({ loginid: usernameLocal, password: passwordLocal });
            setRememberMe(true);
        }
    }, []);

    /****************** close the error msg *********** */
    const onDismisss = () => {
        setCraeteGrpAlert(false);
        setCraeteGrpError("");
        setCraeteGrpAlertType("primary");
    };


    /******************Changing the state of the input field *********** */
    const changeHandler = (field, e) => {
        onDismisss("craeteGrpAlert");
        const updatedFields = { ...fields, [field]: e.target.value };
        setFields(updatedFields);

        if (field === 'loginid') {
            setLoginidError(!updatedFields.loginid);
        }
        else if (field === 'password') {
            setPasswordError(!updatedFields.password);
        }

    };

    /****************** Login API ******************** */

    const loginNow = (e) => {
        e.preventDefault();
        const isValid = validateForm();
        if (isValid) {
            console.log("hi", fields.loginid);
            console.log("hi", fields.password);
            fetch(baseURL + 'login', {
                method: 'POST',
                body: JSON.stringify({
                    'username': fields.loginid,
                    'password': fields.password,
                }),
                headers: {
                    'Content-Type': 'application/json'
                }
            })
                .then((response) => response.json())
                .then((responseJson) => {
                    console.log('sendata response=', responseJson);
                    if (responseJson.status === 1) {
                        localStorage.setItem('username', responseJson.user.username);
                        localStorage.setItem('userid', responseJson.user.userid)

                        localStorage.setItem('token', responseJson.token);
                        localStorage.setItem('roletypeid', responseJson.user.roletypeid)

                        if (rememberMe) {
                            cookies.save('username', fields.loginid, { path: '/' });
                            cookies.save('password', fields.password, { path: '/' });
                        } else {
                            cookies.remove('username');
                            cookies.remove('password');
                        }

                        window.location.replace("/dashboard");

                    }
                    else if (responseJson.status === 0) {
                        setCraeteGrpAlert(true);
                        setCraeteGrpAlertType("danger");
                        setCraeteGrpError(responseJson);

                    }
                });
        }
    };


    /****************** Validation for input fields ************** */
    const validateForm = () => {
        let isValid = true;
        //login validation
        if (!fields.loginid) {
            setLoginidError(true);
            isValid = false;
        }
        //password validation
        if (!fields.password) {
            setPasswordError(true);
            isValid = false;
        }

        return isValid;
    };

    /****************** Reset Password Validation *************** */
    function handleResetValidation() {
        let formIsValid = true;
        const reset = fields.forgotloginid;
        if (reset === undefined || reset.trim() === '') {
            formIsValid = false;
        }
        return formIsValid;
    }

    // If incase of use

    /****************** Reset Password API *********** */
    //Reset password
    // const handleReset = () => {
    //     if (handleResetValidation()) {
    //         // Reset code here
    //     }
    // };


    //page render

    return (
        <Wrapper id="login-page" className="login-body">
            <Navbar className="fixed-top  navbar-expand-md">
                <NavbarBrand full={{ src: require("images/Accord_logo_notag_192.png"), alt: "Accord" }}>
                </NavbarBrand>
                <div className={"collapse navbar-collapse "} id="navbarNavDropdown">
                    <NavbarNav className="mr-auto">
                        <NavItem>
                            <Link className="nav-link" to="/" style={{ color: 'black' }}>Home <span className="sr-only">(current)</span></Link>
                        </NavItem>
                    </NavbarNav>
                </div>
            </Navbar>


            <div className="login-pattern">
                <Container className="px-3">
                    <br /><br />
                    {/* **************** Login Image ******************** */}
                    <Row>
                        <Col className='col-sm-8 col-md-4 col-lg-4' >
                            <div style={{ height: '600px', width: '600px' }}>
                                <Image src={require("images/login-Img.png")} className="img-fluid" alt="bg" />
                            </div>
                        </Col>

                        <Col className="col-sm-2 col-md-3 col-lg-3"></Col>

                        {/* **************** Login Fields ******************** */}
                        <Col className='col-sm-10 col-md-4 col-xl-4'>

                            <Form id="user-login" >
                                <center>
                                    <h2 className='login-word'>Login</h2>
                                </center>
                                <br />
                                <div id="alertid">
                                    <Alert type={craeteGrpAlertType} className="text-center"
                                        isOpen={craeteGrpAlert} onClick={() => onDismisss("craeteGrpAlert")}>
                                        {craeteGrpError.status}: {craeteGrpError.data}
                                    </Alert>
                                </div>
                                <br /><br />
                                <FormGroup>
                                    <Input
                                        tabIndex="0"
                                        autoComplete="username"
                                        name="loginid"
                                        onChange={(e) => changeHandler('loginid', e)}
                                        value={fields.loginid}
                                        type="text"
                                        id="loginid"
                                        className={'form-controls' + (loginIdError ? ' error' : '')}
                                        placeholder="  &nbsp;User Name"
                                        required />
                                    <InvalidFeedback style={{ display: loginIdError ? 'block' : 'none' }} id="username-error">
                                        Please Enter username!
                                    </InvalidFeedback>
                                </FormGroup>
                                <br />
                                <FormGroup>
                                    <Input
                                        autoComplete="new-password"
                                        name="password"
                                        onChange={(e) => changeHandler('password', e)}
                                        value={fields.password}
                                        type="password"
                                        id="password"
                                        className={'form-controls' + (passwordError ? ' error' : '')}
                                        placeholder="  &nbsp; Password"
                                        required />
                                    <InvalidFeedback style={{ display: passwordError ? 'block' : 'none' }} id="password-error">
                                        Please Enter Password
                                    </InvalidFeedback>
                                </FormGroup>
                                <FormGroup className="">
                                    <Row>
                                        <Col className="col-md-7 col-sm-6 col-6 text-nowrap">
                                            <br />
                                            <Input type="checkbox" checked={rememberMe} onChange={(e) => setRememberMe(e.target.checked)} id="rememberMe" />
                                            <label htmlFor="rememberMe">&nbsp;&nbsp;Remember me</label>
                                        </Col>
                                        <Col className="col-md-5 col-sm-6 col-6 p-1 pl-3">
                                            <br />
                                        </Col>
                                    </Row>

                                </FormGroup>

                                <Row className="no-gutters">
                                    <Col className="col-lg-12 pl-lg-1 my-2">
                                        <center>
                                            <Button id="user-login-btn" type="submit" style={{ width: '150px' }} className="btn btn-dark btn-block btn-lg"
                                                onClick={loginNow}>Login Now</Button></center>
                                    </Col>
                                </Row>
                            </Form>
                        </Col>

                        <Col className='col col-sm-12 col-md-2 col-lg-1'>
                        </Col>
                    </Row>
                </Container>

                {/* **************** Footer Component  ******************** */}
                <Footer className='fixed-bottom'>
                    <Row>
                        <Col className='col-sm-3 col-md-3 col-xl-3'>
                        </Col>
                        <Col className="col-sm-6 col-md-6 col-xl-6">
                            <center>
                                <b>Copyright © 2023 Accord Global Technology Solutions.</b>
                            </center>
                        </Col>
                    </Row>
                </Footer>
            </div>

        </Wrapper>
    );
}


export default Login;
