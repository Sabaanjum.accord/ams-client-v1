// eslint-disable-next-line
/******************************************************************************
* License Information :Accord Global Technology Solutions Pvt. Ltd.                  *

*                      #72 & 73, Krishna Reddy Colony, Domlur layout,            *

*                      Domlur,Bangalore - 560071, INDIA                     *

*                      Licensed software and All rights reserved.           *

*****************************************************************************

* File             : home.js
*
* Description      : App components demo page
*
* Author(s)        : Kavita Gupta
*
* Version History:
* <Version Number>                 <Author>              <date>      <defect Number>      <Modification
*                                                                                          made and the
*                                                                                          reason for
*                                                                                          modification >
*  1.0                             Kavita Gupta          12.11.2022        --         initial version
*
* References        :
*                     
* Assumption(s)     : None.
*                     
* Constraint(s)     : None.
*                     
 ****************************************************************************
*/

import React,{Component} from "react";
import {
  Card,
  CardBody,
  CardHeader,
  Container,
  Row,
  Col,
  Alert,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Form,
  Input, Label,
  Textarea,
  FormGroup,
  ContainerFluid,
  CustomCheckbox,
  CustomControl,
  CustomRadio,
  CustomFile,
  InvalidFeedback,
  AlertModal,
  ConfirmAlertModal,
  Table,
  CustomSwitch
} from "component/components";
import { Picky } from 'react-picky';
import 'react-picky/dist/picky.css'; 

import {StateList} from "component/component-util";


class Demo extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      theme:"blue",
      demoModal:false,
      demoModal1:false,
      info:false,
      danger:false,
      success:false,
      warning:false,
      checkboxChecked: true,
      visible:true,
      alertdanger:true,
      alertsuccess:true,
      alertwarning:true,
      confirm:false,
      pickyValue:""

     }
   //  this.handleClick=this.handleClick.bind(this);
  }

  onSelectCandidatesClick() {
    this.setState({
      checkboxChecked: !this.state.checkboxChecked
    });
  }

  onDismiss=()=>{
    this.setState({visible:!this.state.visible})
  }

  onDismiss_danger=()=>{
    this.setState({alertdanger:!this.state.alertdanger})
  }

  onDismiss_success=()=>{
    this.setState({alertsuccess:!this.state.alertsuccess})
  }

  confirmModal=()=>{
    this.setState({confirm:!this.state.confirm});
   }

  onDismiss_warning=()=>{
    this.setState({alertwarning:!this.state.alertwarning})
  }

  toggle=()=>{
   this.setState({demoModal:!this.state.demoModal});
  }
  
  largeModal=()=>{
    this.setState({demoModal1:!this.state.demoModal1});
   }
  
   infoModal=()=>{
    this.setState({info:!this.state.info});
   }

   dangerModal=()=>{
    this.setState({danger:!this.state.danger});
   }

   successModal=()=>{
    this.setState({success:!this.state.success});
   }

   warningModal=()=>{
    this.setState({warning:!this.state.warning});
   }

   change=()=>{
     console.log("val=",document.getElementById("customCheck1").checked);
     console.log("radioval=",document.getElementById("customRadio1").checked);
   }

   fileUpload =() =>{
    var v = document.getElementById("customFile1").value;
    document.getElementById("customFile1").nextSibling.innerHTML=v.replace(/^.*[\\\\/]/, '');
    } 

  render() { 
  
    return (
      <div>
        <ContainerFluid>
        <section className="page-header">
          <h1 className="page-header-text">Demo</h1>
          <Alert type="primary" className="text-center d-inline-block"
            isOpen={this.state.visible} onClick={this.onDismiss.bind(this)}>
              <strong>A simple primary alert!</strong>
            </Alert>

          <hr/>
        </section> 
   {/* -------------------------- Card Component ----------------------------- */}       
  <Row>
              <Col className="col-xs-12 col-sm-6 col-md-4">
              <Card >
                  <CardHeader >
                        <h2 className="card-header-text">Group Management</h2>
                         {/* <div className="small text-muted">November 2017</div> */}
                     
                         <div className="box">
                           <i title="Close" className="fas fa-times" aria-hidden="true" ></i>
                        </div>
                        <div className="box">
                           <i title="Minimize" className="far fa-window-minimize" aria-hidden="true" ></i>
                        </div>
                        <div className="box">
                          <i title="Maximize" className="far fa-window-maximize" aria-hidden="true"></i>
                        </div>
                        <div className="box">
                          <i title="Reload" className="fas fa-sync" aria-hidden="true" ></i>
                        </div>
                  </CardHeader>
                  <CardBody>
                    <div>
                      Some quick example text to build on the card title and make
                      up the bulk of the card's content.{" "}
                    </div>
                  </CardBody>
                  {/* <CardFooter/> */}
                </Card>
              </Col>
              <Col className="col-xs-12 col-sm-6 col-md-4">
              <Card >
                  <CardHeader >
                        <h2 className="card-header-text">Group Management</h2>
                         {/* <div className="small text-muted">November 2017</div> */}
                     
                         <div className="box">
                           <i  title="Close" className="fas fa-times" aria-hidden="true" ></i>
                        </div>
                        <div className="box">
                           <i  title="Minimize" className="far fa-window-minimize" aria-hidden="true" ></i>
                        </div>
                        <div className="box">
                          <i  title="Maximize" className="far fa-window-maximize" aria-hidden="true"></i>
                        </div>
                        <div className="box">
                          <i  title="Reload" className="fas fa-sync" aria-hidden="true" ></i>
                        </div>
                  </CardHeader>
                  <CardBody>
                    <div>
                      Some quick example text to build on the card title and make
                      up the bulk of the card's content.{" "}
                    </div>
                  </CardBody>
                  {/* <CardFooter/> */}
                </Card>
              </Col>
              
              <div className="col-xs-12 col-sm-6 col-md-4">
              <Card >
                  <CardHeader >
                        <h2 className="card-header-text">Group Management</h2>
                         {/* <div className="small text-muted">November 2017</div> */}
                     
                         <div className="box">
                           <i  title="Close" className="fas fa-times" aria-hidden="true" ></i>
                        </div>
                        <div className="box">
                           <i  title="Minimize" className="far fa-window-minimize" aria-hidden="true" ></i>
                        </div>
                        <div className="box">
                          <i  title="Maximize" className="far fa-window-maximize" aria-hidden="true"></i>
                        </div>
                        <div className="box">
                          <i  title="Reload" className="fas fa-sync" aria-hidden="true" ></i>
                        </div>
                  </CardHeader>
                  <CardBody>
                    <div>
                      Some quick example text to build on the card title and make
                      up the bulk of the card's content.{" "}
                    </div>
                  </CardBody>
                  {/* <CardFooter/> */}
                </Card>
                  
              </div>
            
              <Col className="col-xs-12 col-sm-6 col-md-6">
              <Card >
                  <CardHeader >
                        <h2 className="card-header-text">Group Management</h2>
                         {/* <div className="small text-muted">November 2017</div> */}
                     
                         <div className="box">
                           <i  title="Close" className="fas fa-times" aria-hidden="true" ></i>
                        </div>
                        <div className="box">
                           <i  title="Minimize" className="far fa-window-minimize" aria-hidden="true" ></i>
                        </div>
                        <div className="box">
                          <i  title="Maximize" className="far fa-window-maximize" aria-hidden="true"></i>
                        </div>
                        <div className="box">
                          <i  title="Reload" className="fas fa-sync" aria-hidden="true" ></i>
                        </div>
                  </CardHeader>
                  <CardBody>
                    <div>
                      Some quick example text to build on the card title and make
                      up the bulk of the card's content.{" "}
                    </div>
                  </CardBody>
                  {/* <CardFooter/> */}
                </Card>
              </Col>
              
              <div className="col-xs-12 col-sm-12 col-md-6">
                <Card >
                  <CardHeader >
                        <h2 className="card-header-text">Group Management</h2>
                         {/* <div className="small text-muted">November 2017</div> */}
                     
                        <div className="box">
                           <i  title="Close" className="fas fa-times" aria-hidden="true" ></i>
                        </div>
                        <div className="box">
                           <i  title="Minimize" className="far fa-window-minimize" aria-hidden="true" ></i>
                        </div>
                        <div className="box">
                          <i  title="Maximize" className="far fa-window-maximize" aria-hidden="true"></i>
                        </div>
                        <div className="box">
                          <i  title="Reload" className="fas fa-sync" aria-hidden="true" ></i>
                        </div>
                  </CardHeader>
                  <CardBody>
                    <div>
                      Some quick example text to build on the card title and make
                      up the bulk of the card's content.{" "}
                    </div>
                  </CardBody>
                  {/* <CardFooter/> */}
                </Card>
              </div>
            </Row>

  <Row>
            <Col>

            {/* Button primary  */}
            <Button onClick={()=>{console.log("yes,its clicked")}}
            type="submit"
            className="btn btn-primary"
            value="primary-md"
            auotofocus=""
            name="primary-md" >
            btn-primary
            </Button>
            

            {/* Button primary  */}
            <Button onClick={()=>{console.log("yes,its clicked")}}
            type="submit"
            className="btn btn-primary "
            value="primary-md"
            auotofocus=""
            name="primary-md" >
            Ok
            </Button>
            

            {/* Button Warning  */}
            <Button onClick={()=>{console.log("yes,its clicked")}}
            type="submit"
            className="btn btn-warning"
            value="warning-md"
            name="warning-md">
            btn-Warning
            </Button>
            
            
            {/* Button danger  */}
            <Button onClick={()=>{console.log("yes,its clicked")}}
            type="button"
            className="btn btn-danger">
            Danger
            </Button>
            
            
            {/* Button success  */}
            <Button onClick={()=>{console.log("yes,its clicked")}}
            type="button"
            className="btn btn-success" >
            Success
            </Button>
            
            
            {/* Button  outline secondary  */}
            <Button onClick={()=>{console.log("yes,its clicked")}}
            type="button"
            className="btn btn-secondary">
            Secondary
            </Button>

            {/* Button  outline secondary  */}
            <Button onClick={()=>{console.log("yes,its clicked")}}
            type="button"
            className="btn btn-link">
            Secondary
            </Button>

            {/* Button  outline secondary  */}
            <Button onClick={()=>{console.log("yes,its clicked")}}
            type="button"
            className="btn btn-dark">
            Secondary
            </Button>

</Col></Row>
  <Row>
  <Col>
{/* Button Primary  */}
<Button onClick={()=>{console.log("yes,its clicked")}}
          type="submit"
          className="btn btn-outline-primary"
          value="primary-lg"
          name="primary-lg">
          btn-outline-primary
          </Button>
          

          <Button onClick={()=>{console.log("yes,its clicked")}}
          type="submit"
          className="btn btn-outline-primary"
          value="primary-lg"
          name="primary-lg">
          Ok
          </Button>
          

          {/* Button Warning  */}
          <Button onClick={()=>{console.log("yes,its clicked")}}
            type="submit"
            className="btn btn-outline-warning"
            value="warning-lg"
            name="warning-lg">
            btn-outline-warning
            </Button>
   
          {/* Button danger  */}
            <Button onClick={()=>{console.log("yes,its clicked")}}
            type="button"
            className="btn btn-outline-danger">
            btn-outline-danger
            </Button>
   
          {/* Button success  */}
            <Button onClick={()=>{console.log("yes,its clicked")}}
            type="button"
            className="btn btn-outline-success">
            btn outline success
            </Button>
   
          {/* Button  outline secondary  */}
          <div className="bg-primary float-right">
            <Button onClick={()=>{console.log("yes,its clicked")}}
            type="button"
            className="btn btn-outline-secondary">
            Secondary
            </Button>
            </div>
            
  </Col>
</Row>
           
  <Row>
  <Col>
{/* Button Primary  */}
<Button onClick={()=>{console.log("yes,its clicked")}}
          type="submit"
          className="btn btn-primary btn-lg"
          value="primary-lg"
          name="primary-lg">
          btn-Primary btn-lg
          </Button>
          

          <Button onClick={()=>{console.log("yes,its clicked")}}
          type="submit"
          className="btn btn-primary btn-sm"
          value="primary-sm"
          name="primary-sm">
          btn-sm
          </Button>
          

          {/* Button Warning  */}
          <Button onClick={()=>{console.log("yes,its clicked")}}
            type="submit"
            className="btn btn-warning btn-lg"
            value="warning-lg"
            name="warning-lg">
            Warning
            </Button>
            
   
          {/* Button danger  */}
            <Button onClick={()=>{console.log("yes,its clicked")}}
            type="button"
            className="btn btn-outline-danger btn-lg">
            Danger
            </Button>
   
          {/* Button  outline secondary  */}
            <Button onClick={()=>{console.log("yes,its clicked")}}
            type="button"
            className="btn btn-secondary btn-lg"
            >
            Secondary (disabled)
            </Button>
            {/* Button success  */}
            <Button onClick={()=>{console.log("yes,its clicked")}}
            type="button"
            className="btn btn-outline-success btn-sm">
            btn-sm
            </Button>
   
  </Col>
</Row>
  <Row>
            <Col>
              {/* Button  outline secondary  */}
              <Button onClick={()=>{console.log("yes,its clicked")}}
              type="button"
              className="btn btn-primary btn-block">
              Primary btn-Block
              </Button>
            
            </Col>
            <Col>
              {/* Button  outline secondary  */}
              <Button onClick={()=>{console.log("yes,its clicked")}}
              type="button"
              className="btn btn-secondary btn-block">
              Sec btn-Block
              </Button>
            </Col>
          </Row>
        
         
 {/* --------------------------  Alert Component ----------------------------- */}     
 <Row>
         <Col className="col-md-12">
            <Alert type="primary" 
            isOpen={this.state.visible} onClick={this.onDismiss.bind(this)}>
              <strong>A simple primary alert!</strong>
            </Alert>
        </Col>

        <Col className="col-md-12">
        <Alert type="danger"
         isOpen={this.state.alertdanger} onClick={this.onDismiss_danger.bind(this)}>
          <strong>A simple danger alert!</strong>
        </Alert>
        </Col>

        <Col className="col-md-12">
        <Alert type="success"
         isOpen={this.state.alertsuccess} onClick={this.onDismiss_success.bind(this)} >
          <strong>A simple success alert!</strong>
        </Alert>
        </Col>

      <Col className="col-md-12">
        <Alert type="warning"
         isOpen={this.state.alertwarning} onClick={this.onDismiss_warning.bind(this)}>
          <strong>A simple warning alert!</strong>
        </Alert>
        </Col>
        
        </Row>
        </ContainerFluid>
       
                <Container>
                  <Row>
                    <Col className="col-6">
                     <p>Col-6</p>
                     <p><strong>Strong text</strong></p>
                    </Col>

                    <Col className="col-2">
                     <p>fhgdfsdgf hgfhsg nbhjsdgv cxbvjbb bvjhdbv  bdfjhdbh</p>
                    </Col>
                  </Row>

                  <Row>
                    <Col className="col-md-6">
                     <p>Col-6</p>
                    </Col>

                    <Col className="col-md-4">
                     <p>fhgdfsdgf hgfhsg nbhjsdgv  bhbvhjdbvb bjhvbsdjhvb ncxbvjbb bvjhdbv  bdfjhdbh</p>
                    </Col>
                  </Row>

                  <Row>
                    <Col className="col-md-1">
                     <p>Col-1</p>
                    </Col>
                   
                  </Row>
                  </Container>

      <Container>
      <Row>
          <Col className="col-md-12">
              <div>
              <h1 >Header 1 </h1>
              <h2>Header 2 </h2>
              <h3>Header 3 </h3>
              <h4>Header 4 </h4>
              <h5>Header 5 </h5>
              <h6>Header 6 </h6>
            </div>
          </Col>
      </Row>
 
{/* -------------------------- Card modal Component ----------------------------- */} 

        <Row>
          <Col className="col-md-12 col-lg-6 col-xl-6">
          <Button type="button" className="btn btn-primary" onClick={this.toggle.bind(this)}>
            Launch demo modal
          </Button>
          </Col>

          <Col className="col-md-12 col-lg-6 col-xl-6">
          <Button type="button" className="btn btn-primary" onClick={this.largeModal.bind(this)}>
            Launch demo modal large
          </Button>
          </Col>
          <br/>  <br/>  <br/>  
          </Row>

 
         {/************  MODAL  ************ */}     
         <Col className="col-md-12">
            <Modal show={this.state.demoModal} 
            /* l */>
              <ModalHeader onClose={() => { this.setState({ demoModal:false});}}>
                <h3 className="modal-title">Modal title</h3>
              </ModalHeader>
              <ModalBody >
                Some quick example text to build on the card title and make up the bulk of the card's content.
              </ModalBody>
              <ModalFooter>
                <Button type="button" className="btn btn-outline-primary" onClick={this.toggle.bind(this)}>Close</Button>
                <Button type="button" className="btn btn-primary">Save</Button>
              </ModalFooter>
            </Modal>
          </Col> 

          {/************ Large MODAL  ************ */}     
       <Col className="col-md-12">
              <Modal 
                show={this.state.demoModal1} 
                size="modal-lg">
               <ModalHeader onClose={() => { this.setState({ demoModal1:false});}}>
                <h3 className="modal-title">Modal title</h3>
              </ModalHeader>
              <ModalBody >
                Some quick example text to build on the card title and make up the bulk of the card's content.
              </ModalBody>
              <ModalFooter>
                <Button type="button" className="btn btn-outline-primary" onClick={this.largeModal.bind(this)}>Close</Button>
                <Button type="button" className="btn btn-primary">Save</Button>
              </ModalFooter>
            </Modal>
          </Col>  

     
      {/************  Alert MODAL  ************ */}
      <Row>
          
          <Col className="col-md-3 col-lg-3 col-xl-3">
          <Button type="button" className="btn btn-primary" onClick={this.infoModal.bind(this)}>
            Info
          </Button>
          </Col>

          <Col className="col-md-3 col-lg-3 col-xl-3">
          <Button type="button" className="btn btn-success" onClick={this.successModal.bind(this)}>
            Success
          </Button>
          </Col>

          <Col className="col-md-3 col-lg-3 col-xl-3">
          <Button type="button" className="btn btn-warning" onClick={this.warningModal.bind(this)}>
            Warning
          </Button>
          </Col>

          <Col className="col-md-3 col-lg-3 col-xl-3">
          <Button type="button" className="btn btn-danger" onClick={this.dangerModal.bind(this)}>
            Danger
          </Button>
          </Col>

          <Col className="col-md-3 col-lg-3 col-md-xl-3">
          <Button type="button" className="btn btn-primary" onClick={this.confirmModal.bind(this)}>
            Confirm Modal
          </Button>
          </Col>
          </Row>

       <Row>
        {/************ Alert MODAL Danger ************ */}     
        <Col className="col-md-12">
            <AlertModal show={this.state.danger}  type="danger"
             onOk={this.dangerModal.bind(this)} 
             onClose={() => { this.setState({ danger:false});}}
              >
              <p>Some text in the modal.  Indicates a dangerous or potentially negative action</p>
            </AlertModal>
          </Col>

      {/************ Alert MODAL Success ************ */}     
        <Col className="col-md-12">
            <AlertModal show={this.state.success} type="success"
            onOk={this.successModal.bind(this)}
            onClose={() => { this.setState({ success:false});}}>
              <p>Some text in the modal.  Indicates a dangerous or potentially negative action</p>
           
            </AlertModal>
          </Col>

 {/************ Alert MODAL Warning ************ */}     
 <Col className="col-md-12">
            <AlertModal show={this.state.warning} type="warning" 
            onOk={this.warningModal.bind(this)}
            onClose={() => { this.setState({ warning:false});}}>
              <p>Some text in the modal.  Indicates a dangerous or potentially negative action</p>
            </AlertModal>
          </Col>

 {/************ Alert MODAL Info ************ */}     
 <Col className="col-md-12">
            <AlertModal show={this.state.info} type="info" 
                 onOk={this.infoModal.bind(this)}
                 onClose={() => { this.setState({ info:false});}}>
                <p>Some text in the modal.  Indicates a dangerous or potentially negative action</p>
              </AlertModal>
              </Col>

{/************ Alert MODAL Info ************ */}     
 <Col className="col-md-12">
            <ConfirmAlertModal show={this.state.confirm} type="info" 
                 onClose={() => { this.setState({ confirm:false});}}
                 onOk={this.confirmModal.bind(this)}
                 onCancel={() => { this.setState({ confirm:false});}}>
                <p>Some text in the modal.  Indicates a dangerous or potentially negative action</p>
              </ConfirmAlertModal>
              </Col>
            </Row>
           
      

{/*****************************Form Fields**************************************** */}
        
           <h4>Form Elements</h4>
 {/***********Form Elements******************/}
 <Form>
         <Row>
       <Col className="col-md-6 col-lg-6 col-xl-6" >
             <FormGroup>
              <Label  htmlFor="exampleInputName">Name </Label>
              <Input type="text" className="form-control" id="exampleInputName"  placeholder="Enter name"/>
              <InvalidFeedback>No, you missed this one.</InvalidFeedback>
             </FormGroup>

             <FormGroup>
               <Label htmlFor="exampleInputEmail">Email address </Label> 
               <Input type="email" className="form-control" id="exampleInputEmail"  placeholder="Enter email"/>
               <InvalidFeedback>No, you missed this one.</InvalidFeedback>
             </FormGroup>

             <FormGroup>
              <Label htmlFor="exampleInputPassword">Password </Label> 
              <Input type="password" className="form-control" id="exampleInputPassword" placeholder="Password"></Input>
              <InvalidFeedback>No, you missed this one.</InvalidFeedback>
             </FormGroup>

             <FormGroup >
              <Label  htmlFor="exampleInputName1">Name </Label>
              <Input type="text" className="error form-control" id="exampleInputName1"  placeholder="Error"/>
              <InvalidFeedback>No, you missed this one.</InvalidFeedback>
             </FormGroup>

             <FormGroup>
              <Label htmlFor="exampleFormControlTextarea1">Example textarea</Label>
              <Textarea className="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Details"></Textarea>
              <InvalidFeedback>No, you missed this one.</InvalidFeedback>
            </FormGroup>

            <CustomFile id="demofile1" name="demofile1"></CustomFile>

            <CustomFile className="error" id="demofile2" name="demofile2"></CustomFile>
      
            
          </Col>
          <Col className="col-md-2 col-lg-2 col-xl-2 "></Col>
          <Col className="col-md-4 col-lg-4 col-xl-4 ">
           
            <h2 >CheckBox </h2>

                <CustomCheckbox id="democheck1" labelName="Custom Checkbox" defaultChecked={true}></CustomCheckbox>

                <CustomControl className="custom-checkbox">
                 <Input type="checkbox" id="customCheck1" className="custom-control-input"  defaultChecked={true} onClick={this.change.bind(this)}></Input>
                 <Label className="custom-control-label" htmlFor="customCheck1">Small</Label>
                </CustomControl>

                <CustomCheckbox id="democheck3" labelName="Disabled Checkbox" disabled defaultChecked={true}></CustomCheckbox>
                 
                <CustomCheckbox className="custom-checkbox-lg" id="democheck4" labelName="Big Checkbox" defaultChecked={true}></CustomCheckbox>

                <CustomControl className="custom-checkbox custom-checkbox-lg">
                 <Input type="checkbox" className="custom-control-input" id="customCheck5" disabled defaultChecked={true}></Input>
                 <Label className="custom-control-label" htmlFor="customCheck5">Big disabled</Label>
                </CustomControl>
               
                <br/>
              
                <h2>Radio Button </h2>
                
                <CustomControl className="custom-radio">
                  <Input type="radio" className="custom-control-input" id="customRadio1" name="customRadio" ></Input>
                  <Label className="custom-control-label" htmlFor="customRadio1">Radio 1</Label>
                </CustomControl>

                <CustomRadio id="customRadio2" name="customRadio" labelName="Radio 2" ></CustomRadio> 

                <CustomRadio id="customRadio3" name="customRadio" labelName="Radio 3" disabled ></CustomRadio> 
            
                <CustomControl className="custom-radio custom-radio-lg">
                  <Input type="radio" className="custom-control-input" id="customRadio4" name="customRadio" defaultChecked={true}></Input>
                  <Label className="custom-control-label" htmlFor="customRadio4">Big radio</Label>
                </CustomControl>
                
                <CustomRadio id="customRadio5" className="custom-radio custom-radio-lg" name="customRadio" labelName="Disabled lg Radio 2" disabled></CustomRadio>
              
             </Col>
             </Row>
             </Form>

    </Container>
<br/>
    <Row>
        <Col className="col-md-6 col-lg-6 col-xl-6">
            <Label  htmlFor="Remind before">State*</Label>
            <FormGroup>
                <Picky
                    id="taskname" 
                    area-labelledby="State" 
                    open={false}
                    placeholder="State*"
                    options={StateList}
                    value={this.state.pickyValue}
                    labelKey="label"
                    valueKey="value"
                    multiple={true}
                    includeSelectAll={true}
                    includeFilter={true}
                    onChange={values => this.setState({pickyValue:values})}
                    dropdownHeight={150}
                    keepOpen ={false}
                    clearFilterOnClose={true}
                    /> 
                <InvalidFeedback style={{display: (this.state.remindBeforeError?"block":"none")}}>
                    Select state name
                </InvalidFeedback>
            </FormGroup>
            </Col>



      {/* Switch Button */}
      <Col className="col-md-6 col-lg-6 col-xl-6">
        <Label  htmlFor="Notification Type">Switch</Label>
        <FormGroup >
        <Row>
            <Col className="col-md-6 col-lg-6 col-xl-6">
                <CustomSwitch id="email" name="messageType" labelName="Email" 
                value="Email"
                defaultChecked={true}>
               </CustomSwitch> 
            </Col>
            <Col className="col-md-6 col-lg-6 col-xl-6">
            <CustomSwitch id="sms" name="messageType" labelName="SMS" 
                value="sms">
            </CustomSwitch> 
          </Col>
        </Row>
          <InvalidFeedback style={{display: (this.state.reminderTypeError?"block":"none")}}>
              Select repeating schedule
          </InvalidFeedback>
          </FormGroup>
            
      </Col>
    </Row>
<br/>
<ContainerFluid>
  <Row>
    <Col>
    <Table className="table-striped">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">First</th>
                  <th scope="col">Last</th>
                  <th scope="col">Handle</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th scope="row">1</th>
                  <td>Mark</td>
                  <td>Otto</td>
                  <td>@mdo</td>
                </tr>
                <tr>
                  <th scope="row">2</th>
                  <td>Jacob</td>
                  <td>Thornton</td>
                  <td>@fat</td>
                </tr>
                <tr>
                  <th scope="row">3</th>
                  <td>Larry</td>
                  <td>the Bird</td>
                  <td>@twitter</td>
                </tr>
              </tbody>
            </Table>
    </Col>
  </Row>
  <Row>
    <Col>
    <Table  className="table-dark table-hover table-striped">
                    <thead>
                      <tr>
                        <th scope="col">#</th>
                        <th scope="col">First</th>
                        <th scope="col">Last</th>
                        <th scope="col">Handle</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th scope="row">1</th>
                        <td>Mark</td>
                        <td>Otto</td>
                        <td>@mdo</td>
                      </tr>
                      <tr>
                        <th scope="row">2</th>
                        <td>Jacob</td>
                        <td>Thornton</td>
                        <td>@fat</td>
                      </tr>
                      <tr>
                        <th scope="row">3</th>
                        <td>Larry</td>
                        <td>the Bird</td>
                        <td>@twitter</td>
                      </tr>
                    </tbody>
                  </Table>
    </Col>
  </Row>
</ContainerFluid>



    
      
    </div>

      
    );
  }
}
 
export default Demo;

