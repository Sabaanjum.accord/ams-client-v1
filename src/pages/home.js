// eslint-disable-next-line
/******************************************************************************
* License Information :Accord Global Technology Solutions Pvt. Ltd.                  *

*                      #72 & 73, Krishna Reddy Colony, Domlur layout,            *

*                      Domlur,Bangalore - 560071, INDIA                     *

*                      Licensed software and All rights reserved.           *

*****************************************************************************

* File             : home.js
*
* Description      : App home page
*
* Author(s)        : Kavita Gupta
*
* Version History:
* <Version Number>                 <Author>              <date>      <defect Number>      <Modification
*                                                                                          made and the
*                                                                                          reason for
*                                                                                          modification >
*  1.0                             Kavita Gupta          12.11.2022        --         initial version
*
* References        :
*                     
* Assumption(s)     : None.
*                     
* Constraint(s)     : None.
*                     
 ****************************************************************************
*/
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import {Navbar,  
    NavbarBrand,
    Wrapper,
    NavbarNav, NavItem,
    // HeaderDropdown,
    // DropdownToggle,
    // DropdownMenu,
    // DropdownItem,
    Button, Image,
    // ContainerFluid, Input, Label,
    // CustomInput,
    SkinColor,
    Container,
    Row,
    Col,
    Footer,
} from "component/components";
class HomePage extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            collapsed: false
         }
    }

    
    navbarCollapse(){
        if(this.state.collapsed === false)
            this.setState({collapsed: true});
        else
            this.setState({collapsed: false});
    }

    
 

    render() { 
        return (  
            <Wrapper id="home-page" className="home-page">
            <Navbar className="fixed-top  navbar-expand-md">
            <NavbarBrand full={{src: require("images/Accord_logo_notag_192.png"), alt:"Accord"}}></NavbarBrand> 
                   
            <NavbarNav className="ml-auto d-block d-sm-block d-md-none d-lg-none d-xl-none"> 
                <NavItem>
                    <Link alt="Login" className="nav-link" to="/login">
                    <i title="Login"
                    className="fas fa-sign-in-alt"
                    aria-hidden="true"></i>
                    </Link>
                </NavItem>
            </NavbarNav>
            
            <Button className={"navbar-toggler "+(this.state.collapsed?"":"collapsed")}type="button" 
            data-toggle="collapse" data-target="#navbarNavDropdown" 
            aria-controls="navbarNavDropdown" aria-expanded={(this.state.collapsed?"true":"false")}
            aria-label="Toggle navigation" onClick={this.navbarCollapse.bind(this)}>
                <span className="navbar-toggler-icon"></span>
            </Button>

            <div className={"collapse navbar-collapse "+(this.state.collapsed?"show":"")} id="navbarNavDropdown">
                <NavbarNav className="mr-auto">
                <NavItem className="nav-item active">
                    <Link className="nav-link" to="/" style={{color:'black'}}>Home <span className="sr-only">(current)</span></Link>
                </NavItem>
               
                {/* <NavItem className="nav-item">
                    <Link className="nav-link" to="#">Contact Us</Link>
                </NavItem> */}
                
                </NavbarNav>
                <NavbarNav className="navbar-nav ml-auto">
                    <NavItem className="nav-item">
                        <Link className="nav-link" to="/login" >Login</Link>
                    </NavItem>
                </NavbarNav>
            </div> 
            </Navbar>

            <SkinColor/>
                

</Wrapper>
        );
    }
}
 
export default HomePage;