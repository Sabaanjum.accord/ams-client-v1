// eslint-disable-next-line
/******************************************************************************
* License Information :Accord Global Technology Solutions Pvt. Ltd.                  *

*                      #72 & 73, Krishna Reddy Colony, Domlur layout,            *

*                      Domlur,Bangalore - 560071, INDIA                     *

*                      Licensed software and All rights reserved.           *

*****************************************************************************

* File             : home.js
*
* Description      : App routing Page
*
* Author(s)        : Kavita Gupta
*
* Version History:
* <Version Number>                 <Author>              <date>      <defect Number>      <Modification
*                                                                                          made and the
*                                                                                          reason for
*                                                                                          modification >
*  1.0                             Kavita Gupta          12.11.2022        --         initial version
*
* References        :
*                     
* Assumption(s)     : None.
*                     
* Constraint(s)     : None.
*                     
 ****************************************************************************
*/

import { BrowserRouter, Routes, Route } from "react-router-dom";
import Loadable from 'react-loadable';
import HomePage from "pages/home";
import PrivacyPolicy from "pages/privacypolicy";
import Login from "pages/login";
import Department from "views/department/department";
import Assets from "views/asset/asset";


var DefaultLayout;
const loading = () => 
<div className="animated fadeIn pt-3 text-center">
<div className="circle-loader"></div>
</div>;

DefaultLayout = Loadable({
  loader: () => import('defaultLayout/layout'),
  loading
}); 


export default function App() {
  return (
 <BrowserRouter>
      <Routes>
        <Route exact path="/" element={<HomePage />}/>
        <Route exact path="/login" element={<Login/>} />
        <Route exact path="/privacy" element={<PrivacyPolicy />} />
        <Route path="*" name="dashboard" element={<DefaultLayout />} />
 
      </Routes>
    </BrowserRouter> 
  );
}
