import React, { useState, useEffect } from "react";
import {
    Container,
    Row,
    Col,
    Button,
    CustomCheckbox,
    Image,
    FormGroup,
    Input,
    Alert,

} from 'component/components';
import { baseURL } from "config/config";
import Tab from 'react-bootstrap/Tab';
import Tabs from 'react-bootstrap/Tabs';
import Card from 'react-bootstrap/Card';
import 'App.css';


const Department = () => {

    /****************** Hooks for input fields ********************* */
    const [fields, setDeptName] = useState({ dept_name: '' });
    const [mainClassification, setMainClssification] = useState('');
    const [subClassification, setSubClssification] = useState('');
    const [deptIdError, setdeptIdError] = useState(false);
    const [list, setList] = useState([]);
    const [deptList, setDeptList] = useState([]);
    const [mainList, setMainList] = useState([]);
    const [subList, setSubList] = useState([]);
    const [selectedOption, setSelectedOption] =useState("");
    const [selectedMainClsf, setSelectedMainClsf] = useState("");
    const [mainClsfn, setMainClsfn] = useState('');
    const [selectedSubClsf, setSelectedSubClsf] = useState("");
    const [assetType, setAssetType] = useState("");
    const [showBtn, setshowBtn] = useState(false);
    const [key, setKey] = useState('Department');

    /****************** Hooks for error msg ********************* */
    const [craetegrpalert, setCraetegrpalert] = useState(false);
    const [craetegrpalert_type, setCraetegrpalertType] = useState("");
    const [craetegrperror, setCraetegrperror] = useState("");

    /****************** Hooks for code field ********************* */
    const [mainCode, setMainCode] = useState('');
    const [subCode, setSubCode] = useState('');
    const [assetCode, setAssetCode] = useState('');
    const [heading, setHeading] = useState('Department');


    /****************** useEffect for getting all the drop down data ********************* */
    useEffect(() => {
        getAllDepartment();
        getAllMainClassification();
        getAllSubClassification();
        const roletype = window.localStorage.getItem('roletypeid');
        console.log(roletype);

        if (roletype === '1') {
            setshowBtn(true);
        }

        else if (roletype === '2') {
            setshowBtn(true);
        }
        else {
            setshowBtn(false)
        }

    }, []);

    /****************** handle the close error msg ********************* */
    const onDismisss = () => {
        setCraetegrpalert(false);
        setCraetegrperror("");
        setCraetegrpalertType("primary");
    };


    /****************** handle change in dept drop down ********************* */
    const handleOptionChange = (event) => {
        setSelectedOption(event.target.value);
    };

    /****************** Add main classification API ********************* */
    const handleChange = (event) => {
        event.preventDefault();

        fetch(baseURL + 'addmainclassification', {
            method: 'POST',
            body: JSON.stringify({
                'departmentid': selectedOption,
                'username': localStorage.getItem('username'),
                'mainclassificationname': mainClassification,
                'mccode': mainCode
            }),
            headers:
            {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then(
                (response) => response.json()
            ).then((responseJson) => {
                console.log('unnathi floor', responseJson);
                if (responseJson.status == 1) {

                }
            })
    }

    /****************** Add sub classification API ********************* */
    const handleSubClssification = (event) => {
        event.preventDefault();
        fetch(baseURL + 'addsubclassification', {
            method: 'POST',
            body: JSON.stringify({
                'departmentid': selectedOption,
                'username': localStorage.getItem('username'),
                'mainclassificationid': mainClsfn,
                'subclassificationname': subClassification,
                'sccode': subCode
            }),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then(
                (response) => response.json()
            ).then((responseJson) => {
                console.log('unnathi floor', responseJson);
                if (responseJson.status == 1) {

                }
            })
    }


    /****************** Add dept API ********************* */
    const addDepartment = () => {
        console.log("dept name", fields.dept_name);
        fetch(baseURL + 'adddepartment', {
            method: 'POST',
            body: JSON.stringify({
                'departmentname': fields.dept_name,

            }),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then((response) => response.json())
            .then((responseJson) => {
                console.log('sendata response=', responseJson);
                console.log('sendata token=', responseJson.token);
                if (responseJson.status === 1) {
                    setCraetegrpalert(true);
                    setCraetegrpalertType("success");
                    setCraetegrperror(responseJson);
                    getAllDepartment();
                }
            })
    }

    /****************** get all dept API ********************* */
    const getAllDepartment = (e) => {
        {
            fetch(baseURL + 'getalldepartment', {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })
                .then((response) => response.json())
                .then((responseJson) => {
                    console.log('sendata response of department=', responseJson);
                    if (responseJson.status == 1) {
                        var temp = [];
                        for (let i = 0; i < responseJson.data.length; i++) {
                            temp.push({
                                departmentid: responseJson.data[i].departmentid,
                                departmentname: responseJson.data[i].departmentname,
                                createdby: responseJson.data[i].createdby,
                                createdat: responseJson.data[i].createdat
                            });
                        }
                        setDeptList(temp);
                        setList(temp);
                    }
                });
        }
    };

    /****************** get all main classification API ********************* */
    const getAllMainClassification = (e) => {
        {
            fetch(baseURL + 'getallmainclassification', {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })
                .then((response) => response.json())
                .then((responseJson) => {
                    console.log('sendata response=', responseJson);
                    if (responseJson.status == 1) {
                        var temp = [];
                        for (let i = 0; i < responseJson.data.length; i++) {
                            temp.push({
                                mainclassificationid: responseJson.data[i].mainclassificationid,
                                mainclassificationname: responseJson.data[i].mainclassificationname,
                                createdby: responseJson.data[i].createdby,
                                createdat: responseJson.data[i].createdat
                            });
                        }
                        setMainList(temp);
                    }
                });
        }
    };

    /****************** get all sub classification API ********************* */
    const getAllSubClassification = (e) => {
        {
            fetch(baseURL + 'getallsubclassification', {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })
                .then((response) => response.json())
                .then((responseJson) => {
                    console.log('sendata response=', responseJson);
                    if (responseJson.status == 1) {
                        var temp = [];
                        for (let i = 0; i < responseJson.data.length; i++) {
                            temp.push({
                                subclassificationid: responseJson.data[i].subclassificationid,
                                subclassificationname: responseJson.data[i].subclassificationname,
                                createdby: responseJson.data[i].createdby,
                                createdat: responseJson.data[i].createdat
                            });
                        }
                        setSubList(temp);
                    }
                });
        }
    };

    /****************** Add asset type API ********************* */
    const addAssetType = (e) => {
        e.preventDefault();
        fetch(baseURL + 'addassettype',
            {
                method: 'POST',
                body: JSON.stringify({
                    'departmentid': selectedOption,
                    'username': localStorage.getItem('username'),
                    'mainclassificationid': mainClassification,
                    'subclassificationid': selectedSubClsf,
                    'assettypename': assetType,
                    'atcode': assetCode
                }),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })
            .then((response) => response.json())
            .then((responseJson) => {
                console.log('sendata response=', responseJson);
                console.log('sendata token=', responseJson.token);
                if (responseJson.status === 1) {
                }
            })
    }

    /****************** handle change for dept field ********************* */
    const changeHandler = (field, e) => {
        const updatedFields = { ...fields, [field]: e.target.value };
        setDeptName(updatedFields);
        if (field === 'dept_name') {
            setdeptIdError(!updatedFields.deptIdError);
        }
    };

    /****************** delete dept API (for future use) ********************* */
    // const deleteDepartment = (departmentid) => {
    //     console.log("delete", departmentid);
    //     fetch(baseURL + 'deletedepartment', {
    //         method: 'DELETE',
    //         body: JSON.stringify({
    //             'departmentid': departmentid,
    //         }),
    //         headers: {
    //             'Content-Type': 'application/json',
    //             'Authorization': `Bearer ${localStorage.getItem('token')}`
    //         }
    //     })
    //         .then((result) => {
    //             result.json().then((resp) => {

    //                 console.log("reponse", resp);
    //                 if (resp.status === 1) {
    //                     setCraetegrpalert(true);
    //                     setCraetegrpalertType("success");
    //                     setCraetegrperror(resp);
    //                     getAllDepartment();
    //                 }

    //             });
    //         })
    //         .catch((error) => {
    //             console.log('error=', error);
    //         });
    // }


        /****************** update dept API (for future use) ********************* */
    // const updateDepartment = () => {


    //     fetch(baseURL + 'updatedepartment',
    //     {
    //        method: 'PATCH',
    //        body: JSON.stringify({
    //            'departmentid':selectedOption,
    //            'username':localStorage.getItem('username'),
    //            'mainclassificationid':mainClassification,
    //            'subclassificationid':selectedMainClsf,
    //            'assettypename':assetType
    //        }),
    //        headers: {
    //            'Content-Type': 'application/json',
    //            'Authorization': `Bearer ${localStorage.getItem('token')}`
    //        }
    //    })
    //    .then((response) => response.json())
    //    .then((responseJson) => {
    //            console.log('sendata response=', responseJson);
    //            console.log('sendata token=', responseJson.token);
    //             if (responseJson.status === 1) {
    //    }})
    // }


    /****************** handle change for tab ********************* */
    const handleTabChange = (k) => {
        // setActiveTab(tab);
        setKey(k);
        // Call API to fetch data based on the selected tab
        // and update tableData state with the new data
        if (k === "Department") {
            setHeading("Department");
            fetch(baseURL + 'getalldepartment', {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })
                .then((response) => response.json())
                .then((responseJson) => {
                    console.log('sendata response of department=', responseJson);
                    if (responseJson.status == 1) {
                        var temp = [];
                        for (let i = 0; i < responseJson.data.length; i++) {
                            temp.push({
                                departmentid: responseJson.data[i].departmentid,
                                departmentname: responseJson.data[i].departmentname,
                                createdby: responseJson.data[i].createdby,
                                createdat: responseJson.data[i].createdat
                            });
                        }
                        setList(temp);
                        console.log("dept list", list);
                    }
                });
        }
        else if (k === "Main Classification") {
            setHeading("Main Classification");
            fetch(baseURL + 'getallmainclassification', {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })
                .then((response) => response.json())
                .then((responseJson) => {
                    console.log('sendata response of department=', responseJson);
                    if (responseJson.status == 1) {
                        var temp = [];
                        for (let i = 0; i < responseJson.data.length; i++) {
                            temp.push({
                                departmentid: responseJson.data[i].mainclassificationid,
                                departmentname: responseJson.data[i].mainclassificationname,
                                createdby: responseJson.data[i].createdby,
                                createdat: responseJson.data[i].createdat
                            });
                        }
                        setList(temp);
                    }
                });
        }
        else if (k === "Sub Classification") {
            setHeading("Sub Classification");
            fetch(baseURL + 'getallsubclassification', {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })
                .then((response) => response.json())
                .then((responseJson) => {
                    console.log('sendata response of department=', responseJson);
                    if (responseJson.status == 1) {
                        var temp = [];
                        for (let i = 0; i < responseJson.data.length; i++) {
                            temp.push({
                                departmentid: responseJson.data[i].subclassificationid,
                                departmentname: responseJson.data[i].subclassificationname,
                                createdby: responseJson.data[i].createdby,
                                createdat: responseJson.data[i].createdat
                            });
                        }
                        setList(temp);
                    }
                });
        }

        else if (k === "Asset Type") {
            setHeading("Asset Type");

            fetch(baseURL + 'getallassettype', {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })
                .then((response) => response.json())
                .then((responseJson) => {
                    console.log('sendata response of department=', responseJson);
                    if (responseJson.status == 1) {
                        var temp = [];
                        for (let i = 0; i < responseJson.data.length; i++) {
                            temp.push({
                                departmentid: responseJson.data[i].assettypeid,
                                departmentname: responseJson.data[i].assettypename,
                                createdby: responseJson.data[i].createdby,
                                createdat: responseJson.data[i].createdat
                            });
                        }
                        setList(temp);
                    }
                });
        }
    };


    //page render
    return (
        <div>
            <div>
                <Container>
                    {(showBtn) ?
                        <Row>
                            <Col className="col-md-8">
                                <Row>
                                    <h3>
                                        <b>
                                            {console.log("heading", heading)}
                                            &nbsp;&nbsp; {heading}
                                        </b>
                                    </h3>
                                    <br />  <div id="alertid">
                                        <Alert type={craetegrpalert_type} className="text-center"
                                            isOpen={craetegrpalert} onClick={() => onDismisss("craetegrpalert")}>
                                            {craetegrperror.status}: {craetegrperror.data}
                                        </Alert>
                                    </div>
                                </Row>
                                <br />
                                <Row>
                                    <Col className="col-md-12 col-xl-12">

                                        <div>
                                            <table class="table">
                                                <thead className="bg-dark">
                                                    <tr>
                                                        <th scope="col">Name</th>
                                                        <th scope="col">Created At</th>
                                                        <th scope="col">Created by</th>
                                                        {/* <th scope="col">Action</th> */}
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {list.map(item => (
                                                        <tr key={item.departmentid}>

                                                            <th scope="row">{item.departmentname}</th>
                                                            <td> {new Date(item.createdat).toISOString().split('T')[0]}</td>
                                                            <td>{item.createdby}</td>
                                                            {/* <td>  */}
                                                            {/* &nbsp;<button className="btn-edit" onClick={()=>updateDepartment}> 
                                                                            <i class="far fa-edit success" ></i>
                                                                      </button>
                                                                &nbsp; <button className="btn-delete" onClick={()=>deleteDepartment(item.departmentid)}> 
                                                                            <i class="fas fa-trash-alt" ></i>
                                                                        </button></td>  */}
                                                        </tr>
                                                    ))}
                                                </tbody>
                                            </table>
                                        </div>
                                    </Col>
                                </Row>
                            </Col>
                            <Col className="col-md-4">
                                <Row>
                                    <Tabs

                                        id="controlled-tab-example"
                                        activeKey={key}
                                        onSelect={handleTabChange}

                                        className="mb-3"
                                        style={{ width: '600px', fontSize: '9px ' }}>
                                        {/* /****************** Department card ********************* */}
                                        <Tab eventKey="Department" title="Department"  >
                                            <Card className="user-card">
                                                <Card.Body>
                                                    <center>
                                                        <b><h6>Add Department</h6></b>
                                                    </center>
                                                    <Image src={require("images/Add_users.png")} className="user_img" alt="bg" />
                                                    <br />
                                                    <center>
                                                        <FormGroup>
                                                            <Input
                                                                tabIndex="0"
                                                                // autoComplete="username"
                                                                name="dept_name"
                                                                onChange={(e) => changeHandler('dept_name', e)}
                                                                value={fields.dept_name}
                                                                type="text"
                                                                id="loginid"
                                                                className='user-control'
                                                                placeholder="Department Name"
                                                                required
                                                            />
                                                        </FormGroup>
                                                    </center>
                                                    <br />
                                                    <center>
                                                        <Button type="button" className="btn btn-dark" onClick={addDepartment}>&nbsp;Add Department&nbsp;</Button>
                                                    </center>
                                                </Card.Body>
                                            </Card>
                                        </Tab>

                                    {/* /****************** Main classification card ********************* */}
                                        <Tab eventKey="Main Classification" title="Main Classification"  >
                                            <Card className="user-card">
                                                <Card.Body>
                                                    <center>
                                                        <b><h6>Main Classification</h6></b>

                                                    </center>
                                                    <Image src={require("images/Add_users.png")} className="user_img" alt="bg" />
                                                    <br />
                                                    <center>
                                                        <form onSubmit={handleChange}>
                                                            <FormGroup>
                                                                <select
                                                                    value={selectedOption}
                                                                    onChange={handleOptionChange}
                                                                    className='user-control'>
                                                                    <option value="">Select Department</option>
                                                                    {deptList.map((option) => (
                                                                        <option key={option.departmentid} value={option.departmentid}>
                                                                            {option.departmentname}
                                                                        </option>
                                                                    ))}
                                                                </select>
                                                            </FormGroup>

                                                            <FormGroup>
                                                                <Input
                                                                    tabIndex="0"
                                                                    name="mainClassification"
                                                                    onChange={(e) => setMainClssification(e.target.value)}
                                                                    value={mainClassification}
                                                                    type="text"
                                                                    className='user-control'
                                                                    placeholder="Main Classification"
                                                                    required
                                                                />
                                                            </FormGroup>
                                                            <FormGroup>
                                                                <Input
                                                                    tabIndex="0"
                                                                    name="mainCode"
                                                                    onChange={(e) => setMainCode(e.target.value)}
                                                                    value={mainCode}
                                                                    type="text"
                                                                    className='user-control'
                                                                    placeholder="Code"
                                                                    required
                                                                />
                                                            </FormGroup>
                                                            <center>
                                                                <Button type="submit" className="btn btn-dark">&nbsp;Add Main Classification&nbsp;</Button>
                                                            </center>
                                                        </form>
                                                    </center>
                                                </Card.Body>
                                            </Card>
                                        </Tab>

                                    {/* /****************** sub classification card ********************* */}
                                        <Tab eventKey="Sub Classification" title="Sub Classification" >
                                            <Card className="user-card">
                                                <Card.Body>
                                                    <center>
                                                        <b><h5>Sub Classification</h5></b>
                                                    </center>
                                                    <br />
                                                    <form onSubmit={handleSubClssification}>
                                                        <center>
                                                            <FormGroup>
                                                                <select
                                                                    value={selectedOption}
                                                                    onChange={handleOptionChange}
                                                                    className='user-control'>
                                                                    <option value="">Select Department</option>
                                                                    {deptList.map((option) => (
                                                                        <option key={option.departmentid} value={option.departmentid}>
                                                                            {option.departmentname}
                                                                        </option>
                                                                    ))}
                                                                </select>
                                                            </FormGroup>


                                                            <FormGroup>
                                                                <select
                                                                    value={mainClsfn}
                                                                    onChange={(e) => setMainClsfn(e.target.value)}
                                                                    className='user-control'>
                                                                    <option value="">Select Main Classification</option>
                                                                    {mainList.map((options) => (
                                                                        <option key={options.mainclassificationid} value={options.mainclassificationid}>
                                                                            {options.mainclassificationname}
                                                                        </option>
                                                                    ))}
                                                                </select>
                                                            </FormGroup>


                                                            <FormGroup>
                                                                <Input
                                                                    tabIndex="0"
                                                                    name="subClassification"
                                                                    onChange={(e) => setSubClssification(e.target.value)}
                                                                    value={subClassification}
                                                                    type="text"
                                                                    className='user-control'
                                                                    placeholder="Sub Classification"
                                                                    required
                                                                />
                                                            </FormGroup>


                                                            <FormGroup>
                                                                <Input
                                                                    tabIndex="0"
                                                                    name="subCode"
                                                                    onChange={(e) => setSubCode(e.target.value)}
                                                                    value={subCode}
                                                                    type="text"
                                                                    className='user-control'
                                                                    placeholder="Code"
                                                                    required
                                                                />
                                                            </FormGroup>
                                                            <br />
                                                            <Button type="submit" className="btn btn-dark">&nbsp;Add Sub Classification&nbsp;</Button>
                                                        </center>
                                                    </form>
                                                </Card.Body>
                                            </Card>
                                        </Tab>

                                    {/* /****************** asset type card ********************* */}
                                        <Tab eventKey="Asset Type" title="Asset Type" >
                                            <Card className="user-card">
                                                <Card.Body>
                                                    <center>
                                                        <b><h5>Asset Type</h5></b>
                                                    </center>
                                                    <br />
                                                    <center>
                                                        <form onSubmit={addAssetType}>
                                                            <FormGroup>
                                                                <select
                                                                    name='hname'
                                                                    className='user-control'
                                                                    value={selectedOption}
                                                                    onChange={handleOptionChange}>
                                                                    <option value='select_house'>Select Department</option>
                                                                    {deptList.map(listOption => (
                                                                        <option id={listOption.departmentid} key={listOption.departmentid} value={listOption.departmentid}>
                                                                            {listOption.departmentname}
                                                                        </option>
                                                                    ))}
                                                                </select>
                                                            </FormGroup>

                                                            <FormGroup>
                                                                <select
                                                                    value={selectedMainClsf}
                                                                    onChange={(e) => setSelectedMainClsf(e.target.value)}
                                                                    className='user-control'>
                                                                    <option value="">Select Main Classification</option>
                                                                    {mainList.map((options) => (
                                                                        <option key={options.mainclassificationid} value={options.mainclassificationid}>
                                                                            {options.mainclassificationname}
                                                                        </option>
                                                                    ))}
                                                                </select>
                                                            </FormGroup>

                                                            <FormGroup>
                                                                <select
                                                                    name='hname'
                                                                    className='user-control'
                                                                    value={selectedSubClsf}
                                                                    onChange={(e) => setSelectedSubClsf(e.target.value)}>
                                                                    <option value=' '>Sub Classification</option>
                                                                    {subList.map(listOption => (
                                                                        <option id={listOption.subclassificationid} key={listOption.subclassificationid} value={listOption.subclassificationid}>
                                                                            {listOption.subclassificationname}
                                                                        </option>
                                                                    ))}
                                                                </select>
                                                            </FormGroup>

                                                            <FormGroup>
                                                                <Input
                                                                    tabIndex="0"
                                                                    name={assetCode}
                                                                    onChange={(e) => setAssetType(e.target.value)}
                                                                    value={assetType}
                                                                    type="text"
                                                                    className={'user-control'}
                                                                    placeholder="  &nbsp;Asset Type"
                                                                    required
                                                                />
                                                            </FormGroup>

                                                            <FormGroup>
                                                                <Input
                                                                    tabIndex="0"
                                                                    name={assetCode}
                                                                    onChange={(e) => setAssetCode(e.target.value)}
                                                                    value={assetCode}
                                                                    type="text"
                                                                    className={'user-control'}
                                                                    placeholder="  &nbsp;Asset Code"
                                                                    required
                                                                />
                                                            </FormGroup>
                                                            <br />
                                                            <Button type="submit" className="btn btn-dark">&nbsp;Add Main Classification&nbsp;</Button>
                                                        </form>
                                                    </center>
                                                </Card.Body>
                                            </Card>
                                        </Tab>
                                    </Tabs>
                                </Row>
                            </Col>
                        </Row>
                        :
                        <Col className="col-md-12 col-xl-12">
                            <Row>
                                <Col className="col-md-6">
                                    <Row>
                                        &nbsp;&nbsp;&nbsp;<h3><b>Department Details</b></h3>
                                    </Row>
                                </Col>
                                <Col className="col-md-6">
                                </Col>
                            </Row>
                            <br />
                            {/* /****************** Table ********************* */}
                            <Row>
                                <Col className="col-md-12 col-xl-12">
                                    <table class="table">
                                        <thead className="bg-dark">
                                            <tr>
                                                <th scope="col">Department Name</th>
                                                <th scope="col">Created At</th>
                                                <th scope="col">Created by</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {list.map(item => (
                                                <tr key={item.departmentid}>
                                                    <th scope="row">{item.departmentname}</th>
                                                    <td>  {new Date(item.createdat).toISOString().split('T')[0]}</td>
                                                    <td>{item.createdby}</td>
                                                    {/* <td>
                                                            <button className="btn btn-success-btn-lg"  >
                                                                <i class="far fa-edit success" ></i> 
                                                                <i class="fas fa-trash-alt"></i>
                                                            </button>
                                                        </td>  */}
                                                </tr>
                                            ))}
                                        </tbody>
                                    </table>
                                </Col>
                            </Row>
                        </Col>
                    }
                </Container>
            </div>
        </div>
    )
}

export default Department;