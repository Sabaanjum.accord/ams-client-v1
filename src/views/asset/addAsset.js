import React, { useState, useEffect } from "react";
import {
    Card,
    Row,
    FormGroup,
    Input,
    Col, Button,
    Link, Label
} from 'component/components';
import { Container, Form } from "react-bootstrap";
import { baseURL } from "config/config";
import Tab from 'react-bootstrap/Tab';
import Tabs from 'react-bootstrap/Tabs';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { isBefore, isAfter } from "date-fns";



const AddAsset = () => {


    /******************Hooks for List which is getting the data from api********************* */
    const [list, setList] = useState([]);
    const [mainList, setMainList] = useState([]);
    const [subList, setSubList] = useState([]);
    const [assetList, setAssetList] = useState([]);
    const [buildingList, setBuildingList] = useState([]);
    const [floorList, setFloorList] = useState([]);
    const [locationList, setLocationList] = useState([]);

    /******************Hooks for drop down ********************* */
    const [selectedOption, setSelectedOption] = useState("");
    const [mainClassification, setMainClssification] = useState('');
    const [selectedSubClsf, setSelectedSubClsf] = useState("");
    const [assetType, setAssetType] = useState("");
    const [building, setBuilding] = useState("");
    const [floor, setFloor] = useState("");
    const [location, setLocation] = useState("");

    /******************Hooks for input fields ********************* */
    const [make, setMake] = useState("");
    const [user, setUser] = useState("");
    const [invoiceNo, setInvoiceNo] = useState("");
    const [invoiceDate, setInvoiceDate] = useState("");
    const [invoiceAmount, setInvoiceAmount] = useState("");
    const [invoiceSupplier, setInvoiceSupplier] = useState("");
    const [description, setDescription] = useState("");
    const [checkedInsurance, setCheckedInsurance] = useState("");
    const [checkedAMC, setCheckedAMC] = useState("");
    const [showInsurance, setInsurance] = useState(false);
    const [showAMC, setAMC] = useState(false);
    const [insPolicy, setInsPolicy] = useState("");
    const [Amc, setAmc] = useState("");
    const [amcStartPeriod, setAmcStartPeriod] = useState("");
    const [amcEndPeriod, setAmcEndPeriod] = useState("");
    const [amcAmount, setAmcAmount] = useState("");

    /******************Hooks for tabs ********************* */
    const [key, setKey] = useState('Invoice Details');

    /******************Hooks for Date picker ********************* */
    const [selectedDate, setSelectedDate] = useState(null);
    const [startDate, setStartDate] = useState(null);
    const [endDate, setEndDate] = useState(null);
    const [errorMessage, setErrorMessage] = useState("");
    const [startAmcDate, setStartAmcDate] = useState(null);
    const [endAmcDate, setEndAmcDate] = useState(null);
    const [errorAmcMessage, setErrorAmcMessage] = useState("");

    /****************** useEffect for getting the data ********************* */
    useEffect(() => {
        getAllDepartment();
        getAllMainClassification();
        getAllSubClassification();
        getAllAssetType();
        getAllBuilding();
        getAllFloor();
        getAllLocation();
    }, []);

    /****************** Handling selected option from the department ********************* */
    const handleOptionChange = (event) => {
        setSelectedOption(event.target.value);
    };

    /****************** for future work ********************* */
    // const handleInsurance = (event) => {
    //     setCheckedInsurance(event.target.checked);
    //     console.log("clicked");
    //     setInsurance(true);
    // }

    // const handleAMC = (event) => {
    //     setCheckedAMC(event.target.checked);
    //     console.log("clicked AMC");
    //     setAMC(true);
    // }

    /****************** Get All Department API ********************* */
    const getAllDepartment = (e) => {
        // e.preventDefault();
        {
            fetch(baseURL + 'getalldepartment', {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })
                .then((response) => response.json())
                .then((responseJson) => {
                    console.log('Department response=', responseJson);

                    if (responseJson.status == 1) {
                        var temp = [];
                        for (let i = 0; i < responseJson.data.length; i++) {
                            temp.push({
                                departmentid: responseJson.data[i].departmentid,
                                departmentname: responseJson.data[i].departmentname,
                                createdby: responseJson.data[i].createdby,
                                createdat: responseJson.data[i].createdat
                            });
                        }
                        setList(temp);

                    }
                });


        }
    };

    /****************** Get All Main Classification API ********************* */
    const getAllMainClassification = (e) => {

        {
            fetch(baseURL + 'getallmainclassification', {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })
                .then((response) => response.json())
                .then((responseJson) => {
                    console.log('mainClassification response=', responseJson);

                    if (responseJson.status == 1) {
                        var temp = [];
                        for (let i = 0; i < responseJson.data.length; i++) {
                            temp.push({
                                mainclassificationid: responseJson.data[i].mainclassificationid,
                                mainclassificationname: responseJson.data[i].mainclassificationname,
                                createdby: responseJson.data[i].createdby,
                                createdat: responseJson.data[i].createdat
                            });
                        }
                        setMainList(temp);

                    }
                });


        }
    };

    /****************** Get All Sub Classification API ********************* */
    const getAllSubClassification = (e) => {

        //  {
        //   fetch( baseURL + 'getallsubclassification', {
        //                         method: 'GET',
        //                         // body: JSON.stringify({
        //                         //     'mainclassificationid':mainClassification
        //                         // }),
        //                         headers: {
        //                             'Content-Type': 'application/json',
        //                             'Authorization': `Bearer ${localStorage.getItem('token')}`
        //                         }
        //                     })
        //                     .then((response) => response.json())
        //                     .then((responseJson) => {
        //                         console.log(' sub classification response=', responseJson);

        //                         if (responseJson.status == 1) {
        //                             var temp = [];
        //                             for (let i = 0; i < responseJson.data.length; i++) {
        //                                temp.push({
        //                                     subclassificationid: responseJson.data[i].subclassificationid,
        //                                     subclassificationname: responseJson.data[i].subclassificationname,
        //                                     createdby: responseJson.data[i].createdby,
        //                                    createdat: responseJson.data[i].createdat
        //                                });
        //                            }
        //                            setSubList(temp);

        //                        }
        //                         });


        // }
    };

    /****************** Get All Asset Type API ********************* */
    const getAllAssetType = (e) => {

        {
            fetch(baseURL + 'getallassettype', {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })
                .then((response) => response.json())
                .then((responseJson) => {
                    console.log('asset type response=', responseJson);

                    if (responseJson.status == 1) {
                        var temp = [];
                        for (let i = 0; i < responseJson.data.length; i++) {
                            temp.push({
                                assettypeid: responseJson.data[i].assettypeid,
                                assettypename: responseJson.data[i].assettypename,
                                createdby: responseJson.data[i].createdby,
                                createdat: responseJson.data[i].createdat
                            });
                        }
                        setAssetList(temp);

                    }
                });
        }
    };

    /****************** Get All Building API ********************* */
    const getAllBuilding = (e) => {

        {
            fetch(baseURL + 'getallbuilding', {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })
                .then((response) => response.json())
                .then((responseJson) => {
                    console.log('building response=', responseJson);

                    if (responseJson.status == 1) {
                        var temp = [];
                        for (let i = 0; i < responseJson.data.length; i++) {
                            temp.push({
                                buildingid: responseJson.data[i].buildingid,
                                buildingname: responseJson.data[i].buildingname,
                                createdby: responseJson.data[i].createdby,
                                createdat: responseJson.data[i].createdat
                            });
                        }
                        setBuildingList(temp);

                    }
                });
        }
    };

    /****************** Get All Floor API ********************* */
    const getAllFloor = (e) => {

        {
            fetch(baseURL + 'getallfloor', {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })
                .then((response) => response.json())
                .then((responseJson) => {
                    console.log(' floor response=', responseJson);

                    if (responseJson.status == 1) {
                        var temp = [];
                        for (let i = 0; i < responseJson.data.length; i++) {
                            temp.push({
                                floorid: responseJson.data[i].floorid,
                                floorname: responseJson.data[i].floorname,
                                createdby: responseJson.data[i].createdby,
                                createdat: responseJson.data[i].createdat
                            });
                        }
                        setFloorList(temp);

                    }
                });
        }
    };

    /****************** Get All Location API ********************* */
    const getAllLocation = (e) => {

        {
            fetch(baseURL + 'getalllocation', {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })
                .then((response) => response.json())
                .then((responseJson) => {
                    console.log(' location response=', responseJson);

                    if (responseJson.status == 1) {
                        var temp = [];
                        for (let i = 0; i < responseJson.data.length; i++) {
                            temp.push({
                                locationid: responseJson.data[i].locationid,
                                locationname: responseJson.data[i].locationname,
                                createdby: responseJson.data[i].createdby,
                                createdat: responseJson.data[i].createdat
                            });
                        }
                        setLocationList(temp);

                    }
                });
        }
    };

    /****************** Add Asset API ********************* */
    const callAddAsset = (e) => {
        e.preventDefault();

        const day = startDate.getDate();
        const month = startDate.getMonth() + 1;
        const year = startDate.getFullYear();

        const formattedStartDate = `${day}/${month}/${year}`;

        const Eday = endDate.getDate();
        const Emonth = endDate.getMonth() + 1;
        const Eyear = endDate.getFullYear();

        const formattedEndDate = `${Eday}/${Emonth}/${Eyear}`;

        const Invoiceday = selectedDate.getDate();
        const Invoicemonth = selectedDate.getMonth() + 1;
        const Invoiceyear = selectedDate.getFullYear();

        const formattedInvoiceDate = `${Invoiceday}/${Invoicemonth}/${Invoiceyear}`;

        const Amcday = startAmcDate.getDate();
        const Amcmonth = startAmcDate.getMonth() + 1;
        const Amcyear = startAmcDate.getFullYear();

        const formattedAmcDate = `${Amcday}/${Amcmonth}/${Amcyear}`;

        const AmcEndday = endAmcDate.getDate();
        const AmcEndmonth = endAmcDate.getMonth() + 1;
        const AmcEndyear = endAmcDate.getFullYear();

        const formattedAmcEndDate = `${AmcEndday}/${AmcEndmonth}/${AmcEndyear}`;

        console.log("hi department", selectedOption);
        console.log("hi mainclassification", mainClassification);
        console.log("hi subclassification", selectedSubClsf);
        console.log("hi assettype", assetType);
        console.log("hi  building", building);
        console.log("hi  floor ", floor);
        console.log("hi  location", location);
        console.log("hi  make", make);
        console.log("hi  description", description);
        console.log("hi  amount", invoiceAmount);
        // console.log("hi  remark",);
        console.log("hi invoice no ", invoiceNo);
        console.log("hi  incoice date", formattedInvoiceDate);
        console.log("hi  supplier", invoiceSupplier);
        console.log("hi  userid", user);
        // console.log("hi  sl no.", slNo);
        console.log("hi   insurance policy", insPolicy);
        console.log("hi   insurance  start period", formattedStartDate);
        console.log("hi   insurance  end period", formattedEndDate);

        console.log("hi   amc  start period", formattedAmcDate);
        console.log("hi   insurance  end period", formattedAmcEndDate);



        fetch(baseURL + 'addasset', {
            method: 'POST',
            body: JSON.stringify({
                'userid': localStorage.getItem('userid'),
                'departmentid': selectedOption,
                'username': localStorage.getItem('username'),
                'mainclassificationid': mainClassification,
                'subclassificationid': selectedSubClsf,
                'assettypeid': assetType,
                'buildingid': building,
                'floorid': floor,
                'locationid': location,
                'make': make,
                'description': description,
                'amount': invoiceAmount,
                'invoiceno': invoiceNo,
                'invoicedate': formattedInvoiceDate,
                'supplier': invoiceSupplier,
                'isinsurance': 1,
                "insurance":
                {
                    "insurancepolicynum": insPolicy,
                    "periodstart": formattedStartDate,
                    "periodend": formattedEndDate
                },
                "amc":
                {
                    "insurancepolicynum": Amc,
                    "periodstart": formattedAmcDate,
                    "periodend": formattedAmcEndDate,
                    "amount": amcAmount
                },

            }),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then((response) => response.json())
            .then((responseJson) => {

                console.log('sendata add asset=', responseJson);
                console.log('sendata token=', responseJson.token);

                if (responseJson.status === 1) {
                }
            })
    }

    /****************** to clear the fields ********************* */
    const handleClearForm = () => {
        setSelectedOption('');
        setMainClssification('');
        setSelectedSubClsf('');
        setAssetType('');
        setMake('');
        setBuilding('');
        setFloor('');
        setLocation('');
        setUser('');
        setDescription('');
        setInvoiceNo('');
        setInvoiceDate('');
        setInvoiceAmount('');
        setInvoiceSupplier('');
        setInsPolicy('');
        setAmc('');
        setAmcAmount('');
        setSelectedDate('');
        setStartDate('');
        setEndDate('');
        setStartAmcDate('');
        setEndAmcDate('');
    }

    /****************** to handle the start date ********************* */
    const handleStartDateChange = (date) => {
        if (endDate && isAfter(date, endDate)) {
            setErrorMessage("Start date cannot be after end date.");
        }
        else {
            setStartDate(date);
            setErrorMessage("");
        }
    };

    /****************** to handle the end date ********************* */
    const handleEndDateChange = (date) => {
        if (startDate && isBefore(date, startDate)) {
            setErrorMessage("End date cannot be before start date.");
        }
        else {
            setEndDate(date);
            setErrorMessage("");
        }
    };

    /****************** to handle the amc start date ********************* */
    const handleStartAmcDate = (date) => {
        if (endAmcDate && isAfter(date, endAmcDate)) {
            setErrorAmcMessage("Start date cannot be after end date.");
        }
        else {
            setStartAmcDate(date);
            setErrorAmcMessage("");
        }
    };

    /****************** to handle the amc end date ********************* */
    const handleEndAmcDateChange = (date) => {
        if (startAmcDate && isBefore(date, startAmcDate)) {
            setErrorAmcMessage("End date cannot be before start date.");
        } else {
            setEndAmcDate(date);
            setErrorAmcMessage("");
        }
    };

    /****************** to handle mainclassification change ********************* */
    const handleMainClassificationChange = (e) => {
        console.log("ert", e.target.value);

        setMainClssification(e.target.value)
        fetch(baseURL + 'getallsubclassificationbymcid', {
            method: 'POST',
            body: JSON.stringify({
                'mainclassificationid': e.target.value
            }),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(' sub classification response=', responseJson);

                if (responseJson.status == 1) {
                    var temp = [];
                    for (let i = 0; i < responseJson.data.length; i++) {
                        temp.push({
                            subclassificationid: responseJson.data[i].subclassificationid,
                            subclassificationname: responseJson.data[i].subclassificationname,
                            createdby: responseJson.data[i].createdby,
                            createdat: responseJson.data[i].createdat
                        });
                    }
                    setSubList(temp);

                }
            });


    }

    /****************** page render ********************* */
    return (
        <div>
            <div>
                <Container>
                    <Form onSubmit={callAddAsset}>
                        <Row>
                            <Col className='col-md-4'>
                                <h4>
                                    <b>
                                        &nbsp;&nbsp;&nbsp;Add Asset
                                    </b>
                                </h4>
                            </Col>
                            <Col className='col-md-4'>

                            </Col>
                            <Col className='col-md-4'>
                                {/* /****************** Back button ********************* */}
                                <Link to="/asset"  >
                                    <Button type="button" className="btn btn-dark"  >
                                        &nbsp;&nbsp; Back&nbsp;   &nbsp;
                                    </Button>
                                </Link>
                                {/* /****************** Clear button ********************* */}
                                <Button type="button" className="btn btn-danger" onClick={() => handleClearForm()}>
                                    &nbsp; &nbsp;Clear&nbsp; &nbsp;
                                </Button>

                                { /* /****************** Save button ********************* */}
                                <Button type="submit" className="btn btn-success" >
                                    &nbsp; &nbsp; Save&nbsp; &nbsp;
                                </Button>
                            </Col>
                        </Row>
                        {/* /****************** Add Asset Card ********************* */}
                        <Card>
                            <Row>
                                <Col className="col-md-4">
                                    <br />
                                    <Row>
                                        <Col className='col-md-1'></Col>
                                        <Col className="col-md-8">
                                            <Label>
                                                Department
                                            </Label>
                                            <FormGroup>

                                                <select
                                                    value={selectedOption}
                                                    onChange={handleOptionChange}
                                                    className='user-controls'>
                                                    <option value=""> Select</option>
                                                    {list.map((option) => (
                                                        <option key={option.departmentid} value={option.departmentid}>
                                                            {option.departmentname}
                                                        </option>
                                                    ))}
                                                </select>

                                            </FormGroup>

                                        </Col>

                                        <Col className="col-md-3">
                                            <br />
                                            &nbsp;
                                            <Link to="/department"  >
                                                <i className="fa fa-plus-circle fa-lg" ></i>
                                            </Link>
                                        </Col>
                                    </Row>

                                    <Row>
                                        <Col className='col-md-1'></Col>
                                        <Col className="col-md-8">
                                            <Label>
                                                Main Classification
                                            </Label>
                                            {console.log("main clasiification", mainClassification)}
                                            <FormGroup>
                                                <select
                                                    value={mainClassification}
                                                    onChange={(e) => handleMainClassificationChange(e)}
                                                    className='user-controls'>
                                                    <option value="">Select</option>
                                                    {mainList.map((options) => (
                                                        <option key={options.mainclassificationid} value={options.mainclassificationid}>
                                                            {options.mainclassificationname}
                                                        </option>
                                                    ))}
                                                </select>
                                            </FormGroup>
                                        </Col>
                                        <Col className="col-md-3">
                                            <br />
                                            &nbsp;
                                            <Link to="/department"  >
                                                <i className="fa fa-plus-circle fa-lg" ></i>
                                            </Link>
                                        </Col>
                                    </Row>

                                    <Row>
                                        <Col className='col-md-1'></Col>
                                        <Col className="col-md-8">
                                            <Label>
                                                Sub Classification
                                            </Label>
                                            <FormGroup>
                                                <select
                                                    name='hname'
                                                    className='user-controls'
                                                    value={selectedSubClsf}
                                                    onChange={(e) => setSelectedSubClsf(e.target.value)}>
                                                    <option value=' '>Select</option>
                                                    {subList.map(listOption => (
                                                        <option id={listOption.subclassificationid} key={listOption.subclassificationid} value={listOption.subclassificationid}>
                                                            {listOption.subclassificationname}
                                                        </option>
                                                    ))}
                                                </select>
                                            </FormGroup>
                                        </Col>
                                        <Col className="col-md-3">
                                            <br />
                                            &nbsp;
                                            <Link to="/department"  >
                                                <i className="fa fa-plus-circle fa-lg" ></i>
                                            </Link>
                                        </Col>
                                    </Row>

                                    <Row>
                                        <Col className='col-md-1'></Col>
                                        <Col className="col-md-8">
                                            <Label>
                                                Asset Type
                                            </Label>
                                            <FormGroup>
                                                <select
                                                    name='hname'
                                                    className='user-controls'
                                                    value={assetType}
                                                    onChange={(e) => setAssetType(e.target.value)}>
                                                    <option value=' '>Select</option>
                                                    {assetList.map(listOption => (
                                                        <option id={listOption.assettypeid} key={listOption.assettypeid} value={listOption.assettypeid}>
                                                            {listOption.assettypename}
                                                        </option>
                                                    ))}
                                                </select>
                                            </FormGroup>
                                        </Col>
                                        <Col className="col-md-3">
                                            <br />
                                            &nbsp;
                                            <Link to="/department"  >
                                                <i className="fa fa-plus-circle fa-lg" ></i>
                                            </Link>
                                        </Col>
                                    </Row>

                                    <Row>
                                        <Col className='col-md-1'></Col>


                                        <Col className="col-md-8">
                                            <Label>
                                                Make
                                            </Label>
                                            <FormGroup>
                                                <Input
                                                    tabIndex="0"
                                                    // autoComplete="username"
                                                    name="make"
                                                    onChange={(e) => setMake(e.target.value)}
                                                    value={make}
                                                    type="text"

                                                    className='user-controls'
                                                    placeholder="Make"
                                                    required
                                                />

                                            </FormGroup>
                                        </Col>
                                        <Col className="col-md-3">
                                        </Col>
                                    </Row>
                                </Col>

                                <Col className="col-md-4">
                                    <br />
                                    <Row>
                                        <Col className='col-md-1'></Col>
                                        <Col className="col-md-8">
                                            <Label>
                                                Building
                                            </Label>
                                            <FormGroup>
                                                <select
                                                    name='hname'
                                                    className='user-controls'
                                                    value={building}
                                                    onChange={(e) => setBuilding(e.target.value)}>
                                                    <option value=' '>Select</option>
                                                    {buildingList.map(listOption => (
                                                        <option id={listOption.buildingid} key={listOption.buildingid} value={listOption.buildingid}>
                                                            {listOption.buildingname}
                                                        </option>
                                                    ))}
                                                </select>
                                            </FormGroup>
                                        </Col>
                                        <Col className="col-md-3">
                                            <br />
                                            &nbsp;
                                            <Link to="/addBuilding"  >
                                                <i className="fa fa-plus-circle fa-lg" ></i>
                                            </Link>
                                        </Col>
                                    </Row>

                                    <Row>
                                        <Col className='col-md-1'></Col>
                                        <Col className="col-md-8">
                                            <Label>
                                                Floor
                                            </Label>
                                            <FormGroup>
                                                <select
                                                    name='hname'
                                                    className='user-controls'
                                                    value={floor}
                                                    onChange={(e) => setFloor(e.target.value)}>
                                                    <option value=' '>Select</option>
                                                    {floorList.map(listOption => (
                                                        <option id={listOption.floorid} key={listOption.floorid} value={listOption.floorid}>
                                                            {listOption.floorname}
                                                        </option>
                                                    ))}
                                                </select>
                                            </FormGroup>
                                        </Col>
                                        <Col className="col-md-3">
                                            <br />
                                            &nbsp;
                                            <Link to="/addBuilding"  >
                                                <i className="fa fa-plus-circle fa-lg" ></i>
                                            </Link>
                                        </Col>
                                    </Row>

                                    <Row>
                                        <Col className='col-md-1'></Col>
                                        <Col className="col-md-8">
                                            <Label>
                                                Location
                                            </Label>
                                            <FormGroup>
                                                <select
                                                    name='hname'
                                                    className='user-controls'
                                                    value={location}
                                                    onChange={(e) => setLocation(e.target.value)}>
                                                    <option value=' '>Select</option>
                                                    {locationList.map(listOption => (
                                                        <option id={listOption.locationid} key={listOption.locationid} value={listOption.locationid}>
                                                            {listOption.locationname}
                                                        </option>
                                                    ))}
                                                </select>
                                            </FormGroup>
                                        </Col>
                                        <Col className="col-md-3">
                                            <br />
                                            &nbsp;
                                            <Link to="/addBuilding"  >
                                                <i className="fa fa-plus-circle fa-lg" ></i>
                                            </Link>
                                        </Col>
                                    </Row>

                                    <Row>
                                        <Col className='col-md-1'></Col>
                                        <Col className="col-md-8">
                                            <Label>
                                                User
                                            </Label>
                                            <FormGroup>
                                                <Input
                                                    tabIndex="0"
                                                    // autoComplete="username"
                                                    name="user"
                                                    onChange={(e) => setUser(e.target.value)}
                                                    value={user}
                                                    type="text"

                                                    className='user-controls'
                                                    placeholder="User"
                                                    required
                                                />

                                            </FormGroup>
                                        </Col>
                                        <Col className='col-md-1'></Col>
                                        <Col className="col-md-2">
                                        </Col>
                                    </Row>

                                    <Row>
                                        {/* <Col className='col-md-1'></Col> */}
                                        <Col className="col-md-8">
                                            <Row>
                                                <Col className='col-md-1'>

                                                </Col>
                                                <Col className="col-md-10">
                                                    <Label>
                                                        Description
                                                    </Label>
                                                    <FormGroup className="mb-3"  >
                                                        <Form.Control as="textarea" placeholder="Description" value={description} onChange={(e) => setDescription(e.target.value)} style={{ width: '250px' }} rows={2} />
                                                    </FormGroup>
                                                </Col>
                                                <Col className='col-md-1'>

                                                </Col>

                                            </Row>
                                            {/* /****************** For future use ********************* */}
                                            {/* <FormGroup>
                                                 <Input
                                                    tabIndex="0"
                                                    // autoComplete="username"
                                                    name="slNo"
                                                    onChange={(e) => setSlNo(e.target.value)}
                                                    value={ slNo}
                                                    type="text"
                                                  
                                                    className='user-controls'
                                                    placeholder="SL No."
                                                    required
                                                    />
                                                  
                                           </FormGroup> */}
                                        </Col>
                                        <Col className='col-md-1'></Col>
                                        <Col className="col-md-2">
                                            {/* <i className="fa fa-plus-square" ></i> */}
                                        </Col>
                                    </Row>
                                </Col>
                                <Col className="col-md-4">
                                    <Tabs
                                        id="controlled-tab-example"
                                        activeKey={key}
                                        onSelect={(k) => setKey(k)}
                                        className="mb-1"
                                        style={{ width: '600px', fontSize: '9px ' }} >
                                        {/* /****************** Invoice details tab ********************* */}
                                        <Tab eventKey="Invoice Details" title="Invoice Details"  >
                                            <Card>
                                                <h3>
                                                    <center>
                                                        <b>
                                                            Invoice Details
                                                        </b>
                                                    </center>
                                                </h3>
                                                <br />
                                                <center>
                                                    <FormGroup>
                                                        <Label className="labels-invNo">
                                                            Inv. No
                                                        </Label>
                                                        <br />
                                                        <Input
                                                            tabIndex="0"
                                                            // autoComplete="username"
                                                            name="invoiceNo"
                                                            onChange={(e) => setInvoiceNo(e.target.value)}
                                                            value={invoiceNo}
                                                            type="text"

                                                            className='user-controls'
                                                            placeholder="Inv. No." />
                                                    </FormGroup>
                                                    <FormGroup>
                                                        <Label className="labels-invDate">
                                                            Inv.Date
                                                        </Label>
                                                        <br />
                                                        <DatePicker
                                                            value={selectedDate}
                                                            selected={selectedDate}
                                                            onChange={date => setSelectedDate(date)}
                                                            className='user-controls'
                                                            name="selectedDate"
                                                            dateFormat="MM/dd/yyyy" />
                                                    </FormGroup>
                                                    <FormGroup>
                                                        <Label className="label-invAmount">
                                                            &nbsp;   Inv. Amount
                                                        </Label>
                                                        <br />
                                                        <Input
                                                            tabIndex="0"
                                                            name="invoiceAmount"
                                                            onChange={(e) => setInvoiceAmount(e.target.value)}
                                                            value={invoiceAmount}
                                                            type="text"
                                                            className='user-controls'
                                                            placeholder="Amount" />
                                                    </FormGroup>
                                                    <FormGroup>
                                                        <Label className="label-supplier">
                                                            Supplier
                                                        </Label>
                                                        <br />
                                                        <Input
                                                            tabIndex="0"
                                                            name="invoiceSupplier"
                                                            onChange={(e) => setInvoiceSupplier(e.target.value)}
                                                            value={invoiceSupplier}
                                                            type="text"
                                                            className='user-controls'
                                                            placeholder="Supplier" />
                                                    </FormGroup>
                                                </center>
                                            </Card>
                                        </Tab>
                                        {/* /****************** Insurance details tab ********************* */}
                                        <Tab eventKey="Insurance Details" title="Insurance Details"  >
                                            <Card>
                                                <h3>
                                                    <center>
                                                        <b>
                                                            Insurance Details
                                                        </b>
                                                    </center>
                                                </h3>
                                                <br />
                                                <center>
                                                    <FormGroup>
                                                        <Label className="insPolicy">
                                                            Insurance Policy
                                                        </Label>
                                                        <Input
                                                            tabIndex="0"
                                                            // autoComplete="username"
                                                            name="insPolicy"
                                                            onChange={(e) => setInsPolicy(e.target.value)}
                                                            value={insPolicy}
                                                            type="text"
                                                            className='user-controls'
                                                            placeholder="Insurance Policy" />
                                                    </FormGroup>
                                                    <FormGroup>
                                                        <Label className="insSDate">
                                                            Insurance Start Period
                                                        </Label>
                                                        <DatePicker
                                                            value={startDate}
                                                            selected={startDate}
                                                            onChange={(startDate) => handleStartDateChange(startDate)}
                                                            dateFormat="MM/dd/yyyy"
                                                            className='user-controls'
                                                            placeholder="Insurance Start Period" />
                                                    </FormGroup>
                                                    <FormGroup>
                                                        <Label className="insEDate">
                                                            Insurance End Period
                                                        </Label>
                                                        <DatePicker
                                                            selected={endDate}
                                                            onChange={handleEndDateChange}
                                                            dateFormat="MM/dd/yyyy"
                                                            className='user-controls' />
                                                    </FormGroup>
                                                    {errorMessage && <div style={{ color: 'red' }}>{errorMessage}</div>}
                                                    <br /><br /><br /><br />
                                                    {/* <FormGroup>
                                                            <Input
                                                                tabIndex="0"
                                                                // autoComplete="username"
                                                                name="invoiceSupplier"
                                                                onChange={(e) => setInvoiceSupplier(e.target.value)}
                                                                value={ invoiceSupplier}
                                                                type="text"
                                                                
                                                                className='user-controls'
                                                                placeholder="Supplier"
                                                                required
                                                                />
                                                        </FormGroup>
                                                       <br/> */}
                                                </center>
                                            </Card>
                                        </Tab>
                                        {/* /****************** AMC details tab ********************* */}
                                        <Tab eventKey="AMC Details" title="AMC Details"  >
                                            <Card>
                                                <h3>
                                                    <center>
                                                        <b>
                                                            AMC Details
                                                        </b>
                                                    </center>
                                                </h3>
                                                <br />
                                                <center>
                                                    <FormGroup>
                                                        <Label className="insEDate">
                                                            Insurance  Policy No.
                                                        </Label>
                                                        <Input
                                                            tabIndex="0"
                                                            // autoComplete="username"
                                                            name={Amc}
                                                            onChange={(e) => setAmc(e.target.value)}
                                                            value={Amc}
                                                            type="text"
                                                            className='user-controls'
                                                            placeholder="Insurance Policy No." />
                                                    </FormGroup>
                                                    <FormGroup>
                                                        <Label className="amcsDate">
                                                            AMC Start Date
                                                        </Label>
                                                        <DatePicker
                                                            value={startAmcDate}
                                                            selected={startAmcDate}
                                                            onChange={(startAmcDate) => handleStartAmcDate(startAmcDate)}
                                                            dateFormat="MM/dd/yyyy"
                                                            className='user-controls'
                                                            placeholder="Insurance Start Period" />
                                                    </FormGroup>
                                                    <FormGroup>
                                                        <Label className="amcEDate">
                                                            AMC End Date
                                                        </Label>
                                                        <DatePicker
                                                            selected={endAmcDate}
                                                            onChange={handleEndAmcDateChange}
                                                            dateFormat="MM/dd/yyyy"
                                                            className='user-controls' />
                                                    </FormGroup>
                                                    {errorAmcMessage && <div style={{ color: 'red' }}>{errorAmcMessage}</div>}

                                                    <FormGroup>
                                                        <Label className="amcADate">
                                                            AMC Amount
                                                        </Label>
                                                        <Input
                                                            tabIndex="0"
                                                            // autoComplete="username"
                                                            name={amcAmount}
                                                            onChange={(e) => setAmcAmount(e.target.value)}
                                                            value={amcAmount}
                                                            type="text"
                                                            className='user-controls'
                                                            placeholder="AMC Amount" />
                                                    </FormGroup>
                                                </center>
                                            </Card>
                                        </Tab>
                                    </Tabs>
                                </Col>
                            </Row>
                        </Card>
                    </Form>
                </Container>
            </div>
        </div>
    )
}
export default AddAsset;