import React, { useState, useEffect } from "react";
import {
    Card,
    Row,
    FormGroup,
    Input,
    Col, Button,
    Link, Label, Alert
} from 'component/components';
import { Container, Form } from "react-bootstrap";
import { baseURL } from "config/config";
import Tab from 'react-bootstrap/Tab';
import Tabs from 'react-bootstrap/Tabs';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { isBefore, isAfter } from "date-fns";



const UpdateAsset = () => {

    /****************** Hooks for input fields ********************* */
    const [make, setMake] = useState("");
    const [invoiceNo, setInvoiceNo] = useState("");
    const [invoiceAmount, setInvoiceAmount] = useState("");
    const [invoiceSupplier, setInvoiceSupplier] = useState("");
    const [insPolicy, setInsPolicy] = useState("");
    const [Amc, setAmc] = useState("");
    const [amcAmount, setAmcAmount] = useState("");
    const [key, setKey] = useState('Invoice Details');
    const [selectedDate, setSelectedDate] = useState(null);
    const [startDate, setStartDate] = useState(null);
    const [endDate, setEndDate] = useState(null);
    const [errorMessage, setErrorMessage] = useState("");
    const [startAmcDate, setStartAmcDate] = useState(null);
    const [endAmcDate, setEndAmcDate] = useState(null);
    const [errorAmcMessage, setErrorAmcMessage] = useState("");
    const [assetData, setAssetData] = useState({});

    /******************Hooks for error msg ********************* */
    const [craetegrpalert, setCraetegrpalert] = useState(false);
    const [craetegrpalert_type, setCraetegrpalertType] = useState("");
    const [craetegrperror, setCraetegrperror] = useState("");

    /****************** Hooks for changing the state ********************* */
    const [formData, setFormData] = useState({
        make: '',
        username: '',
        description: ''
    });

    /****************** useEffect for getting the data from the asset list page ********************* */
    useEffect(() => {
        const data = sessionStorage.getItem("assetData");
        setAssetData(JSON.parse(data));
        setFormData({
            make: JSON.parse(data).make,
            username: JSON.parse(data).username,
            description: JSON.parse(data).description,
            // assettypename: JSON.parse(data).assettypename
        });
    }, []);

    /******************Handle the change of states variable ********************* */
    const handleInputChange = (e) => {
        const { name, value } = e.target;
        setFormData({ ...formData, [name]: value });
    };

    /****************** handle closing of error msg ********************* */
    const onDismisss = () => {
        setCraetegrpalert(false);
        setCraetegrperror("");
        setCraetegrpalertType("primary");
    };

    /****************** update asset API ********************* */
    const callUpdateAsset = (e) => {
        e.preventDefault();
        console.log("make", formData.make);
        console.log("username", formData.username);
        console.log("description", formData.description);
        fetch(baseURL + 'updateasset', {
            method: 'PATCH',
            body: JSON.stringify({
                'id': assetData.ID,
                'userid': localStorage.getItem('userid'),
                'departmentid': assetData.departmentid,
                'username': localStorage.getItem('username'),
                'mainclassificationid': assetData.mainClassificationid,
                'subclassificationid': assetData.subclassificationid,
                'assettypeid': assetData.assettypeid,
                'buildingid': assetData.buildingid,
                'floorid': assetData.floorid,
                'locationid': assetData.locationid,
                'make': formData.make,
                'description': formData.description,
                'amount': assetData.amount,
                'invoiceno': assetData.invoiceno,
                'invoicedate': assetData.invoicedate,
                'supplier': assetData.supplier,
                'isinsurance': 1,
                "insurance":
                {
                    "insurancepolicynum": assetData.insurance_policynumber,
                    "periodstart": assetData.insurance_periodstart,
                    "periodend": assetData.insurance_periodend
                },
                // "amc":
                // { 
                //     "insurancepolicynum":Amc,
                //     "periodstart":formattedAmcDate,
                //     "periodend":formattedAmcEndDate,
                //     "amount":amcAmount
                // },

            }),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then((response) => response.json())
            .then((responseJson) => {

                console.log('sendata add asset=', responseJson);
                console.log('sendata token=', responseJson.token);

                if (responseJson.status === 1) {
                    setCraetegrpalert(true);
                    setCraetegrpalertType("success");
                    setCraetegrperror(responseJson);
                    sessionStorage.removeItem("assetData");
                }
            })
    }

    /****************** handle insurance start date of the fields ********************* */
    const handleStartDateChange = (date) => {
        if (endDate && isAfter(date, endDate)) {
            setErrorMessage("Start date cannot be after end date.");

        }
        else {
            setStartDate(date);
            setErrorMessage("");
        }
    };

    /****************** handle insurance end date of the fields ********************* */
    const handleEndDateChange = (date) => {
        if (startDate && isBefore(date, startDate)) {
            setErrorMessage("End date cannot be before start date.");
        }
        else {
            setEndDate(date);
            setErrorMessage("");
        }
    };

    /****************** handle AMC start date of the fields ********************* */
    const handleStartAmcDate = (date) => {
        if (endAmcDate && isAfter(date, endAmcDate)) {
            setErrorAmcMessage("Start date cannot be after end date.");
        }
        else {
            setStartAmcDate(date);
            setErrorAmcMessage("");
        }
    };

    /****************** handle AMC end date of the fields ********************* */
    const handleEndAmcDateChange = (date) => {
        if (startAmcDate && isBefore(date, startAmcDate)) {
            setErrorAmcMessage("End date cannot be before start date.");
        }
        else {
            setEndAmcDate(date);
            setErrorAmcMessage("");
        }
    };

    /****************** page render ********************* */
    return (
        <div>
            <div>
                <Container>
                    <Form onSubmit={callUpdateAsset}>
                        <Row>
                            <Col className='col-md-4'>
                                <h4>
                                    <b>
                                        &nbsp;&nbsp;&nbsp;Update Asset
                                    </b>
                                </h4>
                            </Col>
                            <Col className='col-md-4'> </Col>
                            <Col className='col-md-4'>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;
                                <Link to="/asset"  >
                                    <Button type="button" className="btn btn-dark"  >
                                        &nbsp; Back   &nbsp;
                                    </Button>
                                </Link>
                                <Button type="submit" className="btn btn-success" >
                                    &nbsp; Save&nbsp;
                                </Button>
                            </Col>
                            <div id="alertid">
                                <Alert type={craetegrpalert_type} className="text-center"
                                    isOpen={craetegrpalert} onClick={() => onDismisss("craetegrpalert")}>
                                    {craetegrperror.status}: {craetegrperror.data}
                                </Alert>
                            </div>
                        </Row>
                        {/* /****************** Update asset card ********************* */}
                        <Card>
                            <Row>
                                <Col className="col-md-4">
                                    <br />
                                    <Row>
                                        <Col className='col-md-1'></Col>
                                        <Col className="col-md-8">
                                            <Label>
                                                Department
                                            </Label>
                                            <FormGroup>
                                                <Input
                                                    tabIndex="0"
                                                    // autoComplete="username"
                                                    name="make"
                                                    onChange={(e) => setMake(e.target.value)}
                                                    value={assetData.departmentname}
                                                    type="text"
                                                    className='user-controls'
                                                    placeholder="Make"
                                                    required />
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col className='col-md-1'></Col>
                                        <Col className="col-md-8">
                                            <Label>
                                                Main Classification
                                            </Label>
                                            <FormGroup>
                                                <Input
                                                    tabIndex="0"
                                                    name="make"
                                                    onChange={(e) => setMake(e.target.value)}
                                                    value={assetData.mainclassificationname}
                                                    type="text"
                                                    className='user-controls'
                                                    placeholder="Make"
                                                    required />
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col className='col-md-1'></Col>
                                        <Col className="col-md-8">
                                            <Label>
                                                Sub Classification
                                            </Label>
                                            <FormGroup>
                                                <Input
                                                    tabIndex="0"
                                                    name="make"
                                                    onChange={(e) => setMake(e.target.value)}
                                                    value={assetData.subclassificationname}
                                                    type="text"
                                                    className='user-controls'
                                                    placeholder="Make"
                                                    required />
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col className='col-md-1'></Col>
                                        <Col className="col-md-8">
                                            <Label>
                                                Asset Type
                                            </Label>
                                            <FormGroup>
                                                <Input
                                                    tabIndex="0"
                                                    name="make"
                                                    onChange={(e) => setMake(e.target.value)}
                                                    value={assetData.assettypename}
                                                    type="text"
                                                    className='user-controls'
                                                    placeholder="Make"
                                                    required />
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col className='col-md-1'></Col>
                                        <Col className="col-md-8">
                                            <Label>
                                                Make
                                            </Label>
                                            <FormGroup>
                                                <Input
                                                    tabIndex="0"
                                                    name="make"
                                                    onChange={handleInputChange}
                                                    value={formData.make}
                                                    type="text"
                                                    className='user-controls'
                                                    placeholder="Make"
                                                    required />
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                </Col>

                                <Col className="col-md-4">
                                    <br />
                                    <Row>
                                        <Col className='col-md-1'></Col>
                                        <Col className="col-md-8">
                                            <Label>
                                                Building
                                            </Label>
                                            <FormGroup>
                                                <Input
                                                    tabIndex="0"
                                                    name="make"
                                                    onChange={(e) => setMake(e.target.value)}
                                                    value={assetData.buildingname}
                                                    type="text"
                                                    className='user-controls'
                                                    placeholder="Make"
                                                    required />
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col className='col-md-1'></Col>
                                        <Col className="col-md-8">
                                            <Label>
                                                Floor
                                            </Label>
                                            <FormGroup>
                                                <Input
                                                    tabIndex="0"
                                                    name="make"
                                                    onChange={(e) => setMake(e.target.value)}
                                                    value={assetData.floorname}
                                                    type="text"
                                                    className='user-controls'
                                                    placeholder="Make"
                                                    required />
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col className='col-md-1'></Col>
                                        <Col className="col-md-8">
                                            <Label>
                                                Location
                                            </Label>
                                            <FormGroup>
                                                <Input
                                                    tabIndex="0"
                                                    name="make"
                                                    onChange={(e) => setMake(e.target.value)}
                                                    value={assetData.locationname}
                                                    type="text"
                                                    className='user-controls'
                                                    placeholder="Make"
                                                    required />
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col className='col-md-1'></Col>
                                        <Col className="col-md-8">
                                            <Label>
                                                User
                                            </Label>
                                            <FormGroup>
                                                <Input
                                                    tabIndex="0"
                                                    name="username"
                                                    onChange={handleInputChange}
                                                    value={formData.username}
                                                    type="text"
                                                    className='user-controls'
                                                    placeholder="Make"
                                                    required />
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col className="col-md-8">
                                            <Row>
                                                <Col className='col-md-1'></Col>
                                                <Col className="col-md-10">
                                                    <Label>
                                                        Description
                                                    </Label>
                                                    <FormGroup className="mb-3"  >
                                                        <Form.Control as="textarea" name="description" placeholder="Description" value={formData.description} onChange={handleInputChange} style={{ width: '250px' }} rows={2} />
                                                    </FormGroup>
                                                </Col>
                                                <Col className='col-md-1'>
                                                </Col>

                                            </Row>
                                        </Col>
                                        <Col className='col-md-1'></Col>
                                        <Col className="col-md-2">

                                        </Col>
                                    </Row>


                                </Col>
                                <Col className="col-md-4">
                                    <Tabs
                                        id="controlled-tab-example"
                                        activeKey={key}
                                        onSelect={(k) => setKey(k)}
                                        className="mb-1"
                                        style={{ width: '600px', fontSize: '9px ' }} >
                                        {/* /****************** Invoice details card ********************* */}
                                        <Tab eventKey="Invoice Details" title="Invoice Details"  >
                                            <Card>
                                                <h3>
                                                    <center>
                                                        <b>
                                                            Invoice Details
                                                        </b>
                                                    </center>
                                                </h3>
                                                <br />
                                                <center>

                                                    <FormGroup>
                                                        <Label className="labels-invNo">
                                                            Inv. No
                                                        </Label>
                                                        <br />
                                                        <Input
                                                            tabIndex="0"
                                                            // autoComplete="username"
                                                            name="invoiceNo"
                                                            onChange={(e) => setInvoiceNo(e.target.value)}
                                                            value={assetData.invoiceno}
                                                            type="text"
                                                            className='user-controls'
                                                            placeholder="Inv. No." />
                                                    </FormGroup>
                                                    <FormGroup>
                                                        <Label className="labels-invDate">
                                                            Inv.Date
                                                        </Label>
                                                        <br />
                                                        <DatePicker
                                                            value={assetData.invoicedate}
                                                            selected={selectedDate}
                                                            onChange={date => setSelectedDate(date)}
                                                            className='user-controls'
                                                            name="selectedDate"
                                                            dateFormat="MM/dd/yyyy" />
                                                    </FormGroup>
                                                    <FormGroup>
                                                        <Label className="label-invAmount">
                                                            &nbsp;   Inv. Amount
                                                        </Label>
                                                        <br />
                                                        <Input
                                                            tabIndex="0"
                                                            name="invoiceAmount"
                                                            onChange={(e) => setInvoiceAmount(e.target.value)}
                                                            value={assetData.amount}
                                                            type="text"
                                                            className='user-controls'
                                                            placeholder="Amount" />
                                                    </FormGroup>
                                                    <FormGroup>
                                                        <Label className="label-supplier">
                                                            Supplier
                                                        </Label>
                                                        <br />
                                                        <Input
                                                            tabIndex="0"
                                                            // autoComplete="username"
                                                            name="invoiceSupplier"
                                                            onChange={(e) => setInvoiceSupplier(e.target.value)}
                                                            value={assetData.supplier}
                                                            type="text"
                                                            className='user-controls'
                                                            placeholder="Supplier" />
                                                    </FormGroup>
                                                </center>
                                            </Card>
                                        </Tab>
                                        {/* /****************** Insurance details card ********************* */}
                                        <Tab eventKey="Insurance Details" title="Insurance Details"  >
                                            <Card>
                                                <h3>
                                                    <center>
                                                        <b>
                                                            Insurance Details
                                                        </b>
                                                    </center>
                                                </h3>
                                                <br />
                                                <center>
                                                    <FormGroup>
                                                        <Label className="insPolicy">
                                                            Insurance Policy
                                                        </Label>
                                                        <Input
                                                            tabIndex="0"
                                                            // autoComplete="username"
                                                            name="insPolicy"
                                                            onChange={(e) => setInsPolicy(e.target.value)}
                                                            value={assetData.insurance_policynumber}
                                                            type="text"

                                                            className='user-controls'
                                                            placeholder="Insurance Policy" />
                                                    </FormGroup>
                                                    <FormGroup>
                                                        <Label className="insSDate">
                                                            Insurance Start Period
                                                        </Label>
                                                        <DatePicker
                                                            value={assetData.insurance_periodstart}
                                                            selected={startDate}
                                                            onChange={(startDate) => handleStartDateChange(startDate)}
                                                            dateFormat="MM/dd/yyyy"
                                                            className='user-controls'
                                                            placeholder="Insurance Start Period" />
                                                    </FormGroup>
                                                    <FormGroup>
                                                        <Label className="insEDate">
                                                            Insurance End Period
                                                        </Label>
                                                        <DatePicker
                                                            value={assetData.insurance_periodend}
                                                            selected={endDate}
                                                            onChange={handleEndDateChange}
                                                            dateFormat="MM/dd/yyyy"
                                                            className='user-controls' />
                                                    </FormGroup>
                                                    {errorMessage && <div style={{ color: 'red' }}>{errorMessage}</div>}
                                                    <br /><br /><br /><br />
                                                </center>
                                            </Card>
                                        </Tab>
                                        {/* /****************** AMC details card ********************* */}
                                        <Tab eventKey="AMC Details" title="AMC Details"  >
                                            <Card>
                                                <h3>
                                                    <center>
                                                        <b>
                                                            AMC Details
                                                        </b>
                                                    </center>
                                                </h3>
                                                <br />
                                                <center>
                                                    <FormGroup>
                                                        <Label className="insEDate">
                                                            Insurance  Policy No.
                                                        </Label>
                                                        <Input
                                                            tabIndex="0"
                                                            // autoComplete="username"
                                                            name={Amc}
                                                            onChange={(e) => setAmc(e.target.value)}
                                                            value={Amc}
                                                            type="text"

                                                            className='user-controls'
                                                            placeholder="Insurance Policy No." />

                                                    </FormGroup>
                                                    <FormGroup>
                                                        <Label className="amcsDate">
                                                            AMC Start Date
                                                        </Label>
                                                        <DatePicker
                                                            value={startAmcDate}
                                                            selected={startAmcDate}
                                                            onChange={(startAmcDate) => handleStartAmcDate(startAmcDate)}
                                                            dateFormat="MM/dd/yyyy"
                                                            className='user-controls'
                                                            placeholder="Insurance Start Period"
                                                        />
                                                    </FormGroup>
                                                    <FormGroup>
                                                        <Label className="amcEDate">
                                                            AMC End Date
                                                        </Label>
                                                        <DatePicker
                                                            selected={endAmcDate}
                                                            onChange={handleEndAmcDateChange}
                                                            dateFormat="MM/dd/yyyy"
                                                            className='user-controls'
                                                        />
                                                    </FormGroup>
                                                    {errorAmcMessage && <div style={{ color: 'red' }}>{errorAmcMessage}</div>}

                                                    <FormGroup>
                                                        <Label className="amcADate">
                                                            AMC Amount
                                                        </Label>
                                                        <Input
                                                            tabIndex="0"
                                                            // autoComplete="username"
                                                            name={amcAmount}
                                                            onChange={(e) => setAmcAmount(e.target.value)}
                                                            value={amcAmount}
                                                            type="text"

                                                            className='user-controls'
                                                            placeholder="AMC Amount" /></FormGroup>
                                                </center>
                                            </Card>
                                        </Tab>
                                    </Tabs>
                                </Col>
                            </Row>
                        </Card>
                    </Form>
                </Container>
            </div>
        </div>
    )
}
export default UpdateAsset;