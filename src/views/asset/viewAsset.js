import React, { useEffect, useState } from "react";
import { baseURL } from "config/config";
import {
    Col,
    Row,
    Button,
    Container,
    Link,
    Card, 
} from 'component/components';

const ViewAsset = () => 
{

    useEffect(() => {
        getAsset();
    }, []);

    /******************Hooks for getting the asset detials ********************* */
    const [list, setList] = useState([]);
    const [showHistory, setShowHistory] = useState(false);
    const [formData, setFormData] = useState({
        id: ''
    });

    /****************** get a single asset based on asset Id ********************* */
    const getAsset = () => 
    {
        console.log("id", sessionStorage.getItem("viewData"));

        fetch(baseURL + 'getasset', {
            method: 'POST',
            body: JSON.stringify({
                'ids': [sessionStorage.getItem("viewData")]
            }),

            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then((response) => response.json())
            .then((responseJson) => {
                console.log('sendata response=', responseJson);

                if (responseJson.status == 1) {

                    setList(responseJson.data);
                    sessionStorage.removeItem("viewData");
                }

            });
    }

/****************** handle history card ********************* */
    const handleHistory = () => {
        setShowHistory(true);
    }

/****************** Page render ********************* */
    return (
        <div>
            <br />
            <Container>
                {list.map(item => (
                    <Card>
                        <Row>
                            <h4>
                                <b>
                                    &nbsp;&nbsp;&nbsp; &nbsp; Asset Details
                                </b>
                            </h4>
                        </Row>
                        <Row>
                            &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;
                            {/* /****************** back btn ********************* */ }
                            <Link to="/asset"  >
                                <Button type="button" className="btn btn-dark"  >
                                    &nbsp; Back to list  &nbsp;
                                </Button>
                            </Link>
                            {/* /****************** history btn ********************* */ }
                            <Button type="button" className="btn btn-success" onClick={() => handleHistory()} >
                                History
                            </Button>
                        </Row>
                        <br />
                        {/* /****************** Asset Details ********************* */ }
                        <Row>
                            &nbsp;&nbsp;&nbsp; &nbsp;   &nbsp;&nbsp;&nbsp; &nbsp;
                            <Col className='col-md-4'>
                                <h5>
                                    <b  >Asset ID</b>&nbsp; : &nbsp;
                                    {item.assetid}
                                </h5>
                                <h5>
                                    <b >Department</b>&nbsp;:
                                    {item.departmentname}
                                </h5>
                                <h5>
                                    <b  >Main Classification</b>&nbsp; : &nbsp;
                                    {item.mainclassificationname}
                                </h5>
                                <h5>
                                    <b  >Sub Classification</b>&nbsp; : &nbsp;
                                    {item.subclassificationname}
                                </h5>
                                <h5>
                                    <b> Asset Type</b>&nbsp; :&nbsp;
                                    {item.assettypename}
                                </h5>
                                <h5>
                                    <b > Building</b>&nbsp; :&nbsp;
                                    {item.buildingname}
                                </h5>
                            </Col>
                            <Col className='col-md-4'>

                                <h5>
                                    <b >floor</b>&nbsp; :&nbsp;
                                    {item.floorname}
                                </h5>

                                <h5>
                                    <b >Location</b>&nbsp; : &nbsp;
                                    {item.locationname}
                                </h5>

                                <h5>
                                    <b >User</b>&nbsp; : &nbsp;
                                    {item.user}
                                </h5>

                                <h5>
                                    <b  >Make</b>&nbsp; : &nbsp;
                                    {item.make}
                                </h5>

                                <h5>
                                    <b >Description</b>&nbsp; :&nbsp; {item.description}
                                </h5>

                                <h5>
                                    <b  >Invoice Number</b>&nbsp; :&nbsp; {item.invoiceno}
                                </h5>
                            </Col>
                            <Col className='col-md-3'>
                                <h5>
                                    <b >Invoice Date</b>
                                    &nbsp; :&nbsp; 
                                    {new Date(item.invoicedate).toISOString().split('T')[0]}
                                </h5>
                                <h5>
                                    <b >Amount</b>
                                    &nbsp; :  &nbsp; {item.amount}
                                </h5>
                                <h5>
                                    <b >Supplier</b>
                                    &nbsp; :&nbsp; {item.supplier}
                                </h5>
                                <h5>
                                    <b >Insurance Policy Number</b>
                                    &nbsp; : &nbsp; {item.insurance_policynum}
                                </h5>
                                <h5>
                                    <b >Insurance Start Date</b>&nbsp; :&nbsp;<br />
                                    {new Date(item.period_start).toISOString().split('T')[0]}
                                </h5>
                                <h5>
                                    <b >Insurance End Date</b>
                                    &nbsp; : &nbsp;<br /> 
                                    {new Date(item.period_end).toISOString().split('T')[0]}
                                </h5>
                            </Col>
                        </Row>
                        {showHistory ?
                            <>
                                {item.updatelist.map((updateList) => (
                                    <Card>
                                        <Row>
                                            &nbsp;&nbsp;&nbsp; &nbsp;   &nbsp;&nbsp;&nbsp; &nbsp;
                                            <h3><b>History</b></h3>
                                        </Row>
                                        <Row>
                                            &nbsp;&nbsp;&nbsp; &nbsp;   &nbsp;&nbsp;&nbsp; &nbsp;
                                            <Col className='col-md-4'>
                                                <h5>
                                                    <b >Updated by:</b>&nbsp; {updateList.modifiedby}
                                                </h5>
                                                <h5>
                                                    <b>Updated at:</b> &nbsp; {new Date(updateList.modifiedat).toISOString().split('T')[0]}
                                                </h5>
                                                <br />
                                            </Col>
                                            <Col className='col-md-3'>
                                            </Col>
                                        </Row>
                                    </Card>
                                ))}
                            </>
                            : null}
                    </Card>
                ))}
            </Container>
        </div>
    )
}
export default ViewAsset;

