import React, { useEffect, useState } from "react";
import {
    Container,
    Row,
    Col,
    Button,
    Link,
    Alert,

} from 'component/components';
import 'App.css';
import Table from 'react-bootstrap/Table';
import { baseURL } from "config/config";

const Assets = () => {

     /******************Hooks for role based login ********************* */
    const [showBtn, setshowBtn] = useState(false);

     /******************Hooks for getting all the asset ********************* */
    const [list, setList] = useState([]);

     /******************Hooks for Error msg ********************* */
    const [craetegrpalert, setCraetegrpalert] = useState(false);
    const [craetegrpalert_type, setCraetegrpalertType] = useState("");
    const [craetegrperror, setCraetegrperror] = useState("");

     /******************Hooks for Pagination ********************* */
    const [currentPage, setCurrentPage] = useState(1);
    const [itemsPerPage, setItemsPerPage] = useState(5);
    const [searchQuery, setSearchQuery] = useState('');
    const [selectedCheckItems, setSelectedCheckItems] = useState([]);

     /******************Hooks for modal(PDF,Excel,Barcode) ********************* */
    const [demoModal, setDemoModal] = useState(false);
    const [selectedRadioOption, setRadioOption] = useState('');
    const [allChecked, setAllChecked] = useState(false);

     /****************** useEffect for setting the role type ********************* */
    useEffect(() => {
        const roletype = window.localStorage.getItem('roletypeid');
        console.log(roletype);
        if (roletype === '1') 
        {
            setshowBtn(true);
        }
        else if (roletype === '2') {
            setshowBtn(true);
        }
        else {
            setshowBtn(false)
        }
        getAllAsset();
    }, []);

     /****************** close the error msg ********************* */
    const onDismisss = () => {
        setCraetegrpalert(false);
        setCraetegrperror("");
        setCraetegrpalertType("primary");
    };

    /****************** for displaying the pdf,excel,barcode options ********************* */
    const toggle = () => {
        setDemoModal(!demoModal);
    };

    /****************** Change page on previous click ********************* */
    const handlePreviousClick = () => {
        if (currentPage > 1) {
            setCurrentPage(currentPage - 1);
        }
    };
    /****************** Change page on next click ********************* */
    const handleNextClick = () => {
        if (currentPage < totalPages) {
            setCurrentPage(currentPage + 1);
        }
    };

    /****************** get all API ********************* */
    const getAllAsset = (e) => 
    {
        fetch(baseURL + 'getasset', 
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then((response) => response.json())
            .then((responseJson) => {
                console.log('sendata response=', responseJson);

                if (responseJson.status == 1) {
                    var temp = [];
                    for (let i = 0; i < responseJson.data.length; i++) {
                        temp.push({
                            id: responseJson.data[i].id,
                            assetid: responseJson.data[i].assetid,
                            assetypeid: responseJson.data[i].assetypeid,
                            departmentid: responseJson.data[i].departmentid,
                            mainclassificationid: responseJson.data[i].mainclassificationid,
                            submainclassificationid: responseJson.data[i].submainclassificationid,
                            subclassificationname: responseJson.data[i].subclassificationname,
                            departmentname: responseJson.data[i].departmentname,
                            mainclassificationname: responseJson.data[i].mainclassificationname,
                            assettypename: responseJson.data[i].assettypename,
                            assetIDD: responseJson.data[i].id,
                            buildingid: responseJson.data[i].buildingid,
                            buildingname: responseJson.data[i].buildingname,
                            floorid: responseJson.data[i].floorid,
                            floorname: responseJson.data[i].floorname,
                            locationid: responseJson.data[i].locationid,
                            locationname: responseJson.data[i].locationname,

                            invoiceid: responseJson.data[i].invoiceid,
                            invoicedate: responseJson.data[i].invoicedate,
                            invoiceno: responseJson.data[i].invoiceno,
                            supplier: responseJson.data[i].supplier,
                            amount: responseJson.data[i].amount,


                            insuranceid: responseJson.data[i].insuranceid,
                            insurance_periodend: responseJson.data[i].insurance_periodend,
                            insurance_periodstart: responseJson.data[i].insurance_periodstart,
                            insurance_policynumber: responseJson.data[i].insurance_policynumber,

                            make: responseJson.data[i].make,
                            description: responseJson.data[i].description,

                            userid: responseJson.data[i].userid,
                            username: responseJson.data[i].username,

                            createdat: responseJson.data[i].createdat,

                            createdby: responseJson.data[i].createdby,

                        });
                    }
                    setList(temp);
                }
            });
    }

    /****************** delete API ********************* */
    const deleteAsset = (assetid) => 
    {
        console.log("delete assetid", assetid);
        fetch(baseURL + 'deleteasset', 
        {
            method: 'DELETE',
            body: JSON.stringify({
                'id': assetid,
            }),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then((result) => {
                result.json().then((resp) => {

                    console.log("reponse", resp);
                    if (resp.status === 1) {
                        setCraetegrpalert(true);
                        setCraetegrpalertType("success");
                        setCraetegrperror(resp);
                        getAllAsset();
                    }

                });
            })
            .catch((error) => {
                console.log('error=', error);
            });
    }

    /****************** filter data for search ********************* */
    const filteredList = list.filter(
        (item) =>
            item.assetid.toLowerCase().includes(searchQuery.toLowerCase()) ||
            item.departmentname.toLowerCase().includes(searchQuery.toLowerCase()) ||
            item.mainclassificationname.toLowerCase().includes(searchQuery.toLowerCase()) ||
            item.subclassificationname.toLowerCase().includes(searchQuery.toLowerCase()) ||
            item.assettypename.toLowerCase().includes(searchQuery.toLowerCase())
        //   item.createdby.toLowerCase().includes(searchQuery.toLowerCase())
    );



    // Calculate index of the last item on the current page
    const indexOfLastItem = currentPage * itemsPerPage;

    // Calculate index of the first item on the current page
    const indexOfFirstItem = indexOfLastItem - itemsPerPage;

    // Calculate total number of pages
    const totalPages = Math.ceil(filteredList.length / itemsPerPage);

    // Get the current items on the page
    const currentItems = filteredList.slice(
        indexOfFirstItem,
        indexOfLastItem
    );

    /****************** sending data to update page ********************* */
    const handleClick = (item) => {
        const data = {

            assetid: item.assetid,
            assetypeid: item.assetypeid,
            departmentid: item.departmentid,
            mainclassificationid: item.mainclassificationid,
            submainclassificationid: item.submainclassificationid,
            subclassificationname: item.subclassificationname,
            departmentname: item.departmentname,
            mainclassificationname: item.mainclassificationname,
            assettypename: item.assettypename,
            ID: item.id,
            buildingid: item.buildingid,
            buildingname: item.buildingname,
            floorid: item.floorid,
            floorname: item.floorname,
            locationid: item.locationid,
            locationname: item.locationname,

            invoiceid: item.invoiceid,
            invoicedate: item.invoicedate,
            invoiceno: item.invoiceno,
            supplier: item.supplier,
            amount: item.amount,


            insuranceid: item.insuranceid,
            insurance_periodend: item.insurance_periodend,
            insurance_periodstart: item.insurance_periodstart,
            insurance_policynumber: item.insurance_policynumber,

            make: item.make,
            description: item.description,

            userid: item.userid,
            username: item.username,

        };
        sessionStorage.setItem("assetData", JSON.stringify(data));
    };

    /****************** passing asset ID to view page ********************* */
    const handleView = (item) => {
        const Viewdata = item.assetIDD;
        sessionStorage.setItem("viewData", JSON.stringify(Viewdata));

    }

    /****************** Handle table check box ********************* */
    const handleCheckboxChange = (event, item) => {

        const isChecked = event.target.checked;
        let items = [...selectedCheckItems];
        if (isChecked) {
            items.push(item);
        } else {
            items = items.filter((i) => i.assetIDD !== item.assetIDD);
        }
        setSelectedCheckItems(items);
    }

    /****************** Delete selected asset ********************* */
    const handleDelete = () => {
        const selectedIds = selectedCheckItems.map(item => item.assetIDD);
        console.log("delete", selectedIds);
        fetch(baseURL + 'deleteassets', {
            method: 'DELETE',
            body: JSON.stringify({
                'id': selectedIds,
            }),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then((result) => {
                result.json().then((resp) => {

                    console.log("reponse", resp);
                    if (resp.status === 1) {
                        setCraetegrpalert(true);
                        setCraetegrpalertType("success");
                        setCraetegrperror(resp);
                        getAllAsset();
                    }

                });
            })
            .catch((error) => {
                console.log('error=', error);
            });
    }

    /****************** handle radio btns of export ********************* */
    const handleOptionChange = (event) => {
        setRadioOption(event.target.value);
    };

    /****************** Export PDf API ********************* */
    const handleDownloadClick = () => {
        const selectedIds = selectedCheckItems.map(item => item.assetIDD);
        console.log("selected items", selectedIds);
        console.log("called", selectedRadioOption);
        if (selectedRadioOption === '1') {
            console.log("hi saba");
            fetch(baseURL + 'getasset', {
                method: 'POST',
                body: JSON.stringify({
                    'ids': selectedIds
                }),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })


                //   .then((response) => {
                //       const contentDisposition = response.headers.get("Content-Disposition");
                //       const fileNameMatch = contentDisposition.match(/filename="(.+)"/);
                //       const fileName = fileNameMatch ? fileNameMatch[1] : "file.pdf";
                //       return response.blob().then((blob) => {
                //       const url = window.URL.createObjectURL(new Blob([blob]));
                //       const link = document.createElement("a");
                //       link.href = url;
                //       link.setAttribute("download", fileName);
                //       document.body.appendChild(link);
                //       link.click();
                //       document.body.removeChild(link);
                //       });
                //       })

                .then(response => response.blob())
                .then(blob => {

                    const url = window.URL.createObjectURL(new Blob([blob]));
                    const link = document.createElement('a');
                    link.href = url;
                    //  link.setAttribute("download", fileName);
                    link.setAttribute('download', 'asset-report.pdf');
                    document.body.appendChild(link);
                    link.click();
                    document.body.removeChild(link);
                })



            // .then(async (response) => {
            //     console.log(response.headers);
            //     const fileNameHeader =  response.headers.get('content-disposition');
            //     console.log("filename",fileNameHeader);
            //      const fileNameMatches = fileNameHeader.match(/filename="(.*?)"/);
            //      const fileName = fileNameMatches ? fileNameMatches[1] : "file.pdf";
            //      return response.blob().then((blob) => ({ blob, fileName }));
            //      })
            //      .then(({ blob, fileName }) => {
            //      const url = window.URL.createObjectURL(new Blob([blob]));
            //      const link = document.createElement("a");
            //      link.href = url;
            //      link.setAttribute("download", fileName);
            //      document.body.appendChild(link);
            //      link.click();
            //      document.body.removeChild(link);
            //      })
            //  .catch(error => console.error('Error downloading PDF:', error));


            // .then((response) => response.json())
            // .then((responseJson) => {
            //     console.log(' sub classification response=', responseJson);

            //     if (responseJson.status == 1) {
            //         setCraetegrpalert(true);
            //         setCraetegrpalertType("success");
            //         setCraetegrperror(responseJson);
            //         setDemoModal(false);

            //    }
            //     });
        }
        if (selectedRadioOption === '2') {
            console.log("hi saba anjum");
            fetch(baseURL + 'excel', {
                method: 'POST',
                body: JSON.stringify({
                    'ids': selectedIds
                }),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })
                .then(response => response.blob())
                .then(blob => {

                    const url = window.URL.createObjectURL(new Blob([blob]));
                    const link = document.createElement('a');
                    link.href = url;
                    //  link.setAttribute("download", fileName);
                    link.setAttribute('download', 'asset-report.xlsx');
                    document.body.appendChild(link);
                    link.click();
                    document.body.removeChild(link);
                })
        }
        if (selectedRadioOption === '3') {
            console.log("hi saba");
            fetch(baseURL + 'generateBarcode', {
                method: 'POST',
                body: JSON.stringify({
                    'ids': selectedIds
                }),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })
                .then((response) => response.json())
                .then((responseJson) => {
                    console.log(' sub classification response=', responseJson);

                    if (responseJson.status == 1) {
                        setCraetegrpalert(true);
                        setCraetegrpalertType("success");
                        setCraetegrperror(responseJson);
                        setDemoModal(false);
                    }
                });
        }
    }

    /****************** Handle Header check box ********************* */
    const handleAllCheckboxChange = (event) => {
        const isChecked = event.target.checked;
        setAllChecked(isChecked);
        const items = isChecked ? currentItems : [];
        setSelectedCheckItems(items);
    };

  /****************** page render ********************* */
    return (
        
            <div>
                <Container>
                    <Row>
                        <h4>
                            <b>
                                &nbsp;&nbsp;&nbsp; Asset List
                            </b>
                        </h4>
                        <br />
                        &nbsp;&nbsp;
                        &nbsp;&nbsp;
                        {/* /****************** Error msg ********************* */ }
                        <div id="alertid">
                            <Alert type={craetegrpalert_type} className="text-center"
                                isOpen={craetegrpalert} onClick={() => onDismisss("craetegrpalert")}>
                                {craetegrperror.message}
                            </Alert>
                        </div>
                    </Row>
                    <Row>
                        <Col className="col-md-6">
                            <Row>
                                {/* /****************** Add Asset btn ********************* */ }
                                {(showBtn) ?
                                    <Col className="col-md-2 col-sm-4">
                                        <Link to="/addAsset"  >   <Button type="button" className="btn btn-dark">&nbsp;Add Asset&nbsp;</Button> </Link>
                                    </Col>
                                    : null}

                                {/* /****************** Delete btn ********************* */ }
                                {(showBtn) ?
                                    <Col className="col-md-2 col-sm-4">
                                        <Button type="button" className="btn btn-danger" onClick={handleDelete}>&nbsp;&nbsp;&nbsp;&nbsp;Delete&nbsp;&nbsp;&nbsp;&nbsp;</Button>
                                    </Col>
                                    : null}

                                {/* /****************** Export btn ********************* */ }
                                <Col className="col-md-2 col-sm-4">
                                    <Button type="button" className="btn btn-success" onClick={toggle}>&nbsp;&nbsp;&nbsp;&nbsp;Export&nbsp;&nbsp;&nbsp;&nbsp;</Button>
                                </Col>

                                {/* /****************** Export option Modal ********************* */ }
                                <Col className="col-md-2 col-sm-4">
                                    {demoModal &&
                                        <div style={{ width: '100px' }}>
                                            <label >
                                                <input
                                                    type="radio"
                                                    name="option"
                                                    value="1"
                                                    checked={selectedRadioOption === '1'}
                                                    onChange={handleOptionChange}
                                                />
                                                &nbsp; PDF
                                            </label>
                                            <br />
                                            <label>
                                                <input
                                                    type="radio"
                                                    name="option"
                                                    value="2"
                                                    checked={selectedRadioOption === '2'}
                                                    onChange={handleOptionChange}
                                                />
                                                &nbsp; Excel
                                            </label>
                                            <br />
                                            <label>
                                                <input
                                                    type="radio"
                                                    name="option"
                                                    value="3"
                                                    checked={selectedRadioOption === '3'}
                                                    onChange={handleOptionChange}
                                                />
                                                &nbsp; Barcode
                                            </label>

                                            <br />
                                            <i class="fas fa-download" onClick={handleDownloadClick} style={{ justifyContent: 'right' }}></i>
                                        </div>
                                    }
                                </Col>
                            </Row>
                        </Col>
                        <Col className="col-md-5">
                            <Row>
                                <Col className="col-md-6">
                                </Col>
                                {/* /****************** Search component ********************* */ }
                                <Col className="col-md-6">
                                    <Row>
                                        <Col className="col-md-6">
                                        </Col>
                                        <Col className="col-md-5">
                                            <input
                                                type="text"
                                                className="Search-bar"
                                                placeholder="Search"
                                                value={searchQuery}
                                                onChange={(e) => setSearchQuery(e.target.value)}
                                            />

                                            &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
                                        </Col>
                                        <Col className='col-md-1'></Col>
                                    </Row>
                                </Col>
                                <Col className="col-md-1">
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col className="col-md-12">
                        {/* /****************** Asset List table ********************* */ }
                            <Table className="table">
                                <thead className="bg-dark">
                                    <tr>
                                        <th>
                                            <input type="checkbox" id="democheck1" checked={allChecked} onChange={handleAllCheckboxChange} />
                                        </th>
                                        <th >Asset No.</th>
                                        <th >Department</th>
                                        <th >Main Classification</th>
                                        <th >Sub Classification</th>
                                        <th>Asset Type</th>
                                        {/* <th>Createdby</th> */}
                                        <th>Created At</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {currentItems.length > 0 ? (
                                        currentItems.map((item) => (
                                            <tr key={item.assetIDD} >
                                                {/* {console.log("abcdefg",selectedCheckItems)} */}
                                                <td><input type="checkbox" id={`checkbox-${item.assetIDD}`} checked={selectedCheckItems.some(i => i.assetIDD === item.assetIDD)} onChange={event => handleCheckboxChange(event, item)} /></td>
                                                <td>{item.assetid}</td>
                                                <td> {item.departmentname}</td>
                                                <td>{item.mainclassificationname}</td>
                                                <td>{item.subclassificationname}</td>
                                                <td>{item.assettypename}</td>
                                                {/* <td>{item.createdby}</td> */}
                                                <td>{new Date(item.createdat).toISOString().split('T')[0]} </td>
                                                <td>
                                                    <Link to='/viewAsset'><i class="far fa-eye" onClick={() => handleView(item)} style={{ color: 'green', height: '10px' }}></i></Link>&nbsp;&nbsp;
                                                    {(showBtn) ? <Link to='/updateAsset' onClick={() => handleClick(item)}  >
                                                        <i class="far fa-edit success" ></i></Link> : null}
                                                    &nbsp;&nbsp;
                                                    {(showBtn) ?
                                                        <i class="fas fa-trash-alt" onClick={() => deleteAsset(item.assetIDD)}></i> : null}
                                                </td>

                                            </tr>
                                        ))
                                    ) : (
                                        <tr>
                                            <td colSpan={7} style={{ textAlign: "center" }}>
                                                No results found
                                            </td>
                                        </tr>
                                    )}
                                </tbody>
                            </Table>
                        </Col>
                    </Row>
                    <Row>
                        <div style={{ display: 'flex', justifyContent: 'center' }}>
                            <center>
                                <button onClick={handlePreviousClick} className="previous" disabled={currentPage === 1}  ><center>{"<<"}</center></button> &nbsp;
                                <button onClick={handleNextClick} className="next" disabled={currentPage === totalPages}>{">>"}</button>
                            </center>
                        </div>
                    </Row>
                </Container>
            </div>
        
    )
}

export default Assets;