import React, { useState, useEffect } from "react";
import {
    Container,
    Row,
    Col,
    Button,
    Image,
    FormGroup,
    Input,
    Alert,
} from 'component/components';
import { baseURL } from "config/config";
import Tab from 'react-bootstrap/Tab';
import Tabs from 'react-bootstrap/Tabs';
import Card from 'react-bootstrap/Card';
import 'App.css';

const Department = (props) => {

    /******************Hooks for Input Fields********************* */
    const [building, setBuilding] = useState('');
    const [floor, setFloor] = useState('');
    const [floorDrp, setFloorDrp] = useState('');
    const [location, setLocation] = useState('');

    /******************Hooks for List which is getting the data from api********************* */
    const [list, setList] = useState([]);
    const [floorList, setFloorList] = useState([]);
    const [showBtn, setshowBtn] = useState(false);

    /******************Hooks for tabs ********************* */
    const [key, setKey] = useState('Building');

    /******************Hooks for Error msg********************* */
    const [craeteGrpAlert, setCraeteGrpAlert] = useState(false);
    const [craeteGrpAlertType, setCraeteGrpAlertType] = useState("");
    const [craeteGrpError, setCraeteGrpError] = useState("");

    /******************Hooks for displaying heading of the table ********************* */
    const [heading, setHeading] = useState('Building');

     /****************** useEffect for setting the roletype ********************* */
    useEffect(() => {
        getAllBuilding();
        getAllFloor();
        const roletype = window.localStorage.getItem('roletypeid');
        console.log(roletype);
        if (roletype === '1')
        {
            setshowBtn(true);
        }
        else if (roletype === '2') 
        {
            setshowBtn(true);
        }
        else 
        {
            setshowBtn(false)
        }
    }, []);

     /******************to close the error bar ********************* */
    const onDismisss = () => {
        setCraeteGrpAlert(false);
        setCraeteGrpError("");
        setCraeteGrpAlertType("primary");
    };

    /****************** Add floor API ********************* */
    const handleChange = (event) => {
        event.preventDefault();
        fetch(baseURL + 'addfloor', 
        {
            method: 'POST',
            body: JSON.stringify({
                'buildingid': building,
                'username': localStorage.getItem('username'),
                'floorname': floor
            }),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then(
                (response) => response.json()
            ).then((responseJson) => {
                console.log('unnathi floor', responseJson);
                if (responseJson.status == 1) {
                    // var temp = [];
                    // console.log("called", responseJson.data[0].floorid);
                    // for (let i = 0; i < responseJson.data.length; i++) {
                    // temp.push({
                    //     _id: responseJson.data[i]._id,
                    //     floorid: responseJson.data[i].floorid,
                    //     floorname: responseJson.data[i].floorname,
                    //     createdBy: responseJson.data[i].createdBy,
                    //     createAt: responseJson.data[i].createdAt
                    // })
                    // }
                }
            })
    }


    /****************** Add Location API ********************* */
    const handleLocation = (event) => 
    {
        event.preventDefault();
        console.log("floorid", floorDrp);
        fetch(baseURL + 'addlocation', 
        {
            method: 'POST',
            body: JSON.stringify({
                'buildingid': building,
                'username': localStorage.getItem('username'),
                'floorid': floorDrp,
                'locationname': location
            }),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then(
                (response) => response.json()
            ).then((responseJson) => {
                console.log('unnathi floor', responseJson);
                if (responseJson.status == 1) {
                    // var temp = [];
                    // console.log("called", responseJson.data[0].floorid);
                    // for (let i = 0; i < responseJson.data.length; i++) {
                    // temp.push({
                    //     _id: responseJson.data[i]._id,
                    //     floorid: responseJson.data[i].floorid,
                    //     floorname: responseJson.data[i].floorname,
                    //     createdBy: responseJson.data[i].createdBy,
                    //     createAt: responseJson.data[i].createdAt
                    // })
                    // }
                }
            })
    }


    /****************** Add Building API ********************* */
    const addBuilding = () => 
    {
        console.log("dept name", building);
        fetch(baseURL + 'addbuilding', 
        {
            method: 'POST',
            body: JSON.stringify({
                'buildingname': building,
                'username': localStorage.getItem('username')

            }),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then((response) => response.json())
            .then((responseJson) => {

                console.log('sendata response=', responseJson);
                console.log('sendata token=', responseJson.token);

                if (responseJson.status === 1) {
                    getAllBuilding();
                }
            })
    }



    /****************** Get All Building API ********************* */
    const getAllBuilding = (e) => 
    {
        // e.preventDefault();
        // if (isValid)
        {
            fetch(baseURL + 'getallbuilding', {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })
                .then((response) => response.json())
                .then((responseJson) => {
                    console.log('sendata response=', responseJson);

                    if (responseJson.status == 1) {
                        var temp = [];
                        for (let i = 0; i < responseJson.data.length; i++) {
                            temp.push({
                                buildingid: responseJson.data[i].buildingid,
                                buildingname: responseJson.data[i].buildingname,
                                createdby: responseJson.data[i].createdby,
                                createdat: responseJson.data[i].createdat
                            });
                        }
                        setList(temp);

                    }
                });


        }
    };

     /****************** Get All Floor API ********************* */
    const getAllFloor = (e) => {
        // e.preventDefault();
        // if (isValid)
        {
            fetch(baseURL + 'getallfloor', {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })
                .then((response) => response.json())
                .then((responseJson) => {
                    console.log('sendata response=', responseJson);

                    if (responseJson.status == 1) {
                        var temp = [];
                        for (let i = 0; i < responseJson.data.length; i++) {
                            temp.push({
                                floorid: responseJson.data[i].floorid,
                                floorname: responseJson.data[i].floorname,
                                createdby: responseJson.data[i].createdby,
                                createdat: responseJson.data[i].createdat
                            });
                        }
                        setFloorList(temp);

                    }
                });


        }
    };

    {/* **************** Future use if needed  ******************** */}

    // const deleteBuilding = (buildId) => {
    //     console.log("delete assetid", buildId);
    //     fetch(baseURL + 'deletebuilding', {
    //         method: 'DELETE',
    //         body: JSON.stringify({
    //             'buildingid': buildId,
    //         }),
    //         headers: {
    //             'Content-Type': 'application/json',
    //             'Authorization': `Bearer ${localStorage.getItem('token')}`
    //         }
    //     })
    //         .then((result) => {
    //             result.json().then((resp) => {

    //                 console.log("reponse", resp);
    //                 if (resp.status === 1) {
    //                     setCraetegrpalert(true);
    //                     setCraetegrpalertType("success");
    //                     setCraetegrperror(resp);
    //                     getAllBuilding();
    //                 }

    //             });
    //         })
    //         .catch((error) => {
    //             console.log('error=', error);
    //         });
    // }


    /****************** Onclick of tabs respective table will displayed ********************* */
    const handleTabChange = (k) => {
        // setActiveTab(tab);
        setKey(k);
        // Call API to fetch data based on the selected tab
        // and update tableData state with the new data
        if (k === "Building") {
            setHeading("Building");
            fetch(baseURL + 'getallbuilding', {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })
                .then((response) => response.json())
                .then((responseJson) => {
                    console.log('sendata response of building=', responseJson);
                    if (responseJson.status == 1) {
                        var temp = [];
                        for (let i = 0; i < responseJson.data.length; i++) {
                            temp.push({
                                buildingid: responseJson.data[i].buildingid,
                                buildingname: responseJson.data[i].buildingname,
                                createdby: responseJson.data[i].createdby,
                                createdat: responseJson.data[i].createdat
                            });
                        }
                        setList(temp);
                    }
                });
        }
        else if (k === "Floor") {
            setHeading("Floor");
            fetch(baseURL + 'getallfloor', {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })
                .then((response) => response.json())
                .then((responseJson) => {
                    console.log('sendata response of floor=', responseJson);
                    if (responseJson.status == 1) {
                        var temp = [];
                        for (let i = 0; i < responseJson.data.length; i++) {
                            temp.push({
                                buildingid: responseJson.data[i].floorid,
                                buildingname: responseJson.data[i].floorname,
                                createdby: responseJson.data[i].createdby,
                                createdat: responseJson.data[i].createdat
                            });
                        }
                        setList(temp);
                    }
                });
        }
        else if (k === "Location") {
            setHeading("Location");
            fetch(baseURL + 'getalllocation', {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })
                .then((response) => response.json())
                .then((responseJson) => {
                    console.log('sendata response of department=', responseJson);
                    if (responseJson.status == 1) {
                        var temp = [];
                        for (let i = 0; i < responseJson.data.length; i++) {
                            temp.push({
                                buildingid: responseJson.data[i].locationid,
                                buildingname: responseJson.data[i].locationname,
                                createdby: responseJson.data[i].createdby,
                                createdat: responseJson.data[i].createdat
                            });
                        }
                        setList(temp);
                    }
                });
        }


    };


 /******************Page Render ********************* */
    return (
        <div>
            <div>
                <Container>
                    {(showBtn) ?
                        <Row>
                            <Col className="col-md-8 col-xl-8 col-sm-12">
                                <Row>
                                    <Col className="col-md-6 col-xl-6 col-sm-12">
                                        <Row>
                                            <h3>
                                                <b>
                                                    &nbsp;&nbsp;&nbsp; {heading}
                                                </b>
                                            </h3>
                                            <br />  
                                            {/* /****************** display Error msg ********************* */ }
                                            <div id="alertid">
                                                <Alert type={craeteGrpAlertType} className="text-center"
                                                    isOpen={craeteGrpAlert} onClick={() => onDismisss("craeteGrpAlert")}>
                                                    {craeteGrpError.status}: {craeteGrpError.data}
                                                </Alert>
                                            </div>
                                        </Row>
                                    </Col>
                                   
                                </Row>
                                <br />
                                {/* /****************** Building Table ********************* */ }
                                <Row>
                                    <Col className="col-md-12 col-xl-12 col-sm-12">
                                        <table class="table">
                                            <thead className="bg-dark">
                                                <tr>
                                                    <th scope="col">Name</th>
                                                    <th scope="col">Created At</th>
                                                    <th scope="col">Created by</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {list.map(item => (
                                                    <tr key={item.buildingid}>
                                                        <th scope="row">{item.buildingname}</th>
                                                        <td>{new Date(item.createdat).toISOString().split('T')[0]}</td>
                                                        <td>{item.createdby}</td>
                                                        {/* <td> <i class="far fa-edit success" ></i>&nbsp;&nbsp;  
                                                                <i class="fas fa-trash-alt" onClick={()=>deleteBuilding(item.buildingid)}></i> 
                                                            </td>  */}
                                                    </tr>
                                                ))}
                                            </tbody>
                                        </table>
                                    </Col>
                                </Row>
                            </Col>


                     {/* /****************** Add Building Card ********************* */ }
                            <Col className="col-md-4 col-xl-4 col-sm-12">
                                <Row>
                                    <Tabs
                                        id="controlled-tab-example"
                                        activeKey={key}
                                        onSelect={handleTabChange}
                                        className="mb-3"
                                        style={{ width: '600px', fontSize: '9px ' }}>
                                        <Tab eventKey="Building" title=" Building"  >
                                            <Card className="user-card">
                                                <Card.Body>
                                                    <center>
                                                        <b><h6>Add Building</h6></b>
                                                    </center>
                                                    <Image src={require("images/Add_users.png")} className="user_img" alt="bg" />
                                                    <br />
                                                    <center>
                                                        <FormGroup>
                                                            <Input
                                                                tabIndex="0"
                                                                name="building"
                                                                onChange={(e) => setBuilding(e.target.value)}
                                                                value={building}
                                                                type="text"
                                                                id="building"
                                                                className='user-control'
                                                                placeholder="Building Name"
                                                                required
                                                            />
                                                            {/* <InvalidFeedback style={{ display: loginiderror ? 'block' : 'none' }}>
                                                                    Please Enter username!
                                                                    </InvalidFeedback> */}
                                                        </FormGroup>
                                                    </center>
                                                    <br />
                                                    <center>
                                                        <Button type="button" className="btn btn-dark" onClick={addBuilding}>&nbsp;Add Building&nbsp;</Button>
                                                    </center>
                                                </Card.Body>
                                            </Card>

                                        </Tab>

                        {/* /****************** Add Floor Card ********************* */ }
                                        <Tab eventKey="Floor" title="Floor">
                                            <Card className="user-card">
                                                <Card.Body>
                                                    <center>
                                                        <b><h6>Add Floor</h6></b>
                                                    </center>
                                                    <Image src={require("images/Add_users.png")} className="user_img" alt="bg" />
                                                    <br />
                                                    <center>
                                                        <form onSubmit={handleChange}>
                                                            <FormGroup>
                                                                <select
                                                                    value={building}
                                                                    onChange={(e) => setBuilding(e.target.value)}
                                                                    className='user-control'>
                                                                    <option value="">Select Building</option>
                                                                    {list.map((option) => (
                                                                        <option key={option.buildingid} value={option.buildingid}>
                                                                            {option.buildingname}
                                                                        </option>
                                                                    ))}
                                                                </select>
                                                            </FormGroup>
                                                            <br />
                                                            <FormGroup>
                                                                <Input
                                                                    tabIndex="0"
                                                                    name="floor"
                                                                    onChange={(e) => setFloor(e.target.value)}
                                                                    value={floor}
                                                                    type="text"
                                                                    className='user-control'
                                                                    placeholder="Floor"
                                                                    required />

                                                            </FormGroup>

                                                            <center>
                                                                <Button type="submit" className="btn btn-dark">&nbsp;Add Floor&nbsp;</Button>
                                                            </center>

                                                        </form>
                                                    </center>
                                                </Card.Body>
                                            </Card>
                                        </Tab>

                        {/* /****************** Add Location Card ********************* */ }
                                        <Tab eventKey="Location" title="Location" >
                                            <Card className="user-card">
                                                <Card.Body>
                                                    <center>
                                                        <b><h5>Add Location</h5></b>
                                                    </center>
                                                    <br />
                                                    <form onSubmit={handleLocation}>
                                                        <center>
                                                            <FormGroup>
                                                                <select
                                                                    value={building}
                                                                    onChange={(e) => setBuilding(e.target.value)}
                                                                    className='user-control'>
                                                                    <option value="">Select Building</option>
                                                                    {list.map((option) => (
                                                                        <option key={option.buildingid} value={option.buildingid}>
                                                                            {option.buildingname}
                                                                        </option>
                                                                    ))}
                                                                </select>
                                                            </FormGroup>
                                                            <br />
                                                            <FormGroup>
                                                                <select
                                                                    value={floorDrp}
                                                                    onChange={(e) => setFloorDrp(e.target.value)}
                                                                    className='user-control'>
                                                                    <option value="">Floor</option>
                                                                    {floorList.map((option) => (
                                                                        <option key={option.floorid} value={option.floorid}>
                                                                            {option.floorname}
                                                                        </option>
                                                                    ))}
                                                                </select>
                                                            </FormGroup>
                                                            <br />
                                                            <FormGroup>
                                                                <Input
                                                                    tabIndex="0"
                                                                    // autoComplete="username"
                                                                    name="location"
                                                                    onChange={(e) => setLocation(e.target.value)}
                                                                    value={location}
                                                                    type="text"
                                                                    className='user-control'
                                                                    placeholder="Location"
                                                                    required/>
                                                            </FormGroup>
                                                            <br />
                                                            <Button type="submit" className="btn btn-dark"  >&nbsp;Add Location&nbsp;</Button>
                                                        </center>
                                                    </form>
                                                </Card.Body>
                                            </Card>
                                        </Tab>
                                    </Tabs>
                                </Row>
                            </Col>
                        </Row>
                        :
                        
                        <Col className="col-md-12 col-xl-12">
                        {/* /****************** Building Table ********************* */ }
                            <Row>
                                <Col className="col-md-6">
                                    <Row>
                                        &nbsp;&nbsp;  &nbsp;
                                        <h3>
                                            <b>
                                                Building Details
                                            </b>
                                        </h3>
                                    </Row>
                                </Col>
                            </Row>
                            <br />
                            <Row>
                                <Col className="col-md-12 col-xl-12">
                                    <table class="table">
                                        <thead className="bg-dark">
                                            <tr>
                                                <th scope="col"> Name</th>
                                                <th scope="col">Created At</th>
                                                <th scope="col">Created by</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {list.map(item => (
                                                <tr key={item.buildingid}>
                                                    <th scope="row">{item.buildingname}</th>
                                                    <td> {new Date(item.createdat).toISOString().split('T')[0]}</td>
                                                    <td>{item.createdby}</td>
                                                    {/* <td><button className="btn btn-success-btn-lg"  >
                                                             <i class="far fa-edit success" ></i> <i class="fas fa-trash-alt"></i>
                                                            </button>
                                                        </td>  */}
                                                </tr>
                                            ))}
                                        </tbody>
                                    </table>
                                </Col>
                            </Row>
                        </Col>
                        }
                </Container>
            </div>
        </div>
    )
}

export default Department;