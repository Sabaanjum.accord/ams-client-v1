
/*
*****************************************************************************
* License Information :Accord Global Technology Solutions Pvt. Ltd.                  *

*                      #72 & 73, Krishna Reddy Colony, Domlur layout,            *

*                      Domlur,Bangalore - 560071, INDIA                     *

*                      Licensed software and All rights reserved.           *

*****************************************************************************

* File             : 
*
* Description      : User profile page
*
* Author(s)        : Kavita Gupta
*
* Version History:
* <Version Number>                 <Author>              <date>      <defect Number>      <Modification
*                                                                                          made and the
*                                                                                          reason for
*                                                                                          modification >
*  1.0                             Kavita Gupta         21.11.2020        --         initial version
*
* References        :
*                     
* Assumption(s)     : None.
*                     
* Constraint(s)     : None.
*                     
 ****************************************************************************
*/

import React from 'react';
import {Link} from 'react-router-dom';
import {
    num,
    namecheck,
   limitLength,} from 'config/config.js';
import AuthHelper from "azbycxjklg4k5jgh3";
import {StateList, timeConverter} from "component/component-util";
import { Picky } from 'react-picky';
import 'react-picky/dist/picky.css'; 
import {
CustomRadio,
Modal, ModalBody, ModalHeader,
ModalFooter, 
Form,
FormGroup,
AlertModal,
Label,
InvalidFeedback,
Input,
Card,
CardBody,
Row,Alert,Button,
Col, ContainerFluid,
Image, CustomFile
}
from "component/components";

const CODE_LENGTH = new Array(6).fill(0);

class Userprofile extends AuthHelper {

    otpinput = React.createRef();
    constructor(props) {
        super(props);
        this.state = {
            profilealert:false,
            profileloader:"none",
            profilerr:"",
            profilealert_type:"primary",
            userimage: require("images/avatar_image.jpg"),
            primaryRadioContact:true,
            primaryRadioEmail:false,
            otpmodal:false,
            otpvalue: "",
            focused: false,
            pickyValue:"",
            fields: {
                name:"",
                pcontact: "",
                scontact:"",
                email:"",
                organisation:"",
                address:"",
                city:"",
                pin:"",
                otp:"",
                primarycontact:"",
                updatedon:"01/01/1900"
            }
    }
}



handleSubmit=(event)=>{
    event.preventDefault();
    
    if(this.handleProfileValidation())
    {
      //to do
    }
}

//form input onchange function 
changeHandler = (field,event)=> {
    this.onDismiss();
    let fields = this.state.fields;
    switch (field){
        case "name":
            fields["name"] = limitLength((namecheck(event.target.value)),50);
            this.setState({nameerror: false});
            break;
        case "pcontact":
            fields["pcontact"] = limitLength(num(event.target.value), 10);
            this.setState({pcontacterror: false});
            this.setState({pcontactduplicate: false});
            break;
        case "scontact":
            fields["scontact"] = limitLength(num(event.target.value), 10);
            this.setState({scontacterror: false});
            break;
        case "email":
            fields["email"] = limitLength((event.target.value), 100);
            this.setState({emailerror: false});
            this.setState({emailduplicate: false});
            break;
        case "organisation":
            fields["organisation"] = limitLength((event.target.value), 100);
            this.setState({organisationerr: false});
            break;
        case "address":
            fields["address"] = limitLength((event.target.value), 100);
            this.setState({addresserror: false});
            break;
        case "city":
            fields["city"] =  limitLength((namecheck(event.target.value)), 30);
            this.setState({cityerror: false});
            break;
        case "pin":
            fields["pin"] = limitLength(num(event.target.value), 6);
            this.setState({pinerror: false});
            break;
        case "otp":     
            const value = event.target.value;

            this.setState(state => {
               // if (state.value.length >= CODE_LENGTH.length) return null;
                return {
                otpvalue: (state.otpvalue + value).slice(0, CODE_LENGTH.length),
                };
            });
            
            this.setState({otperror: false});
                //console.log('value',this.state.value);

            break;
        case "primarycontact":
            if(event.target.value === "radiophone"){
                this.setState({primaryRadioContact : true, primaryRadioEmail: false});
            }  
            else{
                this.setState({primaryRadioEmail : true, primaryRadioContact: false});
            }
            break; 

        default:
        break;
    }
    this.setState({fields});
};



// create group form validation     
handlePrimaryValidation(){
    
    let formIsValid = true,
    
    pcontact = this.state.fields.pcontact,
    email = this.state.fields.email;

    var mailformat = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/;

    //based on user selection and new primary contact checking
    // with existing primary contact
    if(this.state.primaryRadioContact){
        //scontact
        if(pcontact === undefined || (pcontact).trim() ===""){
            formIsValid = false;
            this.setState({pcontacterror: true});
        }

        //scontact length validation
        if((pcontact).trim()!==""){
            if(pcontact.length >0 && pcontact.length <=9){
                formIsValid = false;
                this.setState({pcontacterror: true});
            }    
        }
        if((pcontact).trim() === this.getLoggedinContact()){
            formIsValid = false;
            this.setState({pcontactduplicate:true});   
        }
    }// pcontact check end
    else{
        //email
        if(email === undefined || (email).trim()===""){
            formIsValid = false;
            this.setState({emailerror: true});
        }

        //email Pattern validation
        if((email).trim()!=="" && (email).trim()!==undefined){
            if(!email.match(mailformat))
            {
                formIsValid = false;
                this.setState({emailerror: true});
            }
        }

        if(this.getLoggedinEmail() === email)
        {
            formIsValid = false;
            this.setState({emailduplicate : true});
        }
        
    }//email check end
    return formIsValid;
}

// create group form validation     
handleProfileValidation(){
    
    let formIsValid = true,
    organisation = this.state.fields.organisation,
    name = this.state.fields.name,
    scontact = this.state.fields.scontact,
    address = this.state.fields.address,
    city = this.state.fields.city,
    pin = this.state.fields.pin;
    
     let StateName;
    // if(this.state.StateName.value===undefined)
    //  {
    //      StateName=this.state.StateName
    //  }
    //  else{
    //     StateName = this.state.StateName.value;
    //  }
  
    //Institute Name
    if(organisation === undefined || (organisation).trim() ===""){
        formIsValid = false;
        this.setState({organisationerr: true});
    }
    //name
    if(name === undefined || (name).trim()===""){
        formIsValid = false;
        this.setState({nameerror: true});
       
    }
   
    //scontact length validation
    if((scontact).trim()!==""){
        if(scontact.length >0 && scontact.length <9){
            formIsValid = false;
            this.setState({scontacterror: true});
        }    
    }
    
     //StateName
     if(StateName === undefined || StateName===""){
        formIsValid = false;
        this.setState({stateerror: true});
    }
    //address
    if(address === undefined || (address).trim() ===""){
        formIsValid = false;
        this.setState({addresserror: true});
    }
    //city
    if(city === undefined || (city).trim()===""){
        formIsValid = false;
        this.setState({cityerror: true});
    }
    //pincode
    if(pin === undefined || (pin)===""){
        formIsValid = false;
        this.setState({pinerror: true});
    }
    //phone length validation
    if((pin)!==""){
        if(pin.length >0 && pin.length <=5){
            formIsValid = false;
            this.setState({pinerror: true});
        }    
    }
    return formIsValid;
}

validateOtp=()=>{
    let formIsValid = true,
    //otp = this.state.fields.otp;
    otp = this.state.otpvalue;
    if((otp).trim() !==""){
        if(otp.length !== 6){
            formIsValid = false;
            this.setState({otperror: true});
        }else{
            console.log(this.state.otpvalue);
        }   
    }

    if((otp).trim() ==="" || !otp){
            formIsValid = false;
            this.setState({otperror: true});
    }

    return formIsValid;
}

otpsubmit = (event) =>{
    event.preventDefault();
    if(this.validateOtp())
    {
        this.loader(true); 
        var c_="",e_="";
        if(this.state.primaryRadioContact){
            c_ = this.state.fields.pcontact;
        }else{
            e_ = this.state.fields.email;
        }
        }
}

changeNow=(event)=>{
    event.preventDefault();
    this.setState({otpmodal:false, otpvalue:""});
    this.setState({
        otpmodalalertmsg: "", 
        otpmodalalerttype: "primary", 
        otpmodalalert:false});

    if(this.handlePrimaryValidation())
    {
    
        //TODO
    
    }
}

//profile alert dismiss
onDismiss=()=>{
    this.setState({profilealert: false, otpmodalalert: false});
}

//close profile
handleClose=()=>{
this.props.history.push('/dashboard');
}
// handle statename change    
handleStateChange=(value)=>{
    this.setState({ StateName: value },()=>{
       // console.log("state:",value)
    });
}

// OTP functions
otphandleClick = () => {
    this.otpinput.current.focus();
  };
otphandleFocus = () => {
    this.setState({ focused: true });
  };
otphandleBlur = () => {
    this.setState({
      focused: false,
    });
  };
otphandleKeyUp = e => {
    if (e.key === "Backspace") {
      this.setState(state => {
        return {
            otpvalue: state.otpvalue.slice(0, state.otpvalue.length - 1),
        };
      });
    }
  };

render() { 
    const { otpvalue, focused } = this.state;

    const values = otpvalue.split("");
    
    const selectedIndex =
      values.length < CODE_LENGTH.length ? values.length : CODE_LENGTH.length - 1;

    const hideInput = !(values.length < CODE_LENGTH.length);

    return ( 

        <div id="userprofile" className="userprofile">
       
        <section className="page-header">
        <h1 className="page-header-text">User Profile</h1>
        <hr/>
        </section> 

        <ContainerFluid>
        <Card>
        <div style={{display:this.state.profileloader}} className="loader"></div>
        <CardBody>

        {(window.innerWidth < 992 ? 
        <div>
            <Col className="col-md-12">
                <AlertModal show={this.state.profilealert} type={this.state.profilealert_type} 
                    onOk={this.onDismiss.bind(this)} 
                    onClose={this.onDismiss.bind(this)}>
                    <p className="text-center">{this.state.profilerr}</p>
                </AlertModal>   
            </Col>
        </div>:
        <div>
            <Alert type={this.state.profilealert_type} className="text-center"
                isOpen={this.state.profilealert} onClick={this.onDismiss.bind(this)}>
                {this.state.profilerr}
            </Alert> 
        </div>
        )}

        <Form >
            <Row>
            <Col className="col-12 col-md-12 col-lg-2 border-right">
                
            <div className="d-flex flex-column align-items-center text-center p-3 py-5">
            <Image src={this.state.userimage} 
             className="img-thumbnail rounded-circle mx-auto d-block" height="150px" alt="User Profile" />
                <div className="profile_name"><strong>{this.state.fields.name}</strong></div>
            </div>
            </Col>
            {/* col 2 - level 1 */}
            <Col className="col-12 col-md-12 col-lg-10">
                <Row>
                <Col className="col-12 col-md-6 col-lg-6 ">
              
              <FormGroup>
                  <Label className="form-label" htmlFor="name">Name</Label>
                  <Input name="name" onChange={this.changeHandler.bind(this,"name")} value={this.state.fields.name} type="text" id="name" 
                      className={"mb-3 form-control"+(this.state.nameerror?" error":"")}
                      placeholder="Name" required="" />
                  <InvalidFeedback style={{display: (this.state.nameerror?"block":"none")}}>
                      Name field is empty or invalid
                  </InvalidFeedback>
              </FormGroup>

              <FormGroup>
                  <Label className="form-label" htmlFor="organisation">Institute / Corporate Name</Label>
                  <Input name="organisation" onChange={this.changeHandler.bind(this,"organisation")}  value={this.state.fields.organisation} type="text" id="organisation" 
                  className={"mb-3 form-control"+(this.state.organisationerr?" error":"")}
                      placeholder="Institute / Corporate Name" required="" />
                  <InvalidFeedback style={{display: this.state.organisationerr?"block":"none"}}>
                       Institute / Corporate Name field is empty or invalid
                  </InvalidFeedback>
              </FormGroup>

           <FormGroup>
              <Label className="form-label" htmlFor="scontact">Secondary Contact</Label>
              <Input name="scontact" onChange={this.changeHandler.bind(this,"scontact")} value={this.state.fields.scontact} type="text" id="scontact" 
                  className={"form-control"+(this.state.scontacterror?" error":"")}
                  placeholder="Secondary Contact" required="" />
              <InvalidFeedback style={{display: (this.state.scontacterror?"block":"none")}}>
                  Contact number is invalid
              </InvalidFeedback>
          </FormGroup> 
          <FormGroup>
              <Label className="form-label" htmlFor="address">Address</Label>
              <Input name="address" onChange={this.changeHandler.bind(this,"address")}  value={this.state.fields.address} type="text" id="address" 
              className={"mb-3 form-control"+(this.state.addresserror?" error":"")}
                  placeholder="Address" required="" />
              <InvalidFeedback style={{display: this.state.addresserror?"block":"none"}}>
                  Address is empty or invalid
              </InvalidFeedback>
          </FormGroup>

          </Col>

                <Col className="col-12 col-md-6 col-lg-6">

          <FormGroup>
                  <Label className="form-label" htmlFor="city">City</Label>
                  <Input name="city" onChange={this.changeHandler.bind(this,"city")}  value={this.state.fields.city} type="text" id="city" 
                  className={"mb-3 form-control"+(this.state.cityerror?" error":"")}
                      placeholder="City" required="" />
                  <InvalidFeedback style={{display: this.state.cityerror?"block":"none"}}>
                  City is empty or invalid
                  </InvalidFeedback>
          </FormGroup>

          <FormGroup>
              <Label className="form-label" htmlFor="state">State</Label>
              <Picky
                    id="taskname" 
                    area-labelledby="State" 
                    open={false}
                    placeholder="State*"
                    options={StateList}
                    value={this.state.pickyValue}
                    labelKey="label"
                    valueKey="value"
                    multiple={true}
                    includeSelectAll={true}
                    includeFilter={true}
                    onChange={values => this.setState({pickyValue:values})}
                    dropdownHeight={150}
                    keepOpen ={false}
                    clearFilterOnClose={true}
                    /> 
             
          <InvalidFeedback style={{display: (this.state.stateerror?"block":"none")}}>
              State is empty or invalid
          </InvalidFeedback>
          </FormGroup>

          <FormGroup>
              <Label className="form-label" htmlFor="pin">Pincode</Label>
              <Input name="state" onChange={this.changeHandler.bind(this,"pin")}  value={this.state.fields.pin} type="text" id="pin" 
              className={"mb-3 form-control"+(this.state.pinerror?" error":"")}
                  placeholder="Pin" required="" />
              <InvalidFeedback style={{display: this.state.pinerror?"block":"none"}}>
                  Pincode is empty or invalid
              </InvalidFeedback>
          </FormGroup>
          
            <Label className="form-label">Profile Image</Label>
            <CustomFile id="demofile1" name="demofile1" disabled></CustomFile>
             
          </Col>
        </Row>

        <Row>
            <Col className="col-sm-6 col-md-6 col-lg-6 col-xl-6">
            </Col>
            <Col className="col-sm-6 col-md-6 col-lg-6 col-xl-6">
            <Button type="button" className="btn btn-primary btn-block" onClick={this.handleSubmit.bind(this)} >Update Profile</Button>
            </Col>
        </Row>
    </Col>
    {/* col 10  - level 1 */}
    </Row>  
    </Form>
    <hr/><br/>
    <Form>
    <Row>
        <Col className="col-lg-2  d-none d-lg-block">
            <span>Last updated on:</span>
            <p className="text-muted">{this.state.fields.updatedon}</p>
        </Col>
        {/* col-hidden for <  768 */}
        <Col className="col-12 col-lg-10">

        <Row>
            <Col className="col-12 col-sm-4 col-lg-2">
            <Label className="form-label">To Change :</Label>
            </Col>
            <Col className="col-7 col-sm-4 col-lg-3">
                <CustomRadio value="radiophone" id="radioPhone"
                name="primarycontact" labelName="Phone Number" 
                checked={this.state.primaryRadioContact}
                onChange={this.changeHandler.bind(this,"primarycontact")} 
                 ></CustomRadio> 
            </Col>
            <Col className="col-5 col-sm-4 col-lg-3">
                 <CustomRadio value="radiomail" name="primarycontact" 
                 labelName="Email" id="radioMail"
                 checked={this.state.primaryRadioEmail}
                 onChange={this.changeHandler.bind(this,"primarycontact")} 
                 ></CustomRadio> 
            </Col>
            <Col></Col>
        </Row>
            <Row>
                <Col>

                {( this.state.primaryRadioContact ? 
                    <FormGroup>
                        <Label className="form-label" htmlFor="pcontact">Primary Contact</Label>
                        <Input name="pcontact" onChange={this.changeHandler.bind(this,"pcontact")}  value={this.state.fields.pcontact} type="text" id="pcontact" 
                        className={"form-control"+(this.state.pcontacterror?" error":"")}
                            placeholder="Primary Contact" required="" />
                        <InvalidFeedback style={{display: this.state.pcontacterror?"block":"none"}}>
                        Contact number is empty or invalid
                        </InvalidFeedback>
                        <InvalidFeedback style={{display: this.state.pcontactduplicate?"block":"none"}}>
                        No changes found!
                        </InvalidFeedback>
                    </FormGroup> 
                :
                <FormGroup>
                    <Label className="form-label" htmlFor="email">Email</Label>
                    <Input name="email" onChange={this.changeHandler.bind(this,"email")}  value={this.state.fields.email} type="text" id="email" 
                    className={"mb-3 form-control"+(this.state.emailerror?" error":"")}
                    placeholder="Email ID" required="" />
                    <InvalidFeedback style={{display: this.state.emailerror?"block":"none"}}>
                        Email ID field is empty or invalid
                    </InvalidFeedback>
                    <InvalidFeedback style={{display: this.state.emailduplicate?"block":"none"}}>
                        No changes found!
                    </InvalidFeedback>
                </FormGroup>
                )}
            </Col>
            </Row>
            <Row>
            <Col className="col-sm-6 col-md-6 col-lg-6 col-xl-6">
            </Col>
            <Col className="col-sm-6 col-md-6 col-lg-6 col-xl-6">
            <Button type="button" className="btn btn-primary btn-block" onClick={this.changeNow.bind(this)}>Change Request</Button>
            </Col>
        </Row>
        </Col>
    </Row>
            
            
        </Form>
        
        <Modal show={this.state.otpmodal}>
              <ModalHeader onClose={() => { this.setState({ otpmodal:false});}}>
                <h3 className="modal-title">OTP</h3>
              </ModalHeader>
              <ModalBody >
                
                <Alert type={this.state.otpmodalalerttype} className="ml-0 mr-0"
                isOpen={this.state.otpmodalalert} onClick={this.onDismiss.bind(this)}>
                {this.state.otpmodalalertmsg}
                </Alert>
                <Form>
                
                <FormGroup>
                  <Label className="form-label" htmlFor="otp">Verify your OTP</Label>
                  
                    <div className="wrap m-auto" onClick={this.otphandleClick}>
                    <input
                        type="number"
                        value=""
                        ref={this.otpinput}
                        onChange ={this.changeHandler.bind(this,"otp")}
                        //onChange={this.handleChange}
                        onKeyUp={this.otphandleKeyUp}
                        onFocus={this.otphandleFocus}
                        onBlur={this.otphandleBlur}
                        className={"otp6"+(this.state.otperror?" error":"")} 
                        style={{
                        width: "32px",
                        top: "0px",
                        bottom: "0px",
                        left: `${selectedIndex * 32}px`,
                        opacity: hideInput ? 0 : 1,
                        }}
                    />
                    {CODE_LENGTH.map((v, index) => {
                        const selected = values.length === index;
                        const filled = values.length === CODE_LENGTH.length && index === CODE_LENGTH.length - 1;

                        return (
                        <div key={index} className="otp-display">
                            {values[index]}
                            {(selected || filled) && focused && <div className="shadows" />}
                        </div>
                        );
                    })}
                    </div>
                    <InvalidFeedback style={{display: (this.state.otperror?"block":"none")}}>
                        Invalid OTP!
                    </InvalidFeedback>
                    </FormGroup>
                    </Form>
                    </ModalBody>

                <ModalFooter >
                <Row className="no-gutters">
                <Col className="col-12 col-sm-12">
                    <Button type="button" className="btn btn-primary btn-block " onClick={this.otpsubmit.bind(this)}>Done</Button>
                </Col>
                <Col className="col-12 col-sm-12">
                   Didn't receive the code? <Link to="#" className="btn btn-link " onClick={this.changeNow.bind(this)}>Resend</Link>
                </Col>
               
                </Row>
                 </ModalFooter>
            </Modal>


        </CardBody>
        </Card>
        </ContainerFluid>


    </div>
    )    
  }
}


export default Userprofile;