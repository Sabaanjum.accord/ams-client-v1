import React from "react";
import {
    Col,
    Row,
    Container,
    Card,
} from 'component/components';

const Dashboard = () => {
    return (
        <div>
            <br />
            <Container>
                <Row>
                    <Col className='col-sm-6 col-md-3'>
                    {/* /****************** Insurance Details Card ********************* */ }
                        <Card className="dashCards">
                            <br />
                            <center>
                                <h4>Track Insurance Details</h4>
                            </center>
                            <center>
                                <h3>100 Assets</h3>
                            </center>
                            <br />
                        </Card>
                    </Col>

                    <Col className='col-sm-6 col-md-3'>
                    {/* /****************** Upcoming Payments Card ********************* */ }
                        <Card className="dashCards">
                            <br />
                            <center>
                                <h4>Upcoming Payments </h4>
                            </center>
                            <center>
                                <h3>50 Assets</h3>
                            </center>
                            <br />
                        </Card>
                    </Col>

                    <Col className='col-sm-6 col-md-3'>
                    {/* /****************** Report Card ********************* */ }
                        <Card className="dashCards">
                            <br />
                            <center>
                                <h4>Report</h4>
                            </center>
                            <br />
                        </Card>
                    </Col>

                    <Col className='col-sm-6 col-md-3'>
                    {/* /****************** Track asset Card ********************* */ }
                        <Card className="dashCards">
                            <br />
                            <center>
                                <h4>Track Assets</h4>
                            </center>
                            <br />
                        </Card>
                    </Col>
                </Row>

                <Row>
                    <Col className='col-sm-6 col-md-3'>
                    {/* /****************** Quick access Card ********************* */ }
                        <Card className="dashCards">
                            <br />
                            <center>
                                <h4>Quick Acces</h4>
                            </center>
                            <br />
                        </Card>
                    </Col>

                    <Col className='col-sm-6 col-md-3'>
                    {/* /****************** Department availability Card ********************* */ }
                        <Card className="dashCards">
                            <br />
                            <center>
                                <h4>Department Accessibility</h4>
                            </center>
                            <br />
                        </Card>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}
export default Dashboard;