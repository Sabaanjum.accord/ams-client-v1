import React, { useState, useEffect } from "react";
import {
    Container,
    Row,
    Col,
    Button,
    Alert,
    Image,
    FormGroup,
    Input
} from 'component/components';
import Card from 'react-bootstrap/Card';
import 'App.css';
import Tab from 'react-bootstrap/Tab';
import Tabs from 'react-bootstrap/Tabs';
import { baseURL } from "config/config";


const Users = () => {


    /******************Hooks for input fields ********************* */
    const [userName, setUserName] = useState('');
    const [eMail, setEmail] = useState('');

    /******************Hooks for getting data from API ********************* */
    const [list, setList] = useState([]);

    /******************Hooks for setting tabs ********************* */
    const [key, setKey] = useState('External');

    /******************Hooks for setting role type ********************* */
    const [showBtn, setshowBtn] = useState(false);

    /******************Hooks for error msg ********************* */
    const [craeteGrpAlert, setCraeteGrpAlert] = useState(false);
    const [craeteGrpAlertType, setCraeteGrpAlertType] = useState("");
    const [craeteGrpError, setCraeteGrpError] = useState("");

    /******************Hooks for search component ********************* */
    const [searchQuery, setSearchQuery] = useState('');

    /****************** useEffect for setting the roletype ********************* */
    useEffect(() => {
        const roletype = window.localStorage.getItem('roletypeid');
        console.log(roletype);
        if (roletype === '1') {
            setshowBtn(true);
        }
        else if (roletype === '2') {
            setshowBtn(false);
        }
        else {
            setshowBtn(false)
        }
        getAllUsers();
    }, []);

    /****************** handle close  error msg ********************* */
    const onDismisss = () => {
        setCraeteGrpAlert(false);
        setCraeteGrpError("");
        setCraeteGrpAlertType("primary");
    };

    /****************** get all users API ********************* */
    const getAllUsers = (e) => {
        // e.preventDefault();
        // if (isValid)

        fetch(baseURL + 'getallusers', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then((response) => response.json())
            .then((responseJson) => {
                console.log('sendata response=', responseJson);

                if (responseJson.status === 1) {
                    var temp = [];
                    for (let i = 0; i < responseJson.data.length; i++) {
                        temp.push({
                            userid: responseJson.data[i].userid,
                            username: responseJson.data[i].username,
                            roletypename: responseJson.data[i].roletypename,
                            email: responseJson.data[i].email,
                            createdat: responseJson.data[i].createdat
                        });
                    }
                    console.log("ijuhyg", temp);
                    setList(temp);
                }
            });
    };


    /****************** Add external user API ********************* */
    const addExternalUser = (e) => {
        e.preventDefault();
        console.log("name", userName);
        console.log("emial", eMail);

        fetch(baseURL + 'addexternaluser', {
            method: 'POST',
            body: JSON.stringify({
                'username': userName,
                'email': eMail
            }),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then((response) => response.json())
            .then((responseJson) => {
                console.log('sendata response=', responseJson);
                // console.log('sendata token=', responseJson.token);
                if (responseJson.status === 1) {
                    setCraeteGrpAlert(true);
                    setCraeteGrpAlertType("success");
                    setCraeteGrpError(responseJson);
                    getAllUsers();
                }
                if (responseJson.status === 0) {
                    setCraeteGrpAlert(true);
                    setCraeteGrpAlertType("danger");
                    setCraeteGrpError(responseJson);
                    getAllUsers();
                }
            })
    }

    /****************** for future work ********************* */
    // const deleteUser=(userid)=>{
    //     console.log("user id",userid);
    //     fetch(baseURL + 'deleteuser', {
    //         method: 'DELETE',
    //         body: JSON.stringify({
    //             'userid': userid,
    //         }),
    //         headers: {
    //             'Content-Type': 'application/json',
    //             'Authorization': `Bearer ${localStorage.getItem('token')}`
    //         }
    //     })
    //         .then((result) => {
    //             result.json().then((resp) => {

    //                     console.log("reponse",resp);
    //                 if(resp.status===1){
    //                     setCraeteGrpAlert(true);
    //                            setCraeteGrpAlertType("success");
    //                             setCraeteGrpError(resp);
    //                             getAllUsers();
    //                 }

    //             });
    //         })
    //         .catch((error) => {
    //             console.log('error=', error);
    //         });
    // }

    /****************** filter the data for search component ********************* */
    const filteredList = list.filter(
        (item) =>
            item.username.toLowerCase().includes(searchQuery.toLowerCase())

    );

    /****************** page render ********************* */
    return (
        <div>
            <Container>
                {(showBtn) ?
                    <Row>
                        <Col className="col-md-8">
                            <Row>
                                <Col className="col-md-6">
                                    <Row>
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        <h3>
                                            <b>User Details</b>
                                        </h3>
                                        &nbsp;&nbsp;
                                    </Row>
                                </Col>
                                <Col className="col-md-6">
                                    <Row>
                                        <Col className="col-md-4">
                                        </Col>
                                        <Col className="col-md-4">
                                            <input
                                                type="text"
                                                className="Search-bar"
                                                placeholder="Search"
                                                value={searchQuery}
                                                onChange={(e) => setSearchQuery(e.target.value)}
                                            />
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                            <Row>
                                <Col className="col-md-12">
                                    <table class="table">
                                        <thead className="bg-dark">
                                            <tr>
                                                <th scope="col">username</th>
                                                <th scope="col">Email ID</th>
                                                <th scope="col">Role Type</th>
                                                <th scope="col">Created At</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {filteredList.map(item => (
                                                <tr key={item.userid}>
                                                    <th scope="row">{item.username}</th>
                                                    <td> {item.email}</td>
                                                    <td> {item.roletypename}</td>
                                                    <td>{new Date(item.createdat).toISOString().split('T')[0]}</td>
                                                    {/* <td><button className="btn btn-success-btn-lg" >
                                                                <i class="far fa-edit success" ></i>    
                                                            </button>
                                                            <button onClick={deleteUser(item.userid)}>
                                                                <i class="fas fa-trash-alt"></i>
                                                            </button>
                                                        </td>  */}
                                                </tr>
                                            ))}
                                        </tbody>
                                    </table>
                                </Col>
                            </Row>
                        </Col>
                        <Col className="col-md-4">
                            <Row>
                                <Tabs
                                    id="controlled-tab-example"
                                    activeKey={key}
                                    onSelect={(k) => setKey(k)}
                                    className="mb-3"
                                    style={{ width: '600px', fontSize: '9px ' }}>
                                    {/* /****************** Add internal user card ********************* */}
                                    <Tab eventKey="InternalUser" title="Internal User">
                                        <Card className="user-card">
                                            <Card.Body>
                                                <center>
                                                    <b><h3>Add User</h3></b>
                                                </center>
                                                <Image src={require("images/Add_users.png")} className="user_img" alt="bg" />
                                                <br /><br />
                                                <center>
                                                    <FormGroup>
                                                        <Input
                                                            tabIndex="0"
                                                            autoComplete="username"
                                                            name="loginid"
                                                            // onChange={(e) => changeHandler('loginid', e)}
                                                            // value={}
                                                            type="text"
                                                            id="loginid"
                                                            className={'user-control'}
                                                            placeholder="  &nbsp;User Name"
                                                            required
                                                        />
                                                    </FormGroup>
                                                </center>
                                                <br />
                                                <center>
                                                    <Button type="button" className="btn btn-dark">&nbsp;Add User&nbsp;</Button>
                                                </center>
                                            </Card.Body>
                                        </Card>
                                    </Tab>
                                    {/* /****************** Add external user card ********************* */}
                                    <Tab eventKey="External" title="External User">
                                        <Card className="user-card">
                                            <Card.Body>
                                                <center>
                                                    <b><h3>External User</h3></b>
                                                </center>
                                                <Image src={require("images/Add_users.png")} className="user_img" alt="bg" />
                                                <div id="alertid">
                                                    <Alert type={craeteGrpAlertType} className="text-center"
                                                        isOpen={craeteGrpAlert} onClick={() => onDismisss("craeteGrpAlert")}>
                                                        {craeteGrpError.status}: {craeteGrpError.data}
                                                    </Alert>
                                                </div>
                                                <form onSubmit={addExternalUser}>
                                                    <center>
                                                        <FormGroup>
                                                            <Input
                                                                tabIndex="0"
                                                                autoComplete="username"
                                                                // name="loginid"
                                                                onChange={(e) => setUserName(e.target.value)}
                                                                value={userName}
                                                                type="text"
                                                                // id="loginid"
                                                                className={'user-control'}
                                                                placeholder="  &nbsp;User Name"
                                                                required />
                                                        </FormGroup>
                                                        <FormGroup>
                                                            <Input
                                                                tabIndex="0"
                                                                autoComplete="username"
                                                                // name="loginid"
                                                                onChange={(e) => setEmail(e.target.value)}
                                                                value={eMail}
                                                                type="text"
                                                                id="loginid"
                                                                className={'user-control'}
                                                                placeholder="  &nbsp;Email"
                                                                required />
                                                        </FormGroup>
                                                        <Button type="submit" className="btn btn-dark">&nbsp;Add User&nbsp;</Button>
                                                    </center>
                                                </form>
                                            </Card.Body>
                                        </Card>
                                    </Tab>

                                </Tabs>
                            </Row>

                        </Col>
                    </Row>
                    :
                    <Row>
                        <Col className="col-md-12">
                            <Row>
                                <Col className="col-md-6">
                                    <Row>
                                    </Row>
                                </Col>
                                <Col className="col-md-6">
                                    <Row>
                                        <Col className="col-md-6">
                                        </Col>

                                        <Col className="col-md-4">
                                            <input
                                                type="text"
                                                className="Search-bar"
                                                placeholder="Search"
                                                value={searchQuery}
                                                onChange={(e) => setSearchQuery(e.target.value)} />
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                            <br />
                            {/* /****************** user table ********************* */}
                            <Row>
                                <Col className="col-md-12">
                                    <table class="table">
                                        <thead className="bg-dark">
                                            <tr>
                                                <th scope="col">username</th>
                                                <th scope="col">Email ID</th>
                                                <th scope="col">Role Type</th>
                                                <th scope="col">Created At</th>
                                                {/* <th scope="col">Action</th> */}
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {filteredList.map(item => (
                                                <tr key={item.userid}>
                                                    <th scope="row">{item.username}</th>
                                                    <td> {item.email}</td>
                                                    <td> {item.roletypename}</td>
                                                    <td>{new Date(item.createdat).toISOString().split('T')[0]}</td>
                                                    {/* <td>
                                                            <button className="btn btn-success-btn-lg" >
                                                                <i class="far fa-edit success" ></i> 
                                                                <i class="fas fa-trash-alt"></i>
                                                            </button>
                                                        </td>  */}
                                                </tr>
                                            ))}
                                        </tbody>
                                    </table>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                }
            </Container>
        </div>
    )
}

export default Users;